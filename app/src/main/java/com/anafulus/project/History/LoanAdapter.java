package com.anafulus.project.History;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.anafulus.project.DetailInstallment.NotYet;
import com.anafulus.project.Models.Loan;
import com.anafulus.project.R;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;


public class LoanAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<Loan> dataLoan;
    private LayoutInflater mInflater;

    public LoanAdapter(Context context, ArrayList<Loan> dataLoan) {
        this.context = context;
        this.dataLoan = dataLoan;
        mInflater = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getViewTypeCount() {
        return dataLoan.size();
    }
    @Override

    public int getItemViewType(int position) {

        return position;
    }

    @Override
    public int getCount() {
        return dataLoan.size();
    }

    @Override
    public Object getItem(int position) {

        return dataLoan.get(position);

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.adapter_loan, null, true);

            holder.tv_number = convertView.findViewById(R.id.number);
            holder.tv_borrow = convertView.findViewById(R.id.amount_borrow);
            holder.tv_installment = convertView.findViewById(R.id.amount_installment);
            holder.tv_tenor = convertView.findViewById(R.id.amount_tenor);
            holder.tv_type = convertView.findViewById(R.id.type_installment);
            holder.tv_status = convertView.findViewById(R.id.status_borrow);
            holder.btn_look = convertView.findViewById(R.id.btn_look);

            convertView.setTag(holder);
        }else {
            // the getTag returns the viewHolder object set as a tag to the view
            holder = (ViewHolder)convertView.getTag();
        }
        Locale localeID = new Locale("in", "ID");
        NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);

        holder.tv_number.setText(dataLoan.get(position).getId().toString());
        holder.tv_borrow.setText(formatRupiah.format((double)dataLoan.get(position).getLoan_principal()));
        if (dataLoan.get(position).getInstallment_type().equals("Mingguan")) {
            holder.tv_installment.setText(formatRupiah.format((double)dataLoan.get(position).getInstallment_nominal()) + " / " + "Minggu");
        } else {
            holder.tv_installment.setText(formatRupiah.format((double)dataLoan.get(position).getInstallment_nominal()) + " / " + "Bulan");
        }

        holder.tv_tenor.setText(dataLoan.get(position).getTenor());
        holder.tv_type.setText(dataLoan.get(position).getInstallment_type());
        holder.tv_status.setText(dataLoan.get(position).getLoan_status());

        holder.btn_look.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, NotYet.class);
                i.putExtra("loanStatus", dataLoan.get(position).getLoan_status());
                i.putExtra("idLoan", dataLoan.get(position).getId().toString());
                context.startActivity(i);
            }
        });

        return convertView;
    }

    private class ViewHolder {
        protected TextView tv_number, tv_borrow, tv_installment, tv_tenor, tv_type, tv_status;
        protected Button btn_look;
    }
}



