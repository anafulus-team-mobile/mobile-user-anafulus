package com.anafulus.project.History;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.anafulus.project.Config.BorrowerApi;
import com.anafulus.project.Config.RetrofitClient;
import com.anafulus.project.DetailInstallment.NotYet;
import com.anafulus.project.Help.Help;
import com.anafulus.project.Models.Borrowers;
import com.anafulus.project.Models.Loan;
import com.anafulus.project.Notify.PageNotify;
import com.anafulus.project.PageDashboard;
import com.anafulus.project.PageInfo;
import com.anafulus.project.Payment.Payment;
import com.anafulus.project.Profile.PageAccount;
import com.anafulus.project.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;


public class PageHistory extends AppCompatActivity {
    BorrowerApi mApiInterface;
    SharedPreferences preferences;

    private static final String TAG = "PageHistory";

    Context context;
    private Toolbar toolbar;
    private Spinner spinner;
    private RelativeLayout layout_running, layout_finish, layout_null;
    private Button btn_look, btn_pay;
    private boolean doubleTap = false;
    private ListView lv_reject, lv_finish, lv_approve, lv_cancel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.page_history);
        context = this;
        preferences = getSharedPreferences("borrower_profil",MODE_PRIVATE);
        mApiInterface = RetrofitClient.getClient().create(BorrowerApi.class);

        //Toolbar
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Riwayat");

        //Relative Layout Null
        layout_null = findViewById(R.id.layout_null);
        layout_null.setVisibility(View.GONE);

        // Get reference of widgets from XML layout
        spinner = findViewById(R.id.spinner);

        // Initializing a String Array
        String[] plants = new String[]{
                "Pilih Status...",
                "Cicilan Sedang Berjalan",
                "Cicilan Terlambat",
                "Pinjaman Belum Disetujui",
                "Pinjaman Selesai",
                "Pinjaman Ditolak",
                "Pinjaman Disetujui",
                "Pinjaman Dibatalkan"
        };

        final List<String> plantsList = new ArrayList<>(Arrays.asList(plants));

        // Initializing an ArrayAdapter
        final ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                this, R.layout.spinner_item, plantsList) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                } else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if (position == 0) {
                    // Set the hint text color gray
                    tv.setTextColor(Color.GRAY);
                } else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };

        //Layout Running Installment
        layout_running = findViewById(R.id.layout_running);

        //Button Look Detail Istallment
        btn_look = findViewById(R.id.btn_look);

        //Button Pay Installment
        btn_pay = findViewById(R.id.btn_pay);

        //Listview Reject
        lv_reject = findViewById(R.id.listview_reject);
        lv_reject.setVisibility(View.GONE);
        //Listview Finish
        lv_finish = findViewById(R.id.listview_finish);
        lv_finish.setVisibility(View.GONE);
        //Listview Approve
        lv_approve = findViewById(R.id.listview_approve);
        lv_approve.setVisibility(View.GONE);
        //Listview Cancel
        lv_cancel = findViewById(R.id.listview_cancel);
        lv_cancel.setVisibility(View.GONE);


        //Bottom Nav
        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottomNavView_Bar);
        Menu menu = bottomNavigationView.getMenu();
        MenuItem menuItem = menu.getItem(3);
        menuItem.setChecked(true);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.nav_dashboard:
                        Intent a = new Intent(PageHistory.this, PageDashboard.class);
                        startActivity(a);
                        finish();
                        break;

                    case R.id.nav_notify:
                        Intent b = new Intent(PageHistory.this, PageNotify.class);
                        startActivity(b);
                        finish();
                        break;

                    case R.id.nav_info:
                        Intent c = new Intent(PageHistory.this, PageInfo.class);
                        startActivity(c);
                        finish();
                        break;

                    case R.id.nav_history:
                        break;

                    case R.id.nav_account:
                        Intent d = new Intent(PageHistory.this, PageAccount.class);
                        startActivity(d);
                        finish();
                        break;
                }
                return false;
            }
        });

        //Spinner
        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
        spinner.setAdapter(spinnerArrayAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    layout_running.setVisibility(View.GONE);
                    layout_null.setVisibility(View.VISIBLE);
                } else if (position == 1) {
                    // Notify the selected item text
                    String loanStatus = "Cicilan sedang berjalan";
                    getLatestLoanByStatus(loanStatus);
                    layout_null.setVisibility(View.GONE);
                    layout_running.setVisibility(View.VISIBLE);
                    lv_finish.setVisibility(View.GONE);
                    lv_reject.setVisibility(View.GONE);
                    lv_approve.setVisibility(View.GONE);
                    lv_cancel.setVisibility(View.GONE);
                    btn_pay.setVisibility(View.VISIBLE);
                    btn_pay.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent i = new Intent(PageHistory.this, Payment.class);
                            startActivity(i);
                            finish();
                        }
                    });
                    btn_look.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent i = new Intent(PageHistory.this, NotYet.class);
                            i.putExtra("loanStatus", "Cicilan sedang berjalan");
                            startActivity(i);
                            finish();
                        }
                    });
                } else if (position == 2) {
                    String loanStatus = "Cicilan Terlambat";
                    getLatestLoanByStatus(loanStatus);
                    layout_null.setVisibility(View.GONE);
                    layout_running.setVisibility(View.VISIBLE);
                    lv_finish.setVisibility(View.GONE);
                    lv_reject.setVisibility(View.GONE);
                    lv_approve.setVisibility(View.GONE);
                    lv_cancel.setVisibility(View.GONE);
                    btn_pay.setVisibility(View.VISIBLE);
                    btn_pay.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent i = new Intent(PageHistory.this, Payment.class);
                            startActivity(i);
                            finish();
                        }
                    });
                    btn_look.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent i = new Intent(PageHistory.this, NotYet.class);
                            startActivity(i);
                            finish();
                        }
                    });
                } else if (position == 3) {
                    String loanStatus = "Belum disetujui";
                    getLatestLoanByStatus(loanStatus);
                    layout_null.setVisibility(View.GONE);
                    layout_running.setVisibility(View.VISIBLE);
                    lv_finish.setVisibility(View.GONE);
                    lv_reject.setVisibility(View.GONE);
                    lv_approve.setVisibility(View.GONE);
                    lv_cancel.setVisibility(View.GONE);
                    btn_pay.setVisibility(View.GONE);
                    btn_look.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent i = new Intent(PageHistory.this, NotYet.class);
                            i.putExtra("loanStatus", "Belum disetujui");
                            startActivity(i);
                            finish();
                        }
                    });

                } else if (position == 4) {
                    Integer layout = R.id.listview_finish;
                    String loanStatus = "Pinjaman sudah selesai";
                    getAllLoanByStatus(loanStatus, layout);
                    layout_null.setVisibility(View.GONE);
                    layout_running.setVisibility(View.GONE);
                    lv_finish.setVisibility(View.VISIBLE);
                    lv_reject.setVisibility(View.GONE);
                    lv_approve.setVisibility(View.GONE);
                    lv_cancel.setVisibility(View.GONE);
                    btn_pay.setVisibility(View.GONE);
                    btn_look.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent i = new Intent(PageHistory.this, NotYet.class);
                            Log.d("STATUSS","pinjaman selesai");
                            i.putExtra("loanStatus", "Pinjaman sudah selesai");
                            startActivity(i);
                            finish();
                        }
                    });

                } else if (position == 5) {
                    Integer layout = R.id.listview_reject;
                    String loanStatus = "Pinjaman ditolak";
                    getAllLoanByStatus(loanStatus, layout);
                    layout_null.setVisibility(View.GONE);
                    layout_running.setVisibility(View.GONE);
                    lv_finish.setVisibility(View.GONE);
                    lv_reject.setVisibility(View.VISIBLE);
                    lv_approve.setVisibility(View.GONE);
                    lv_cancel.setVisibility(View.GONE);
                    btn_pay.setVisibility(View.GONE);
                    btn_look.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent i = new Intent(PageHistory.this, NotYet.class);
                            i.putExtra("loanStatus", "Pinjaman ditolak");
                            startActivity(i);
                            finish();
                        }
                    });

                } else if (position == 6) {
                    Integer layout = R.id.listview_approve;
                    String loanStatus = "Pinjaman disetujui";
                    getAllLoanByStatus(loanStatus, layout);
                    layout_null.setVisibility(View.GONE);
                    layout_running.setVisibility(View.GONE);
                    lv_finish.setVisibility(View.GONE);
                    lv_reject.setVisibility(View.GONE);
                    lv_approve.setVisibility(View.VISIBLE);
                    lv_cancel.setVisibility(View.GONE);
                    btn_pay.setVisibility(View.GONE);

                } else if (position == 7) {
                    Integer layout = R.id.listview_cancel;
                    String loanStatus = "Pinjaman dibatalkan";
                    getAllLoanByStatus(loanStatus, layout);
                    layout_null.setVisibility(View.GONE);
                    layout_running.setVisibility(View.GONE);
                    lv_finish.setVisibility(View.GONE);
                    lv_reject.setVisibility(View.GONE);
                    lv_approve.setVisibility(View.GONE);
                    lv_cancel.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    //BUTTON HELP
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.help_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_help:
                Intent i = new Intent(PageHistory.this, Help.class);
                startActivity(i);
        }
        return super.onOptionsItemSelected(item);
    }

    //Double Tap For Exit App
    @Override
    public void onBackPressed() {
        if (doubleTap) {
            super.onBackPressed();
        } else {
            Toast.makeText(this, "Ketuk Tombol Back 2 Kali Untuk Keluar Aplikasi", Toast.LENGTH_SHORT).show();
            doubleTap = true;
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    doubleTap = false;
                }
            }, 500);
        }
    }

    public void clickMe(View view) {
        Button bt = (Button) view;
        Toast.makeText(this, "Button " + bt.getText().toString(), Toast.LENGTH_LONG).show();
    }

    private void getAllLoanByStatus(String loanStatus, Integer layout) {
        String idBorrower = preferences.getString("idBorrower",null);
        final String status = loanStatus;
        final Integer layoutList = layout;
        Call<Borrowers> getDataLoan = mApiInterface.getAllLoanByStatus(idBorrower, status);
        getDataLoan.enqueue(new Callback<Borrowers>() {
            @Override
            public void onResponse(Response<Borrowers> response, Retrofit retrofit) {
                RelativeLayout layoutNullValue = findViewById(R.id.layout_null);
                RelativeLayout layoutFoundValue = findViewById(R.id.layout_running);
                TextView txtNotFound = findViewById(R.id.notFound);
                if (response.body().getListLoan().isEmpty()) {
                    txtNotFound.setText("Tidak ada riwayat untuk status " + status);
                    layoutFoundValue.setVisibility(View.GONE);
                    layoutNullValue.setVisibility(View.VISIBLE);
                } else {
                    ArrayList<Loan> modelLoanArrayList = new ArrayList<>();
                    for (int a = 0; a < response.body().getListLoan().size(); a++) {

                        Loan loanView = new Loan();
                        loanView.setId(response.body().getListLoan().get(a).getId());
                        loanView.setLoan_principal(response.body().getListLoan().get(a).getLoan_principal());
                        loanView.setInstallment_nominal(response.body().getListLoan().get(a).getInstallment_nominal());
                        loanView.setTenor(response.body().getListLoan().get(a).getTenor().toString());
                        loanView.setInstallment_type(response.body().getListLoan().get(a).getInstallment_type());
                        loanView.setLoan_status(response.body().getListLoan().get(a).getLoan_status());
                        modelLoanArrayList.add(loanView);
                    }
                    makeToList(modelLoanArrayList, layoutList);
                }
            }
            @Override
            public void onFailure(Throwable t) {
                Log.d("AKSES KE SERVER GAGAL",t.getMessage());
                Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
            }
        });
    }
    private void makeToList(ArrayList<Loan> dataLoan, Integer layout) {
        ListView listViewLoan;
        LoanAdapter loanAdapter;
        listViewLoan = findViewById(layout);

        loanAdapter = new LoanAdapter(this, dataLoan);
        listViewLoan.setAdapter(loanAdapter);
    }

    //    }
    private void getLatestLoanByStatus(String loanStatus) {
        String idBorrower = preferences.getString("idBorrower",null);
        final String status = loanStatus;
        Call<Borrowers> getDataLoan = mApiInterface.getLatestLoanByStatus(idBorrower, status);
        getDataLoan.enqueue(new Callback<Borrowers>() {
            @Override
            public void onResponse(Response<Borrowers> response, Retrofit retrofit) {
                int i =0 ;

                TextView txtIdLoan = findViewById(R.id.idLoan);
                TextView txtLoanPrincipal = findViewById(R.id.loanPrincipal);
                TextView txtInstallment = findViewById(R.id.installment);
                TextView txtTenor = findViewById(R.id.tenor);
                TextView txtInstallmentType = findViewById(R.id.installmentType);
                TextView txtloanStatus = findViewById(R.id.loanStatus);

                RelativeLayout layoutNullValue = findViewById(R.id.layout_null);
                RelativeLayout layoutFoundValue = findViewById(R.id.layout_running);
                TextView txtNotFound = findViewById(R.id.notFound);
                Locale localeID = new Locale("in", "ID");
                NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);

                if (response.body()==null){
                    txtNotFound.setText("Tidak ada riwayat untuk status " + status.toString());
                    layoutFoundValue.setVisibility(View.GONE);
                    layoutNullValue.setVisibility(View.VISIBLE);
                } else {
                    txtIdLoan.setText(response.body().getListLoan().get(i).getId().toString());
                    txtLoanPrincipal.setText(formatRupiah.format((double)response.body().getListLoan().get(i).getLoan_principal()));
                    if (response.body().getListLoan().get(i).getInstallment_type().equals("Mingguan")) {
                        txtInstallment.setText(formatRupiah.format((double)response.body().getListLoan().get(i).getInstallment_nominal()) + " / " + "Minggu");
                    } else {
                        txtInstallment.setText(formatRupiah.format((double)response.body().getListLoan().get(i).getInstallment_nominal()) + " / " + "Bulan");
                    }
                    txtTenor.setText(response.body().getListLoan().get(i).getTenor());
                    txtInstallmentType.setText(response.body().getListLoan().get(i).getInstallment_type());
                    txtloanStatus.setText(response.body().getListLoan().get(i).getLoan_status());
                }
            }
            @Override
            public void onFailure(Throwable t) {
                Log.d("AKSES KE SERVER GAGAL",t.getMessage());
                Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
            }
        });
    }

}