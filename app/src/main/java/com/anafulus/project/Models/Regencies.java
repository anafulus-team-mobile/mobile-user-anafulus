package com.anafulus.project.Models;

import com.google.gson.annotations.SerializedName;

public class Regencies {
    @SerializedName("id")
    private Integer id;

    @SerializedName("id_province")
    private Integer idProvince;

    @SerializedName("regency_name")
    private String regencyName;

    public Regencies() {
    }

    public Regencies(Integer id, Integer idProvince, String regencyName) {
        this.id = id;
        this.idProvince = idProvince;
        this.regencyName = regencyName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdProvince() {
        return idProvince;
    }

    public void setIdProvince(Integer idProvince) {
        this.idProvince = idProvince;
    }

    public String getRegencyName() {
        return regencyName;
    }

    public void setRegencyName(String regencyName) {
        this.regencyName = regencyName;
    }
}
