package com.anafulus.project.Models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Agents {
    @SerializedName("error")
    private  boolean err;

    @SerializedName("message")
    private  String msg;

    @SerializedName("data")
    List<Agent> listDataAgent;

    public boolean isErr() {
        return err;
    }

    public void setErr(boolean err) {
        this.err = err;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<Agent> getListDataAgent() {
        return listDataAgent;
    }

    public void setListDataAgent(List<Agent> listDataAgent) {
        this.listDataAgent = listDataAgent;
    }
}
