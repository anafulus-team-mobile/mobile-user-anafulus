package com.anafulus.project.Models;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class Borrower {
    @SerializedName("id")
    private Integer id;
    @SerializedName("id_borrower")
    private Integer id_borrower;
    @SerializedName("gender")
    private String gender;
    @SerializedName("borrower_local_id")
    private Integer borrower_local_id;
    @SerializedName("birth_date")
    private String birth_date;
    @SerializedName("bank_acc_number")
    private Integer bank_acc_number;
    @SerializedName("last_education")
    private String last_education;
    @SerializedName("is_active")
    private Integer is_active;
    @SerializedName("id_village")
    private Integer id_village;
    @SerializedName("id_sub_district")
    private Integer id_sub_district;
    @SerializedName("family_phone_number")
    private String family_phone_number;
    @SerializedName("family_relationship")
    private String family_relationship;
    @SerializedName("family_name")
    private String family_name;
    @SerializedName("name")
    private String name;
    @SerializedName("email")
    private String email;
    @SerializedName("phone_number")
    private String phone_number;
    @SerializedName("domicile_address")
    private String domicile_address;
    @SerializedName("id_card_address")
    private String id_card_address;
    @SerializedName("profil_image")
    private String profil_image;
    @SerializedName("last_job")
    private String last_job;
    @SerializedName("income")
    private Integer income;
    @SerializedName("family_card_id")
    private Integer family_card_id;
    @SerializedName("principal_taxpayer_id")
    private Integer principal_taxpayer_id;
    @SerializedName("family_card_image")
    private String family_card_image;
    @SerializedName("local_id_image")
    private String local_id_image;
    @SerializedName("id_selfie_image")
    private String id_selfie_image;
    @SerializedName("home_image")
    private String home_image;
    @SerializedName("pin")
    private String pin;
    @SerializedName("rating")
    private String rating;
    @SerializedName("rating_desc")
    private String rating_desc;
    @SerializedName("salary_slip_image")
    private String salary_slip_image;
    @SerializedName("id_agent")
    private Integer id_agent;
    @SerializedName("village_name")
    private String village_name;
    @SerializedName("sub_district_name")
    private String sub_district_name;
    @SerializedName("regency_name")
    private String regency_name;
    @SerializedName("province_name")
    private String province_name;
    @SerializedName("finance_balance")
    private Integer finance_balance;

    public Borrower(Integer id, Integer id_borrower, String gender, Integer borrower_local_id, String birth_date, Integer bank_acc_number, String last_education, Integer is_active, Integer id_village, Integer id_sub_district, String family_phone_number, String family_relationship, String family_name, String name, String email, String phone_number, String domicile_address, String id_card_address, String profil_image, String last_job, Integer income, Integer family_card_id, Integer principal_taxpayer_id, String family_card_image, String local_id_image, String id_selfie_image, String home_image, String pin, String rating, String rating_desc, String salary_slip_image, Integer id_agent, String village_name, String sub_district_name, String regency_name, String province_name, Integer finance_balance) {
        this.id = id;
        this.id_borrower = id_borrower;
        this.gender = gender;
        this.borrower_local_id = borrower_local_id;
        this.birth_date = birth_date;
        this.bank_acc_number = bank_acc_number;
        this.last_education = last_education;
        this.is_active = is_active;
        this.id_village = id_village;
        this.id_sub_district = id_sub_district;
        this.family_phone_number = family_phone_number;
        this.family_relationship = family_relationship;
        this.family_name = family_name;
        this.name = name;
        this.email = email;
        this.phone_number = phone_number;
        this.domicile_address = domicile_address;
        this.id_card_address = id_card_address;
        this.profil_image = profil_image;
        this.last_job = last_job;
        this.income = income;
        this.family_card_id = family_card_id;
        this.principal_taxpayer_id = principal_taxpayer_id;
        this.family_card_image = family_card_image;
        this.local_id_image = local_id_image;
        this.id_selfie_image = id_selfie_image;
        this.home_image = home_image;
        this.pin = pin;
        this.rating = rating;
        this.rating_desc = rating_desc;
        this.salary_slip_image = salary_slip_image;
        this.id_agent = id_agent;
        this.village_name = village_name;
        this.sub_district_name = sub_district_name;
        this.regency_name = regency_name;
        this.province_name = province_name;
        this.finance_balance = finance_balance;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId_borrower() {
        return id_borrower;
    }

    public void setId_borrower(Integer id_borrower) {
        this.id_borrower = id_borrower;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Integer getBorrower_local_id() {
        return borrower_local_id;
    }

    public void setBorrower_local_id(Integer borrower_local_id) {
        this.borrower_local_id = borrower_local_id;
    }

    public String getBirth_date() {
        return birth_date;
    }

    public void setBirth_date(String birth_date) {
        this.birth_date = birth_date;
    }

    public Integer getBank_acc_number() {
        return bank_acc_number;
    }

    public void setBank_acc_number(Integer bank_acc_number) {
        this.bank_acc_number = bank_acc_number;
    }

    public String getLast_education() {
        return last_education;
    }

    public void setLast_education(String last_education) {
        this.last_education = last_education;
    }

    public Integer getIs_active() {
        return is_active;
    }

    public void setIs_active(Integer is_active) {
        this.is_active = is_active;
    }

    public Integer getId_village() {
        return id_village;
    }

    public void setId_village(Integer id_village) {
        this.id_village = id_village;
    }

    public Integer getId_sub_district() {
        return id_sub_district;
    }

    public void setId_sub_district(Integer id_sub_district) {
        this.id_sub_district = id_sub_district;
    }

    public String getFamily_phone_number() {
        return family_phone_number;
    }

    public void setFamily_phone_number(String family_phone_number) {
        this.family_phone_number = family_phone_number;
    }

    public String getFamily_relationship() {
        return family_relationship;
    }

    public void setFamily_relationship(String family_relationship) {
        this.family_relationship = family_relationship;
    }

    public String getFamily_name() {
        return family_name;
    }

    public void setFamily_name(String family_name) {
        this.family_name = family_name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getDomicile_address() {
        return domicile_address;
    }

    public void setDomicile_address(String domicile_address) {
        this.domicile_address = domicile_address;
    }

    public String getId_card_address() {
        return id_card_address;
    }

    public void setId_card_address(String id_card_address) {
        this.id_card_address = id_card_address;
    }

    public String getProfil_image() {
        return profil_image;
    }

    public void setProfil_image(String profil_image) {
        this.profil_image = profil_image;
    }

    public String getLast_job() {
        return last_job;
    }

    public void setLast_job(String last_job) {
        this.last_job = last_job;
    }

    public Integer getIncome() {
        return income;
    }

    public void setIncome(Integer income) {
        this.income = income;
    }

    public Integer getFamily_card_id() {
        return family_card_id;
    }

    public void setFamily_card_id(Integer family_card_id) {
        this.family_card_id = family_card_id;
    }

    public Integer getPrincipal_taxpayer_id() {
        return principal_taxpayer_id;
    }

    public void setPrincipal_taxpayer_id(Integer principal_taxpayer_id) {
        this.principal_taxpayer_id = principal_taxpayer_id;
    }

    public String getFamily_card_image() {
        return family_card_image;
    }

    public void setFamily_card_image(String family_card_image) {
        this.family_card_image = family_card_image;
    }

    public String getLocal_id_image() {
        return local_id_image;
    }

    public void setLocal_id_image(String local_id_image) {
        this.local_id_image = local_id_image;
    }

    public String getId_selfie_image() {
        return id_selfie_image;
    }

    public void setId_selfie_image(String id_selfie_image) {
        this.id_selfie_image = id_selfie_image;
    }

    public String getHome_image() {
        return home_image;
    }

    public void setHome_image(String home_image) {
        this.home_image = home_image;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getRating_desc() {
        return rating_desc;
    }

    public void setRating_desc(String rating_desc) {
        this.rating_desc = rating_desc;
    }

    public String getSalary_slip_image() {
        return salary_slip_image;
    }

    public void setSalary_slip_image(String salary_slip_image) {
        this.salary_slip_image = salary_slip_image;
    }

    public Integer getId_agent() {
        return id_agent;
    }

    public void setId_agent(Integer id_agent) {
        this.id_agent = id_agent;
    }

    public String getVillage_name() {
        return village_name;
    }

    public void setVillage_name(String village_name) {
        this.village_name = village_name;
    }

    public String getSub_district_name() {
        return sub_district_name;
    }

    public void setSub_district_name(String sub_district_name) {
        this.sub_district_name = sub_district_name;
    }

    public String getRegency_name() {
        return regency_name;
    }

    public void setRegency_name(String regency_name) {
        this.regency_name = regency_name;
    }

    public String getProvince_name() {
        return province_name;
    }

    public void setProvince_name(String province_name) {
        this.province_name = province_name;
    }

    public Integer getFinance_balance() {
        return finance_balance;
    }

    public void setFinance_balance(Integer finance_balance) {
        this.finance_balance = finance_balance;
    }
}
