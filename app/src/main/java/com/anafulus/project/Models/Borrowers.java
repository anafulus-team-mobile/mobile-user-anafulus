package com.anafulus.project.Models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Borrowers {
    @SerializedName("error")
    private  boolean err;

    @SerializedName("message")
    private  String msg;

    @SerializedName("data")
    List<Borrower> listDataBorrower;

    @SerializedName("dataLoan")
    List<Loan> listLoan;

    @SerializedName("dataNotification")
    List<Notification> listNotification;

    @SerializedName("dataBankAccount")
    List<BankAccount> listBankAccount;

    @SerializedName("dataProvinces")
    List<Provinces> listDataProvinces;

    @SerializedName("dataRegencies")
    List<Regencies> listDataRegencies;

    @SerializedName("dataSubdistrics")
    List<Subdistrics> listDataSubdistrics;

    @SerializedName("dataVillages")
    List<Villages> listDataVillages;


    public boolean isErr() {
        return err;
    }

    public void setErr(boolean err) {
        this.err = err;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<Borrower> getListDataBorrower() {
        return listDataBorrower;
    }

    public void setListDataBorrower(List<Borrower> listDataBorrower) {
        this.listDataBorrower = listDataBorrower;
    }

    public List<Loan> getListLoan() {
        return listLoan;
    }

    public void setListLoan(List<Loan> listLoan) {
        this.listLoan = listLoan;
    }

    public List<Notification> getListNotification() {
        return listNotification;
    }

    public void setListNotification(List<Notification> listNotification) {
        this.listNotification = listNotification;
    }

    public List<BankAccount> getListBankAccount() {
        return listBankAccount;
    }

    public void setListBankAccount(List<BankAccount> listBankAccount) {
        this.listBankAccount = listBankAccount;
    }

    public List<Provinces> getListDataProvinces() {
        return listDataProvinces;
    }

    public void setListDataProvinces(List<Provinces> listDataProvinces) {
        this.listDataProvinces = listDataProvinces;
    }

    public List<Regencies> getListDataRegencies() {
        return listDataRegencies;
    }

    public void setListDataRegencies(List<Regencies> listDataRegencies) {
        this.listDataRegencies = listDataRegencies;
    }

    public List<Subdistrics> getListDataSubdistrics() {
        return listDataSubdistrics;
    }

    public void setListDataSubdistrics(List<Subdistrics> listDataSubdistrics) {
        this.listDataSubdistrics = listDataSubdistrics;
    }

    public List<Villages> getListDataVillages() {
        return listDataVillages;
    }

    public void setListDataVillages(List<Villages> listDataVillages) {
        this.listDataVillages = listDataVillages;
    }
}
