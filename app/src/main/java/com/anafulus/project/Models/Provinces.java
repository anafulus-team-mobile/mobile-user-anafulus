package com.anafulus.project.Models;

import com.google.gson.annotations.SerializedName;

public class Provinces {
    @SerializedName("id")
    private Integer id;

    @SerializedName("province_name")
    private String provinceName;

    public Provinces() {
    }


    public Provinces(Integer id, String provinceName) {
        this.id = id;
        this.provinceName = provinceName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getProvinceName() {
        return provinceName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }
}
