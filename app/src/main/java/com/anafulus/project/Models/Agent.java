package com.anafulus.project.Models;

import com.anafulus.project.Murabahah.AdapterAgentMurabahah;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Agent  {
    @SerializedName("id")
    private Integer id;
    @SerializedName("agent_code")
    private String agent_code;
    @SerializedName("name")
    private String name;
    @SerializedName("phone_number")
    private String phone_number;

    public Agent() {
    }

    public Agent(Integer id, String agent_code, String name, String phone_number) {
        this.id = id;
        this.agent_code = agent_code;
        this.name = name;
        this.phone_number = phone_number;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAgent_code() {
        return agent_code;
    }

    public void setAgent_code(String agent_code) {
        this.agent_code = agent_code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }
}
