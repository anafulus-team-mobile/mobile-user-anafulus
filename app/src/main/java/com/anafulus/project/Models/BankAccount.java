package com.anafulus.project.Models;

import com.google.gson.annotations.SerializedName;

public class BankAccount {
    @SerializedName("id")
    private Integer id;
    @SerializedName("id_borrower")
    private Integer id_borrower;
    @SerializedName("id_agent")
    private Integer id_agent;
    @SerializedName("id_head_agent")
    private Integer id_head_agent;
    @SerializedName("is_read")
    private String is_read;
    @SerializedName("phone_account_number")
    private Integer phone_account_number;
    @SerializedName("bank_account_number")
    private Integer bank_account_number;
    @SerializedName("bank_account_name")
    private String bank_account_name;
    @SerializedName("bank_name")
    private String bank_name;
    @SerializedName("bank_branch")
    private String bank_branch;


    public BankAccount() {
    }

    public BankAccount(Integer id, Integer id_borrower, Integer id_agent, Integer id_head_agent, String is_read, Integer phone_account_number, Integer bank_account_number, String bank_account_name, String bank_name, String bank_branch) {
        this.id = id;
        this.id_borrower = id_borrower;
        this.id_agent = id_agent;
        this.id_head_agent = id_head_agent;
        this.is_read = is_read;
        this.phone_account_number = phone_account_number;
        this.bank_account_number = bank_account_number;
        this.bank_account_name = bank_account_name;
        this.bank_name = bank_name;
        this.bank_branch = bank_branch;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId_borrower() {
        return id_borrower;
    }

    public void setId_borrower(Integer id_borrower) {
        this.id_borrower = id_borrower;
    }

    public Integer getId_agent() {
        return id_agent;
    }

    public void setId_agent(Integer id_agent) {
        this.id_agent = id_agent;
    }

    public Integer getId_head_agent() {
        return id_head_agent;
    }

    public void setId_head_agent(Integer id_head_agent) {
        this.id_head_agent = id_head_agent;
    }

    public String getIs_read() {
        return is_read;
    }

    public void setIs_read(String is_read) {
        this.is_read = is_read;
    }

    public Integer getPhone_account_number() {
        return phone_account_number;
    }

    public void setPhone_account_number(Integer phone_account_number) {
        this.phone_account_number = phone_account_number;
    }

    public Integer getBank_account_number() {
        return bank_account_number;
    }

    public void setBank_account_number(Integer bank_account_number) {
        this.bank_account_number = bank_account_number;
    }

    public String getBank_account_name() {
        return bank_account_name;
    }

    public void setBank_account_name(String bank_account_name) {
        this.bank_account_name = bank_account_name;
    }

    public String getBank_name() {
        return bank_name;
    }

    public void setBank_name(String bank_name) {
        this.bank_name = bank_name;
    }

    public String getBank_branch() {
        return bank_branch;
    }

    public void setBank_branch(String bank_branch) {
        this.bank_branch = bank_branch;
    }
}
