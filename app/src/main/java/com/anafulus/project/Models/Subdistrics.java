package com.anafulus.project.Models;

import com.google.gson.annotations.SerializedName;

public class Subdistrics {
    @SerializedName("id")
    private Integer id;

    @SerializedName("id_regency")
    private Integer idRegency;

    @SerializedName("sub_district_name")
    private String subDistrictName;

    public Subdistrics() {
    }

    public Subdistrics(Integer id, Integer idRegency, String subDistrictName) {
        this.id = id;
        this.idRegency = idRegency;
        this.subDistrictName = subDistrictName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdRegency() {
        return idRegency;
    }

    public void setIdRegency(Integer idRegency) {
        this.idRegency = idRegency;
    }

    public String getSubDistrictName() {
        return subDistrictName;
    }

    public void setSubDistrictName(String subDistrictName) {
        this.subDistrictName = subDistrictName;
    }
}
