package com.anafulus.project.Models;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class Loan {
    @SerializedName("id")
    private Integer id;
    @SerializedName("loan_principal")
    private Integer loan_principal;
    @SerializedName("is_confirm_agent")
    private Integer is_confirm_agent;
    @SerializedName("is_confirm_first_admin")
    private Integer is_confirm_first_admin;
    @SerializedName("is_confirm_last_admin")
    private Integer is_confirm_last_admin;
    @SerializedName("loan_status")
    private String loan_status;
    @SerializedName("rejected_reason")
    private String rejected_reason;
    @SerializedName("installment_nominal")
    private Integer installment_nominal;
    @SerializedName("tenor")
    private String tenor;
    @SerializedName("installment_type")
    private String installment_type;
    @SerializedName("submition_date")
    private String submission_date;
    @SerializedName("loan_category")
    private String loan_category;
    @SerializedName("loan_type")
    private String loan_type;
    @SerializedName("validation_date")
    private String validation_date;
    @SerializedName("verification_date")
    private String verification_date;
    @SerializedName("approvement_date")
    private String approvement_date;
    @SerializedName("withdraw_fund_date")
    private String withdraw_fund_date;
    @SerializedName("rejection_date")
    private String rejection_date;
    @SerializedName("cancellation_date")
    private String cancellation_date;


    public Loan() {
    }

    public Loan(Integer id, Integer loan_principal, Integer is_confirm_agent, Integer is_confirm_first_admin, Integer is_confirm_last_admin, String loan_status, String rejected_reason, Integer installment_nominal, String tenor, String installment_type, String submission_date, String loan_category, String loan_type, String validation_date, String verification_date, String approvement_date, String withdraw_fund_date, String rejection_date, String cancellation_date) {
        this.id = id;
        this.loan_principal = loan_principal;
        this.is_confirm_agent = is_confirm_agent;
        this.is_confirm_first_admin = is_confirm_first_admin;
        this.is_confirm_last_admin = is_confirm_last_admin;
        this.loan_status = loan_status;
        this.rejected_reason = rejected_reason;
        this.installment_nominal = installment_nominal;
        this.tenor = tenor;
        this.installment_type = installment_type;
        this.submission_date = submission_date;
        this.loan_category = loan_category;
        this.loan_type = loan_type;
        this.validation_date = validation_date;
        this.verification_date = verification_date;
        this.approvement_date = approvement_date;
        this.withdraw_fund_date = withdraw_fund_date;
        this.rejection_date = rejection_date;
        this.cancellation_date = cancellation_date;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getLoan_principal() {
        return loan_principal;
    }

    public void setLoan_principal(Integer loan_principal) {
        this.loan_principal = loan_principal;
    }

    public Integer getIs_confirm_agent() {
        return is_confirm_agent;
    }

    public void setIs_confirm_agent(Integer is_confirm_agent) {
        this.is_confirm_agent = is_confirm_agent;
    }

    public Integer getIs_confirm_first_admin() {
        return is_confirm_first_admin;
    }

    public void setIs_confirm_first_admin(Integer is_confirm_first_admin) {
        this.is_confirm_first_admin = is_confirm_first_admin;
    }

    public Integer getIs_confirm_last_admin() {
        return is_confirm_last_admin;
    }

    public void setIs_confirm_last_admin(Integer is_confirm_last_admin) {
        this.is_confirm_last_admin = is_confirm_last_admin;
    }

    public String getLoan_status() {
        return loan_status;
    }

    public void setLoan_status(String loan_status) {
        this.loan_status = loan_status;
    }

    public String getRejected_reason() {
        return rejected_reason;
    }

    public void setRejected_reason(String rejected_reason) {
        this.rejected_reason = rejected_reason;
    }

    public Integer getInstallment_nominal() {
        return installment_nominal;
    }

    public void setInstallment_nominal(Integer installment_nominal) {
        this.installment_nominal = installment_nominal;
    }

    public String getTenor() {
        return tenor;
    }

    public void setTenor(String tenor) {
        this.tenor = tenor;
    }

    public String getInstallment_type() {
        return installment_type;
    }

    public void setInstallment_type(String installment_type) {
        this.installment_type = installment_type;
    }

    public String getSubmission_date() {
        return submission_date;
    }

    public void setSubmission_date(String submission_date) {
        this.submission_date = submission_date;
    }

    public String getLoan_category() {
        return loan_category;
    }

    public void setLoan_category(String loan_category) {
        this.loan_category = loan_category;
    }

    public String getLoan_type() {
        return loan_type;
    }

    public void setLoan_type(String loan_type) {
        this.loan_type = loan_type;
    }

    public String getValidation_date() {
        return validation_date;
    }

    public void setValidation_date(String validation_date) {
        this.validation_date = validation_date;
    }

    public String getVerification_date() {
        return verification_date;
    }

    public void setVerification_date(String verification_date) {
        this.verification_date = verification_date;
    }

    public String getApprovement_date() {
        return approvement_date;
    }

    public void setApprovement_date(String approvement_date) {
        this.approvement_date = approvement_date;
    }

    public String getWithdraw_fund_date() {
        return withdraw_fund_date;
    }

    public void setWithdraw_fund_date(String withdraw_fund_date) {
        this.withdraw_fund_date = withdraw_fund_date;
    }

    public String getRejection_date() {
        return rejection_date;
    }

    public void setRejection_date(String rejection_date) {
        this.rejection_date = rejection_date;
    }

    public String getCancellation_date() {
        return cancellation_date;
    }

    public void setCancellation_date(String cancellation_date) {
        this.cancellation_date = cancellation_date;
    }
}
