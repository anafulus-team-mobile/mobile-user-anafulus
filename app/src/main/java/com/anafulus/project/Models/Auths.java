package com.anafulus.project.Models;

import com.google.gson.annotations.SerializedName;

public class Auths {
    @SerializedName("error")
    private  boolean err;

    @SerializedName("message")
    private  String msg;

    @SerializedName("id_borrower")
    private  Integer idBorrower;

    public boolean isErr() {
        return err;
    }

    public void setErr(boolean err) {
        this.err = err;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Integer getIdBorrower() {
        return idBorrower;
    }

    public void setIdBorrower(Integer idBorrower) {
        this.idBorrower = idBorrower;
    }
}