package com.anafulus.project.Models;

import com.google.gson.annotations.SerializedName;

public class Villages {
    @SerializedName("id")
    private Integer id;

    @SerializedName("village_name")
    private String villageName;

    public Villages() {
    }

    public Villages(Integer id, String villageName) {
        this.id = id;
        this.villageName = villageName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getVillageName() {
        return villageName;
    }

    public void setVillageName(String villageName) {
        this.villageName = villageName;
    }
}
