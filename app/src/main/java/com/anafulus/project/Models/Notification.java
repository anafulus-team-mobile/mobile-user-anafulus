package com.anafulus.project.Models;

import com.google.gson.annotations.SerializedName;

public class Notification {
    @SerializedName("id")
    private Integer id;
    @SerializedName("id_loan")
    private Integer id_loan;
    @SerializedName("id_borrower")
    private Integer id_borrower;
    @SerializedName("description")
    private String description;
    @SerializedName("is_read")
    private String is_read;
    @SerializedName("updated_at")
    private String updated_at;

    public Notification() {
    }

    public Notification(Integer id, Integer id_loan, Integer id_borrower, String description, String is_read, String updated_at) {
        this.id = id;
        this.id_loan = id_loan;
        this.id_borrower = id_borrower;
        this.description = description;
        this.is_read = is_read;
        this.updated_at = updated_at;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId_loan() {
        return id_loan;
    }

    public void setId_loan(Integer id_loan) {
        this.id_loan = id_loan;
    }

    public Integer getId_borrower() {
        return id_borrower;
    }

    public void setId_borrower(Integer id_borrower) {
        this.id_borrower = id_borrower;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIs_read() {
        return is_read;
    }

    public void setIs_read(String is_read) {
        this.is_read = is_read;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}
