package com.anafulus.project;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.anafulus.project.Murabahah.Murabahah;
import com.anafulus.project.Reguler.Reguler;

import static android.content.Context.MODE_PRIVATE;


public class FragmentReguler extends Fragment {
    private static final String TAG = "FragmentReguler";
    SharedPreferences preferencesLoanActive;

    private Button btnTEST;
    private CardView single, multi;
    private TextView txt_single, txt_multi;
    String string;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_reguler,container,false);
        preferencesLoanActive = getActivity().getSharedPreferences("loan_active",MODE_PRIVATE);

        //CardView Borrow Single
        single = view.findViewById(R.id.single);
        single.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String loanActive= preferencesLoanActive.getString("loanActive",null);
                if (loanActive.equals("true")) {
                    final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    View view = getLayoutInflater().inflate(R.layout.pop_up_can_not_make_loan, null);
                    builder.setView(view);
                    AlertDialog dialog = builder.create();
                    dialog.show();
                } else {
                    Intent i = new Intent(getActivity(), Reguler.class);
                    startActivity(i);
                }
            }
        });

        return view;
    }
    public void showPopup(View anchorView) {

//        final View popupView = getLayoutInflater().inflate(R.layout.pop_up_can_not_make_loan, null);
//
//        final PopupWindow popupWindow = new PopupWindow(popupView, 650 , 370, true);
//        popupWindow.showAtLocation(popupView, Gravity.CENTER, 0, 0);
//        popupWindow.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        // If the PopupWindow should be focusable
//        popupWindow.setFocusable(true);
//
//        // If you need the PopupWindow to dismiss when when touched outside
//
//        int location[] = new int[2];
//
//        // Get the View's(the one that was clicked in the Fragment) location
//        anchorView.getLocationOnScreen(location);
//
//        // Using location, the PopupWindow will be displayed right under anchorView
//        popupWindow.showAtLocation(anchorView, Gravity.NO_GRAVITY,
//                location[0], location[1] + anchorView.getHeight());
//
//        TextView txtClose = popupView.findViewById(R.id.txtclose);
//        txtClose.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View layout) {
//                popupWindow.dismiss();
//            }
//        });
    }
}
