package com.anafulus.project.DetailInstallment;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.anafulus.project.History.PageHistory;
import com.anafulus.project.Payment.Payment;
import com.anafulus.project.R;

import java.text.NumberFormat;
import java.util.Locale;

public class ReasonCancel extends AppCompatActivity {
    private TextView txtPenalty, txtReason ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reason_cancel);

        //Toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Membatalkan Pinjaman");
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ReasonCancel.this, NotYet.class);
                i.putExtra("loanStatus", getIntent().getExtras().getString("loanStatus"));
                startActivity(i);
                finish();
            }
        });
        
        String loanStatus = getIntent().getExtras().getString("loanStatus");
        String loanPrincipal = getIntent().getExtras().getString("loanPrincipal").substring(1);
        String doubleRupiah = loanPrincipal.substring(1).replaceAll("[-+.^:,]", "");

        //Perhitungan denda
        String penalty = "2%";
        double totalPenalty = 0.02 * Double.parseDouble(doubleRupiah);

        Locale localeID = new Locale("in", "ID");
        NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);

        final String strTotalPenalty = formatRupiah.format(totalPenalty);
        txtPenalty = findViewById(R.id.txtPenalty);
        txtReason = findViewById(R.id.reason);
        txtPenalty.setText("Untuk membatalkan pengajuan pinjaman anda akan dikenakan denda sebesar " + penalty + " dari pinjaman anda, yaitu " + strTotalPenalty);

        Button buttonPayment = findViewById(R.id.payment);
        buttonPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtReason.getText().toString().isEmpty()){
                    txtReason.setError("Berikan alasan anda");
                } else {
                    Intent i = new Intent(ReasonCancel.this, Payment.class);
                    i.putExtra("idBorrower", getIntent().getExtras().getString("idBorrower"));
                    i.putExtra("totalPenalty", strTotalPenalty);
                    i.putExtra("reason", txtReason.getText().toString());
                    startActivity(i);
                    finish();
                }
            }
        });

    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(ReasonCancel.this, NotYet.class);
        i.putExtra("loanStatus", getIntent().getExtras().getString("loanStatus"));
        startActivity(i);
        finish();
    }
}
