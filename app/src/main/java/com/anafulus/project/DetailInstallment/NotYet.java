package com.anafulus.project.DetailInstallment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.anafulus.project.Config.BorrowerApi;
import com.anafulus.project.Config.RetrofitClient;
import com.anafulus.project.History.PageHistory;
import com.anafulus.project.Models.Borrowers;
import com.anafulus.project.R;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class NotYet extends AppCompatActivity {
    BorrowerApi mApiInterface;
    SharedPreferences preferences;

    private Toolbar toolbar;
    private RelativeLayout indicator5;
    private TextView txtTitileProcess, txtTitleWithdraw, txtTitleVerification, txtTitleValidation, txtInformation, txtTitleTextInformation, txtLoanPrincipal, txtloanStatus, txtTitleDetail, txtDateUploadFile, txtDateValidate, txtDateVerification, txtDateProcess, txtDateWithdraw;
    private Button btnPayment, btnAggree, btnCancel, btnWithdrawFunds;
    private RelativeLayout layoutExplanation, layoutInformation, layoutIndicatorWithdraw;
    private ImageView imgWithDraw, imgProcess, imgVerification, imgValidate;
    private View shapeProcess;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.not_yet);

        preferences = getSharedPreferences("borrower_profil",MODE_PRIVATE);
        mApiInterface = RetrofitClient.getClient().create(BorrowerApi.class);
        String idBorrower = preferences.getString("idBorrower",null);
        String loanStatus = getIntent().getExtras().getString("loanStatus");

        btnPayment = findViewById(R.id.btn_pay);
        btnAggree = findViewById(R.id.btn_agree);
        btnCancel = findViewById(R.id.btn_cancel);
        btnWithdrawFunds = findViewById(R.id.withdraw_funds);
        layoutExplanation = findViewById(R.id.layout_explanation);
        layoutInformation = findViewById(R.id.layout_information);
        txtTitleDetail = findViewById(R.id.titleDetail);
        txtLoanPrincipal = findViewById(R.id.loanPrincipal);
        txtloanStatus = findViewById(R.id.loanStatus);
        txtInformation = findViewById(R.id.information);
        txtTitleTextInformation = findViewById(R.id.text_information);
        //
        txtTitleValidation = findViewById(R.id.validate);
        imgValidate = findViewById(R.id.img_validate);
        txtTitleVerification = findViewById(R.id.verification);
        txtDateUploadFile = findViewById(R.id.dateUploadFile);
        txtDateValidate = findViewById(R.id.date_validate);
        txtDateVerification = findViewById(R.id.date_verification);
        imgVerification = findViewById(R.id.img_verification);

        shapeProcess = findViewById(R.id.shape_process);
        txtTitileProcess = findViewById(R.id.process);
        txtDateProcess = findViewById(R.id.date_process);
        imgProcess = findViewById(R.id.img_process);

        txtTitleWithdraw = findViewById(R.id.withdraw);
        txtDateWithdraw = findViewById(R.id.date_withdraw);
        layoutIndicatorWithdraw = findViewById(R.id.indicator5);
        imgWithDraw = findViewById(R.id.img_withdraw);

        if((loanStatus.equals("Cicilan sedang berjalan")) || (loanStatus.equals("Belum disetujui")) || (loanStatus.equals("Pinjaman disetujui"))) {
            getDataLoan(idBorrower, loanStatus);
        } else {
            String idLoan = getIntent().getExtras().getString("idLoan");
            getHistoryDataLoan(idBorrower, idLoan, loanStatus);
        }

        //Toolbar
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Detail Pinjaman");
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(NotYet.this, PageHistory.class);
                startActivity(i);
                finish();
            }
        });

        //BUTTON CANCEL
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(NotYet.this);
                View view = getLayoutInflater().inflate(R.layout.popup_cancel, null);
                Button buttonNo = view.findViewById(R.id.btn_no);
                Button buttonYes = view.findViewById(R.id.btn_yes);
                buttonNo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(getApplicationContext(), NotYet.class);
                        i.putExtra("idBorrower", preferences.getString("idBorrower",null));
                        i.putExtra("loanStatus", getIntent().getExtras().getString("loanStatus"));
                        startActivity(i);
                        finish();
                    }
                });
                buttonYes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(NotYet.this, ReasonCancel.class);
                        i.putExtra("idBorrower", preferences.getString("idBorrower",null));
                        i.putExtra("loanStatus", getIntent().getExtras().getString("loanStatus"));
                        i.putExtra("loanPrincipal", txtLoanPrincipal.getText().toString());
                        startActivity(i);
                        finish();
                    }
                });

                builder.setView(view);
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(NotYet.this, PageHistory.class);
        startActivity(i);
        finish();
    }
    private void getDataLoan(String idBorrower, String loanStatus) {
        final String statusLoan = loanStatus;
        Call<Borrowers> getDataLoan = mApiInterface.getLatestLoanByStatus(idBorrower, loanStatus);
        getDataLoan.enqueue(new Callback<Borrowers>() {
            @Override
            public void onResponse(Response<Borrowers> response, Retrofit retrofit) {
                int i =0 ;
                TextView txtIdLoan = findViewById(R.id.idLoan);
                TextView txtInstallment = findViewById(R.id.installmentNominal);
                TextView txtTenor = findViewById(R.id.tenor);
                TextView txtInstallmentType = findViewById(R.id.installmentType);
                TextView txtSubmissionDate = findViewById(R.id.submissionDate);
                TextView txtDescription = findViewById(R.id.description);
                TextView txtLoanType = findViewById(R.id.loanType);
                TextView txtloanStatus = findViewById(R.id.loanStatus);


                Locale localeID = new Locale("in", "ID");
                NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);

                txtIdLoan.setText(response.body().getListLoan().get(i).getId().toString());
                txtLoanPrincipal.setText(formatRupiah.format((double)response.body().getListLoan().get(i).getLoan_principal()));
                if (response.body().getListLoan().get(i).getInstallment_type().equals("Mingguan")) {
                    txtInstallment.setText(formatRupiah.format((double)response.body().getListLoan().get(i).getInstallment_nominal()) + " / " + "Minggu");
                } else {
                    txtInstallment.setText(formatRupiah.format((double)response.body().getListLoan().get(i).getInstallment_nominal()) + " / " + "Bulan");
                }
                txtTenor.setText(response.body().getListLoan().get(i).getTenor());
                txtInstallmentType.setText(response.body().getListLoan().get(i).getInstallment_type());
                txtloanStatus.setText(response.body().getListLoan().get(i).getLoan_status());
                txtSubmissionDate.setText(response.body().getListLoan().get(i).getSubmission_date().toString());
                txtDescription.setText(response.body().getListLoan().get(i).getLoan_category());
                txtLoanType.setText(response.body().getListLoan().get(i).getLoan_type());
                if(statusLoan.equals("Belum disetujui")){
                    if((response.body().getListLoan().get(i).getIs_confirm_agent() == null) && (response.body().getListLoan().get(i).getIs_confirm_first_admin() == null) && (response.body().getListLoan().get(i).getIs_confirm_last_admin() == null)){
                        btnPayment.setVisibility(View.GONE);
                        btnAggree.setVisibility(View.GONE);
                        btnCancel.setVisibility(View.VISIBLE);
                        btnWithdrawFunds.setVisibility(View.GONE);
                        layoutExplanation.setVisibility(View.GONE);
                        layoutInformation.setVisibility(View.GONE);
                        txtTitleDetail.setVisibility(View.GONE);
                        txtDateUploadFile.setText(changeDateFormat("yyyy-MM-dd hh:mm:ss","dd MMM yyyy hh:mm",response.body().getListLoan().get(i).getSubmission_date()));
                        txtDateValidate.setText("-");
                        txtTitleValidation.setText("Validasi Data");
                        txtDateValidate.setTextColor(getResources().getColor(R.color.grey));
                        txtTitleValidation.setTextColor(getResources().getColor(R.color.grey));
                        imgValidate.setImageResource(R.drawable.circle_process_grey);

                        txtDateVerification.setText("-");
                        txtTitleVerification.setText("Verifikasi Data");
                        txtDateVerification.setTextColor(getResources().getColor(R.color.grey));
                        txtTitleVerification.setTextColor(getResources().getColor(R.color.grey));
                        imgVerification.setImageResource(R.drawable.circle_process_grey);

                        txtDateProcess.setText("-");
                        txtTitileProcess.setText("Proses disetujui");
                        txtDateProcess.setTextColor(getResources().getColor(R.color.grey));
                        txtTitileProcess.setTextColor(getResources().getColor(R.color.grey));
                        imgProcess.setImageResource(R.drawable.circle_process_grey);

                        txtDateWithdraw.setText("-");
                        txtTitleWithdraw.setText("Pencairan Dana");
                        txtTitleWithdraw.setTextColor(getResources().getColor(R.color.grey));
                        txtDateWithdraw.setTextColor(getResources().getColor(R.color.grey));
                        imgWithDraw.setImageResource(R.drawable.circle_process_grey);

                        layoutExplanation.setVisibility(View.VISIBLE);
                        txtTitleTextInformation.setText("Keterangan");
                        txtInformation.setText("Menunggu persetujuan agent");

                    } else if ((response.body().getListLoan().get(i).getIs_confirm_agent() == 1) && (response.body().getListLoan().get(i).getIs_confirm_first_admin() == null) && (response.body().getListLoan().get(i).getIs_confirm_last_admin() == null)){
                        btnPayment.setVisibility(View.GONE);
                        btnAggree.setVisibility(View.GONE);
                        btnCancel.setVisibility(View.VISIBLE);
                        btnWithdrawFunds.setVisibility(View.GONE);
                        layoutExplanation.setVisibility(View.GONE);
                        layoutInformation.setVisibility(View.GONE);
                        txtTitleDetail.setVisibility(View.GONE);

                        txtDateUploadFile.setText(changeDateFormat("yyyy-MM-dd hh:mm:ss","dd MMM yyyy hh:mm",response.body().getListLoan().get(i).getSubmission_date()));

                        txtDateValidate.setText(changeDateFormat("yyyy-MM-dd hh:mm:ss","dd MMM yyyy hh:mm",response.body().getListLoan().get(i).getValidation_date()));
                        txtTitleValidation.setText("Validasi Data");

                        txtDateVerification.setText("-");
                        txtTitleVerification.setText("Verifikasi Data");
                        txtDateVerification.setTextColor(getResources().getColor(R.color.grey));
                        txtTitleVerification.setTextColor(getResources().getColor(R.color.grey));
                        imgVerification.setImageResource(R.drawable.circle_process_grey);

                        txtDateProcess.setText("-");
                        txtTitileProcess.setText("Proses disetujui");
                        txtDateProcess.setTextColor(getResources().getColor(R.color.grey));
                        txtTitileProcess.setTextColor(getResources().getColor(R.color.grey));
                        imgProcess.setImageResource(R.drawable.circle_process_grey);

                        txtDateWithdraw.setText("-");
                        txtTitleWithdraw.setText("Pencairan Dana");
                        txtTitleWithdraw.setTextColor(getResources().getColor(R.color.grey));
                        txtDateWithdraw.setTextColor(getResources().getColor(R.color.grey));
                        imgWithDraw.setImageResource(R.drawable.circle_process_grey);

                        layoutExplanation.setVisibility(View.VISIBLE);
                        txtTitleTextInformation.setText("Keterangan");
                        txtInformation.setText("Menunggu persetujuan admin");
                    } else if ((response.body().getListLoan().get(i).getIs_confirm_agent() == 1) && (response.body().getListLoan().get(i).getIs_confirm_first_admin() == 1) && (response.body().getListLoan().get(i).getIs_confirm_last_admin() == null)){
                        btnPayment.setVisibility(View.GONE);
                        btnAggree.setVisibility(View.GONE);
                        btnCancel.setVisibility(View.VISIBLE);
                        btnWithdrawFunds.setVisibility(View.GONE);
                        layoutExplanation.setVisibility(View.GONE);
                        layoutInformation.setVisibility(View.GONE);
                        txtTitleDetail.setVisibility(View.GONE);

                        txtDateUploadFile.setText(changeDateFormat("yyyy-MM-dd hh:mm:ss","dd MMM yyyy hh:mm",response.body().getListLoan().get(i).getSubmission_date()));

                        txtDateValidate.setText(changeDateFormat("yyyy-MM-dd hh:mm:ss","dd MMM yyyy hh:mm",response.body().getListLoan().get(i).getValidation_date()));
                        txtTitleValidation.setText("Validasi Data");

                        txtDateVerification.setText(changeDateFormat("yyyy-MM-dd hh:mm:ss","dd MMM yyyy hh:mm",response.body().getListLoan().get(i).getVerification_date()));
                        txtTitleVerification.setText("Verifikasi Data");

                        txtDateProcess.setText("-");
                        txtTitileProcess.setText("Proses disetujui");
                        txtDateProcess.setTextColor(getResources().getColor(R.color.grey));
                        txtTitileProcess.setTextColor(getResources().getColor(R.color.grey));
                        imgProcess.setImageResource(R.drawable.circle_process_grey);

                        txtDateWithdraw.setText("-");
                        txtTitleWithdraw.setText("Pencairan Dana");
                        txtTitleWithdraw.setTextColor(getResources().getColor(R.color.grey));
                        txtDateWithdraw.setTextColor(getResources().getColor(R.color.grey));
                        imgWithDraw.setImageResource(R.drawable.circle_process_grey);

                        layoutExplanation.setVisibility(View.VISIBLE);
                        txtTitleTextInformation.setText("Keterangan");
                        txtInformation.setText("Menunggu persetujuan admin");
                    }
                } else if(statusLoan.equals("Pinjaman disetujui")){
                    btnPayment.setVisibility(View.GONE);
                    btnAggree.setVisibility(View.GONE);
                    btnCancel.setVisibility(View.VISIBLE);
                    btnWithdrawFunds.setVisibility(View.VISIBLE);
                    layoutExplanation.setVisibility(View.GONE);
                    layoutInformation.setVisibility(View.GONE);
                    txtTitleDetail.setVisibility(View.GONE);
                    txtloanStatus.setTextColor(getResources().getColor(R.color.green));
                    txtDateUploadFile.setText(changeDateFormat("yyyy-MM-dd hh:mm:ss","dd MMM yyyy hh:mm",response.body().getListLoan().get(i).getSubmission_date()));
                    txtDateValidate.setText(changeDateFormat("yyyy-MM-dd hh:mm:ss","dd MMM yyyy hh:mm",response.body().getListLoan().get(i).getValidation_date()));
                    txtDateVerification.setText(changeDateFormat("yyyy-MM-dd hh:mm:ss","dd MMM yyyy hh:mm",response.body().getListLoan().get(i).getVerification_date()));
                    txtDateWithdraw.setText("-");
                    txtTitleWithdraw.setText("Pencairan Dana");
                    txtTitleWithdraw.setTextColor(getResources().getColor(R.color.grey));
                    txtDateWithdraw.setTextColor(getResources().getColor(R.color.grey));
                    imgWithDraw.setImageResource(R.drawable.circle_process_grey);
                    layoutExplanation.setVisibility(View.VISIBLE);
                    txtTitleTextInformation.setText("Keterangan");
                    txtInformation.setText("-");

                }
            }
            @Override
            public void onFailure(Throwable t) {
                Log.d("AKSES KE SERVER GAGAL",t.getMessage());
                Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
            }
        });
    }
    private void getHistoryDataLoan(String idBorrower, String idLoan, final String loanStatus) {
        final  String statusLoan = loanStatus;
        Call<Borrowers> getDataLoan = mApiInterface.getHistoryLoanByStatus(idBorrower, Integer.parseInt(idLoan), loanStatus);
        getDataLoan.enqueue(new Callback<Borrowers>() {
            @Override
            public void onResponse(Response<Borrowers> response, Retrofit retrofit) {
                int i =0 ;

                TextView txtIdLoan = findViewById(R.id.idLoan);
                TextView txtInstallment = findViewById(R.id.installmentNominal);
                TextView txtTenor = findViewById(R.id.tenor);
                TextView txtInstallmentType = findViewById(R.id.installmentType);
                TextView txtSubmissionDate = findViewById(R.id.submissionDate);
                TextView txtDescription = findViewById(R.id.description);
                TextView txtLoanType = findViewById(R.id.loanType);

                Locale localeID = new Locale("in", "ID");
                NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);

                txtIdLoan.setText(response.body().getListLoan().get(i).getId().toString());
                txtLoanPrincipal.setText(formatRupiah.format((double)response.body().getListLoan().get(i).getLoan_principal()));
                if (response.body().getListLoan().get(i).getInstallment_type().equals("Mingguan")) {
                    txtInstallment.setText(formatRupiah.format((double)response.body().getListLoan().get(i).getInstallment_nominal()) + " / " + "Minggu");
                } else {
                    txtInstallment.setText(formatRupiah.format((double)response.body().getListLoan().get(i).getInstallment_nominal()) + " / " + "Bulan");
                }
                txtTenor.setText(response.body().getListLoan().get(i).getTenor());
                txtInstallmentType.setText(response.body().getListLoan().get(i).getInstallment_type());
                txtloanStatus.setText(response.body().getListLoan().get(i).getLoan_status());
                txtSubmissionDate.setText(changeDateFormat("yyyy-MM-dd hh:mm:ss","dd MMM yyyy hh:mm:ss",response.body().getListLoan().get(i).getSubmission_date()));
                txtDescription.setText(response.body().getListLoan().get(i).getLoan_category());
                txtLoanType.setText(response.body().getListLoan().get(i).getLoan_type());

                if(statusLoan.equals("Pinjaman sudah selesai")){
                    btnPayment.setVisibility(View.GONE);
                    btnAggree.setVisibility(View.GONE);
                    btnCancel.setVisibility(View.GONE);
                    btnWithdrawFunds.setVisibility(View.GONE);
                    layoutExplanation.setVisibility(View.GONE);
                    layoutInformation.setVisibility(View.GONE);
                    txtTitleDetail.setVisibility(View.GONE);
                    txtloanStatus.setTextColor(getResources().getColor(R.color.green));
                    txtDateUploadFile.setText(changeDateFormat("yyyy-MM-dd hh:mm:ss","dd MMM yyyy hh:mm",response.body().getListLoan().get(i).getSubmission_date()));
                    txtDateValidate.setText(changeDateFormat("yyyy-MM-dd hh:mm:ss","dd MMM yyyy hh:mm",response.body().getListLoan().get(i).getValidation_date()));
                    txtDateVerification.setText(changeDateFormat("yyyy-MM-dd hh:mm:ss","dd MMM yyyy hh:mm",response.body().getListLoan().get(i).getVerification_date()));
                    txtDateProcess.setText(changeDateFormat("yyyy-MM-dd hh:mm:ss","dd MMM yyyy hh:mm",response.body().getListLoan().get(i).getApprovement_date()));
                    txtDateWithdraw.setText(changeDateFormat("yyyy-MM-dd hh:mm:ss","dd MMM yyyy hh:mm",response.body().getListLoan().get(i).getWithdraw_fund_date()));
                } else if (statusLoan.equals("Pinjaman ditolak")){
                    if((response.body().getListLoan().get(i).getIs_confirm_first_admin() == 0) && (response.body().getListLoan().get(i).getIs_confirm_first_admin() == 0) && (response.body().getListLoan().get(i).getIs_confirm_last_admin() == null)) {
                        btnPayment.setVisibility(View.GONE);
                        btnAggree.setVisibility(View.GONE);
                        btnCancel.setVisibility(View.GONE);
                        btnWithdrawFunds.setVisibility(View.GONE);
                        layoutExplanation.setVisibility(View.GONE);
                        layoutInformation.setVisibility(View.GONE);
                        txtTitleDetail.setVisibility(View.GONE);
                        txtloanStatus.setTextColor(getResources().getColor(R.color.green));
                        txtDateUploadFile.setText(changeDateFormat("yyyy-MM-dd hh:mm:ss","dd MMM yyyy hh:mm",response.body().getListLoan().get(i).getSubmission_date()));
                        txtDateValidate.setText(changeDateFormat("yyyy-MM-dd hh:mm:ss","dd MMM yyyy hh:mm",response.body().getListLoan().get(i).getRejection_date()));
                        txtTitleValidation.setText("Validasi Data - Pinjaman Ditolak");
                        txtTitleValidation.setTextColor(getResources().getColor(R.color.red));
                        imgValidate.setImageResource(R.drawable.circle_process_red);

                        txtDateVerification.setText("-");
                        txtTitleVerification.setText("Verifikasi Data");
                        txtDateVerification.setTextColor(getResources().getColor(R.color.grey));
                        txtTitleVerification.setTextColor(getResources().getColor(R.color.grey));
                        imgVerification.setImageResource(R.drawable.circle_process_grey);

                        txtDateProcess.setText("-");
                        txtTitileProcess.setText("Proses disetujui");
                        txtDateProcess.setTextColor(getResources().getColor(R.color.grey));
                        txtTitileProcess.setTextColor(getResources().getColor(R.color.grey));
                        imgProcess.setImageResource(R.drawable.circle_process_grey);

                        txtDateWithdraw.setText("-");
                        txtTitleWithdraw.setText("Pencairan Dana");
                        txtTitleWithdraw.setTextColor(getResources().getColor(R.color.grey));
                        txtDateWithdraw.setTextColor(getResources().getColor(R.color.grey));
                        imgWithDraw.setImageResource(R.drawable.circle_process_grey);

                        layoutExplanation.setVisibility(View.VISIBLE);
                        txtTitleTextInformation.setText("Keterangan");
                        txtInformation.setText(response.body().getListLoan().get(i).getRejected_reason());
                    } else if((response.body().getListLoan().get(i).getIs_confirm_agent() == 1) && (response.body().getListLoan().get(i).getIs_confirm_first_admin() == 0) && (response.body().getListLoan().get(i).getIs_confirm_last_admin() == null)) {
                        btnPayment.setVisibility(View.GONE);
                        btnAggree.setVisibility(View.GONE);
                        btnCancel.setVisibility(View.GONE);
                        btnWithdrawFunds.setVisibility(View.GONE);
                        layoutExplanation.setVisibility(View.GONE);
                        layoutInformation.setVisibility(View.GONE);
                        txtTitleDetail.setVisibility(View.GONE);
                        txtloanStatus.setTextColor(getResources().getColor(R.color.green));
                        txtDateUploadFile.setText(changeDateFormat("yyyy-MM-dd hh:mm:ss","dd MMM yyyy hh:mm",response.body().getListLoan().get(i).getSubmission_date()));

                        txtDateValidate.setText(changeDateFormat("yyyy-MM-dd hh:mm:ss","dd MMM yyyy hh:mm",response.body().getListLoan().get(i).getValidation_date()));
                        txtTitleValidation.setText("Verifikasi Data");

                        txtDateVerification.setText(changeDateFormat("yyyy-MM-dd hh:mm:ss","dd MMM yyyy hh:mm",response.body().getListLoan().get(i).getRejection_date()));
                        txtTitleVerification.setText("Verifikasi Data - Pinjaman Ditolak");
                        txtTitleVerification.setTextColor(getResources().getColor(R.color.red));
                        imgVerification.setImageResource(R.drawable.circle_process_red);

                        txtDateProcess.setText("-");
                        txtTitileProcess.setText("Proses disetujui");
                        txtDateProcess.setTextColor(getResources().getColor(R.color.grey));
                        txtTitileProcess.setTextColor(getResources().getColor(R.color.grey));
                        imgProcess.setImageResource(R.drawable.circle_process_grey);

                        txtDateWithdraw.setText("-");
                        txtTitleWithdraw.setText("Pencairan Dana");
                        txtTitleWithdraw.setTextColor(getResources().getColor(R.color.grey));
                        txtDateWithdraw.setTextColor(getResources().getColor(R.color.grey));
                        imgWithDraw.setImageResource(R.drawable.circle_process_grey);

                        layoutExplanation.setVisibility(View.VISIBLE);
                        txtTitleTextInformation.setText("Keterangan");
                        txtInformation.setText(response.body().getListLoan().get(i).getRejected_reason());
                    } else if((response.body().getListLoan().get(i).getIs_confirm_agent() == 1) && (response.body().getListLoan().get(i).getIs_confirm_first_admin() == 1) && (response.body().getListLoan().get(i).getIs_confirm_last_admin() == 0)) {
                        btnPayment.setVisibility(View.GONE);
                        btnAggree.setVisibility(View.GONE);
                        btnCancel.setVisibility(View.GONE);
                        btnWithdrawFunds.setVisibility(View.GONE);
                        layoutExplanation.setVisibility(View.GONE);
                        layoutInformation.setVisibility(View.GONE);
                        txtTitleDetail.setVisibility(View.GONE);
                        txtloanStatus.setTextColor(getResources().getColor(R.color.green));
                        txtDateUploadFile.setText(changeDateFormat("yyyy-MM-dd hh:mm:ss","dd MMM yyyy hh:mm",response.body().getListLoan().get(i).getSubmission_date()));

                        txtDateValidate.setText(changeDateFormat("yyyy-MM-dd hh:mm:ss","dd MMM yyyy hh:mm",response.body().getListLoan().get(i).getValidation_date()));
                        txtTitleValidation.setText("Validasi Data");

                        txtDateVerification.setText(changeDateFormat("yyyy-MM-dd hh:mm:ss","dd MMM yyyy hh:mm",response.body().getListLoan().get(i).getVerification_date()));
                        txtTitleVerification.setText("Verifikasi Data");

                        txtDateProcess.setText(changeDateFormat("yyyy-MM-dd hh:mm:ss","dd MMM yyyy hh:mm",response.body().getListLoan().get(i).getRejection_date()));
                        txtTitileProcess.setText("Proses disetujui - Pinjaman Ditolak");
                        txtTitileProcess.setTextColor(getResources().getColor(R.color.red));
                        imgProcess.setImageResource(R.drawable.circle_process_red);

                        txtDateWithdraw.setText("-");
                        txtTitleWithdraw.setText("Pencairan Dana");
                        txtTitleWithdraw.setTextColor(getResources().getColor(R.color.grey));
                        txtDateWithdraw.setTextColor(getResources().getColor(R.color.grey));
                        imgWithDraw.setImageResource(R.drawable.circle_process_grey);

                        layoutExplanation.setVisibility(View.VISIBLE);
                        txtTitleTextInformation.setText("Keterangan");
                        txtInformation.setText(response.body().getListLoan().get(i).getRejected_reason());
                    }
                }else if (statusLoan.equals("Pinjaman dibatalkan")){
                    Log.d("Statusnya ketiga ", loanStatus);
                    if((response.body().getListLoan().get(i).getIs_confirm_agent() == 1) && (response.body().getListLoan().get(i).getIs_confirm_first_admin() == null) && (response.body().getListLoan().get(i).getIs_confirm_last_admin() == null)) {
                        btnPayment.setVisibility(View.GONE);
                        btnAggree.setVisibility(View.GONE);
                        btnCancel.setVisibility(View.GONE);
                        btnWithdrawFunds.setVisibility(View.GONE);
                        layoutExplanation.setVisibility(View.GONE);
                        layoutInformation.setVisibility(View.GONE);
                        txtTitleDetail.setVisibility(View.GONE);
                        txtloanStatus.setTextColor(getResources().getColor(R.color.green));
                        txtDateUploadFile.setText(changeDateFormat("yyyy-MM-dd hh:mm:ss","dd MMM yyyy hh:mm",response.body().getListLoan().get(i).getSubmission_date()));
                        txtDateValidate.setText(changeDateFormat("yyyy-MM-dd hh:mm:ss","dd MMM yyyy hh:mm",response.body().getListLoan().get(i).getCancellation_date()));
                        txtTitleValidation.setText("Validasi Data - Pinjaman Dibatalkan");
                        txtTitleValidation.setTextColor(getResources().getColor(R.color.yellow));
                        imgValidate.setImageResource(R.drawable.circle_process_yellow);

                        txtDateVerification.setText("-");
                        txtTitleVerification.setText("Verifikasi Data");
                        txtDateVerification.setTextColor(getResources().getColor(R.color.grey));
                        txtTitleVerification.setTextColor(getResources().getColor(R.color.grey));
                        imgVerification.setImageResource(R.drawable.circle_process_grey);

                        txtDateProcess.setText("-");
                        txtTitileProcess.setText("Proses disetujui");
                        txtDateProcess.setTextColor(getResources().getColor(R.color.grey));
                        txtTitileProcess.setTextColor(getResources().getColor(R.color.grey));
                        imgProcess.setImageResource(R.drawable.circle_process_grey);

                        txtDateWithdraw.setText("-");
                        txtTitleWithdraw.setText("Pencairan Dana");
                        txtTitleWithdraw.setTextColor(getResources().getColor(R.color.grey));
                        txtDateWithdraw.setTextColor(getResources().getColor(R.color.grey));
                        imgWithDraw.setImageResource(R.drawable.circle_process_grey);

                        layoutExplanation.setVisibility(View.VISIBLE);
                        txtTitleTextInformation.setText("Keterangan");
                        txtInformation.setText("-");
                    } else if((response.body().getListLoan().get(i).getIs_confirm_agent() == 1) && (response.body().getListLoan().get(i).getIs_confirm_first_admin() == 1) && (response.body().getListLoan().get(i).getIs_confirm_last_admin() == null)) {
                        btnPayment.setVisibility(View.GONE);
                        btnAggree.setVisibility(View.GONE);
                        btnCancel.setVisibility(View.GONE);
                        btnWithdrawFunds.setVisibility(View.GONE);
                        layoutExplanation.setVisibility(View.GONE);
                        layoutInformation.setVisibility(View.GONE);
                        txtTitleDetail.setVisibility(View.GONE);
                        txtloanStatus.setTextColor(getResources().getColor(R.color.green));
                        txtDateUploadFile.setText(changeDateFormat("yyyy-MM-dd hh:mm:ss","dd MMM yyyy hh:mm",response.body().getListLoan().get(i).getSubmission_date()));

                        txtDateValidate.setText(changeDateFormat("yyyy-MM-dd hh:mm:ss","dd MMM yyyy hh:mm",response.body().getListLoan().get(i).getValidation_date()));
                        txtTitleValidation.setText("Verifikasi Data");

                        txtDateVerification.setText(changeDateFormat("yyyy-MM-dd hh:mm:ss","dd MMM yyyy hh:mm",response.body().getListLoan().get(i).getCancellation_date()));
                        txtTitleVerification.setText("Verifikasi Data - Pinjaman dibatalkan");
                        txtTitleVerification.setTextColor(getResources().getColor(R.color.yellow));
                        imgVerification.setImageResource(R.drawable.circle_process_yellow);

                        txtDateProcess.setText("-");
                        txtTitileProcess.setText("Proses disetujui");
                        txtDateProcess.setTextColor(getResources().getColor(R.color.grey));
                        txtTitileProcess.setTextColor(getResources().getColor(R.color.grey));
                        imgProcess.setImageResource(R.drawable.circle_process_grey);

                        txtDateWithdraw.setText("-");
                        txtTitleWithdraw.setText("Pencairan Dana");
                        txtTitleWithdraw.setTextColor(getResources().getColor(R.color.grey));
                        txtDateWithdraw.setTextColor(getResources().getColor(R.color.grey));
                        imgWithDraw.setImageResource(R.drawable.circle_process_grey);

                        layoutExplanation.setVisibility(View.VISIBLE);
                        txtTitleTextInformation.setText("Keterangan");
                        txtInformation.setText("-");
                    } else if((response.body().getListLoan().get(i).getIs_confirm_agent() == 1) && (response.body().getListLoan().get(i).getIs_confirm_first_admin() == 1) && (response.body().getListLoan().get(i).getIs_confirm_last_admin() == 1)) {
                        btnPayment.setVisibility(View.GONE);
                        btnAggree.setVisibility(View.GONE);
                        btnCancel.setVisibility(View.GONE);
                        btnWithdrawFunds.setVisibility(View.GONE);
                        layoutExplanation.setVisibility(View.GONE);
                        layoutInformation.setVisibility(View.GONE);
                        txtTitleDetail.setVisibility(View.GONE);
                        txtloanStatus.setTextColor(getResources().getColor(R.color.green));
                        txtDateUploadFile.setText(changeDateFormat("yyyy-MM-dd hh:mm:ss","dd MMM yyyy hh:mm",response.body().getListLoan().get(i).getSubmission_date()));

                        txtDateValidate.setText(changeDateFormat("yyyy-MM-dd hh:mm:ss","dd MMM yyyy hh:mm",response.body().getListLoan().get(i).getValidation_date()));
                        txtTitleValidation.setText("Validasi Data");

                        txtDateVerification.setText(changeDateFormat("yyyy-MM-dd hh:mm:ss","dd MMM yyyy hh:mm",response.body().getListLoan().get(i).getVerification_date()));
                        txtTitleVerification.setText("Verifikasi Data");

                        txtDateProcess.setText(changeDateFormat("yyyy-MM-dd hh:mm:ss","dd MMM yyyy hh:mm",response.body().getListLoan().get(i).getCancellation_date()));
                        txtTitileProcess.setText("Proses disetujui - Pinjaman Dibatalkan");
                        txtTitileProcess.setTextColor(getResources().getColor(R.color.yellow));
                        imgProcess.setImageResource(R.drawable.circle_process_yellow);

                        txtDateWithdraw.setText("-");
                        txtTitleWithdraw.setText("Pencairan Dana");
                        txtTitleWithdraw.setTextColor(getResources().getColor(R.color.grey));
                        txtDateWithdraw.setTextColor(getResources().getColor(R.color.grey));
                        imgWithDraw.setImageResource(R.drawable.circle_process_grey);

                        layoutExplanation.setVisibility(View.VISIBLE);
                        txtTitleTextInformation.setText("Keterangan");
                        txtInformation.setText("-");
                    }
                }
            }
            @Override
            public void onFailure(Throwable t) {
                Log.d("AKSES KE SERVER GAGAL",t.getMessage());
                Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
            }
        });
    }
    private String changeDateFormat(String currentFormat,String requiredFormat,String dateString){
        String result="";
        SimpleDateFormat formatterOld = new SimpleDateFormat(currentFormat, Locale.getDefault());
        SimpleDateFormat formatterNew = new SimpleDateFormat(requiredFormat, Locale.getDefault());
        Date date=null;
        try {
            date = formatterOld.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (date != null) {
            result = formatterNew.format(date);
        }
        return result;
    }
}
