package com.anafulus.project.ForgotPIN;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.anafulus.project.Profile.PinChangeDataPersonal;
import com.anafulus.project.R;

public class ChangePIN extends AppCompatActivity {

    private Button btn_change;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pin_fragment);

        //Button Change PIN
        btn_change = findViewById(R.id.btn_change);
        btn_change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ChangePIN.this, PinChangeDataPersonal.class);
                startActivity(i);
                Toast.makeText(ChangePIN.this, "PIN Berhasil Dirubah", Toast.LENGTH_SHORT).show();
                finish();
            }
        });

    }
}
