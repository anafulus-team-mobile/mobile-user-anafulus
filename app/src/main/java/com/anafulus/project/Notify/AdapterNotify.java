package com.anafulus.project.Notify;

public class AdapterNotify {
    private String date;
    private String clock;
    private String describe;

    public AdapterNotify(String date, String clock, String describe) {
        this.date = date;
        this.clock = clock;
        this.describe = describe;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getClock() {
        return clock;
    }

    public void setClock(String clock) {
        this.clock = clock;
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }
}
