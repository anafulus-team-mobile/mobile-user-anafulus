package com.anafulus.project.Notify;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
//import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.anafulus.project.Config.BorrowerApi;
import com.anafulus.project.Config.RetrofitClient;
import com.anafulus.project.Help.Help;
import com.anafulus.project.History.LoanAdapter;
import com.anafulus.project.Models.Borrowers;
import com.anafulus.project.Models.Loan;
import com.anafulus.project.Models.Notification;
import com.anafulus.project.PageDashboard;
import com.anafulus.project.History.PageHistory;
import com.anafulus.project.PageInfo;
import com.anafulus.project.Profile.PageAccount;
import com.anafulus.project.R;
import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;

import java.util.ArrayList;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;


public class PageNotify extends AppCompatActivity {
    BorrowerApi mApiInterface;
    SharedPreferences preferences;
    SwipeMenuListView listViewNotify;

    private static final String TAG = "PageNotify";

    private Toolbar toolbar;
    private boolean doubleTap = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.page_notify);
        Log.d(TAG, "onCreate: Started.");
        preferences = getSharedPreferences("borrower_profil", MODE_PRIVATE);
        mApiInterface = RetrofitClient.getClient().create(BorrowerApi.class);

        //Toolbar
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Notifikasi");

        //ListView
        String idBorrower = preferences.getString("idBorrower", null);
        Integer isRead = null;
        listViewNotify = findViewById(R.id.listview);

        getNotification(idBorrower, isRead);

        SwipeMenuCreator creator = new SwipeMenuCreator() {
            @Override
            public void create(SwipeMenu menu) {
                Log.d("ampassss", "testttt");
                // create "delete" item
                SwipeMenuItem deleteItem = new SwipeMenuItem(getApplicationContext());
                // set item background
                deleteItem.setBackground(new ColorDrawable(Color.blue(130)));
                // set item width
                deleteItem.setWidth(170);
                // set a icon
                deleteItem.setTitle("Hapus");
                deleteItem.setTitleSize(15);
                deleteItem.setTitleColor(Color.WHITE);
                // add to menu
                menu.addMenuItem(deleteItem);
            }
        };
        listViewNotify.setMenuCreator(creator);

        listViewNotify.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                switch (index){
                    case 0:
                        Toast.makeText(PageNotify.this, "dihapus", Toast.LENGTH_SHORT).show();
                        Log.d(TAG,"onMenuItemClick: clicked item" + index);
                        break;
                }
                return false;
            }
        });

        //Bottom Navigation
        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottomNavView_Bar);
        Menu menu = bottomNavigationView.getMenu();
        MenuItem menuItem = menu.getItem(1);
        menuItem.setChecked(true);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.nav_dashboard:
                        Intent a = new Intent(PageNotify.this, PageDashboard.class);
                        startActivity(a);
                        finish();
                        break;

                    case R.id.nav_notify:
                        break;

                    case R.id.nav_info:
                        Intent b = new Intent(PageNotify.this, PageInfo.class);
                        startActivity(b);
                        finish();
                        break;

                    case R.id.nav_history:
                        Intent c = new Intent(PageNotify.this, PageHistory.class);
                        startActivity(c);
                        finish();
                        break;

                    case R.id.nav_account:
                        Intent d = new Intent(PageNotify.this, PageAccount.class);
                        startActivity(d);
                        finish();
                        break;
                }

                return false;
            }
        });
    }

    private void getNotification(String idBorrower, Integer isRead) {
        Call<Borrowers> getDataNotification = mApiInterface.getNotification(idBorrower, isRead);
        getDataNotification.enqueue(new Callback<Borrowers>() {
            @Override
            public void onResponse(Response<Borrowers> response, Retrofit retrofit) {
                ImageView imgEmpty = findViewById(R.id.imageEmpty);
                TextView txtEmpty = findViewById(R.id.txtEmpty);
                if (response.body().getListNotification().isEmpty()) {
                    imgEmpty.setVisibility(View.VISIBLE);
                    txtEmpty.setVisibility(View.VISIBLE);
                } else {
                    imgEmpty.setVisibility(View.GONE);
                    txtEmpty.setVisibility(View.GONE);
                    ArrayList<Notification> modelNotificationArrayList = new ArrayList<>();
                    for (int a = 0; a < response.body().getListNotification().size(); a++) {

                        Notification notificationView = new Notification();
                        notificationView.setId(response.body().getListNotification().get(a).getId());
                        notificationView.setId_loan(response.body().getListNotification().get(a).getId_loan());
                        notificationView.setDescription(response.body().getListNotification().get(a).getDescription());
                        notificationView.setIs_read(response.body().getListNotification().get(a).getIs_read());
                        notificationView.setUpdated_at(response.body().getListNotification().get(a).getUpdated_at());
                        modelNotificationArrayList.add(notificationView);
                    }
                    makeToList(modelNotificationArrayList);
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Log.d("AKSES KE SERVER GAGAL", t.getMessage());
                Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void makeToList(ArrayList<Notification> dataNotification) {
        NotifyListAdapter notifyListAdapterAdapter;
        notifyListAdapterAdapter = new NotifyListAdapter(this, dataNotification);
        listViewNotify.setAdapter(notifyListAdapterAdapter);

//        SwipeMenuCreator creator = new SwipeMenuCreator() {
//            @Override
//            public void create(SwipeMenu menu) {
//                Log.d("ampassss", "testttt");
//                // create "delete" item
//                SwipeMenuItem deleteItem = new SwipeMenuItem(getApplicationContext());
//                // set item background
//                deleteItem.setBackground(new ColorDrawable(Color.blue(130)));
//                // set item width
//                deleteItem.setWidth(170);
//                // set a icon
//                deleteItem.setTitle("Hapus");
//                deleteItem.setTitleSize(15);
//                deleteItem.setTitleColor(Color.WHITE);
//                // add to menu
//                menu.addMenuItem(deleteItem);
//            }

//        listViewNotify.setMenuCreator(creator);
//
//        listViewNotify.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
//            @Override
//            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
//                switch (index) {
//                    case 0:
//                        Toast.makeText(PageNotify.this, "Notifikasi dihapus", Toast.LENGTH_SHORT).show();
//                        Log.d(TAG, "onMenuItemClick: clicked item " + index);
//                        break;
//                }
//                // false : close the menu; true : not close the menu
//                return false;
//            }
//        });

    }

    //BUTTON HELP
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.help_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_help:
                Intent i = new Intent(PageNotify.this, Help.class);
                startActivity(i);
        }
        return super.onOptionsItemSelected(item);
    }

    //Double Tap For Exit App
    @Override
    public void onBackPressed() {
        if (doubleTap) {
            super.onBackPressed();
        } else {
            Toast.makeText(this, "Ketuk Tombol Back 2 Kali Untuk Keluar Aplikasi", Toast.LENGTH_SHORT).show();
            doubleTap = true;
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    doubleTap = false;
                }
            }, 500);
        }
    }

}
