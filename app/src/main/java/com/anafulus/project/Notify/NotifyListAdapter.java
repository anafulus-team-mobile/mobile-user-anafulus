package com.anafulus.project.Notify;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.anafulus.project.Config.BorrowerApi;
import com.anafulus.project.Config.RetrofitClient;
import com.anafulus.project.DetailInstallment.NotYet;
import com.anafulus.project.History.LoanAdapter;
import com.anafulus.project.Models.Borrowers;
import com.anafulus.project.Models.Loan;
import com.anafulus.project.Models.Notification;
import com.anafulus.project.Profile.ChangePersonal;
import com.anafulus.project.Profile.ChangePonsel;
import com.anafulus.project.Profile.OtpChangePhoneNumber;
import com.anafulus.project.R;

import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

import static android.content.Context.MODE_PRIVATE;

public class NotifyListAdapter extends BaseAdapter {
    BorrowerApi mApiInterface;
    SharedPreferences preferences;

    private Context context;
    private ArrayList<Notification> dataNotification;
    private LayoutInflater mInflater;


    public NotifyListAdapter(Context context, ArrayList<Notification> dataNotification) {
        this.context = context;
        this.dataNotification = dataNotification;
        mInflater = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getViewTypeCount() {
        return dataNotification.size();
    }

    @Override

    public int getItemViewType(int position) {

        return position;
    }

    @Override
    public int getCount() {
        return dataNotification.size();
    }

    @Override
    public Object getItem(int position) {

        return dataNotification.get(position);

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        NotifyListAdapter.ViewHolder holder;
        if (convertView == null) {
            holder = new NotifyListAdapter.ViewHolder();
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.adapter_notify, null, true);

            holder.tv_date = convertView.findViewById(R.id.date);
            holder.tv_describe = convertView.findViewById(R.id.describe);
            holder.layoutDate = convertView.findViewById(R.id.layout_date);
            holder.txtIsRead = convertView.findViewById(R.id.isRead);

            convertView.setTag(holder);
        } else {
            // the getTag returns the viewHolder object set as a tag to the view
            holder = (NotifyListAdapter.ViewHolder) convertView.getTag();
        }
        Locale localeID = new Locale("in", "ID");
        NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);

        if (dataNotification.get(position).getIs_read() == null){
            holder.txtIsRead.setText("Belum dilihat");
            holder.txtIsRead.setTextColor(context.getResources().getColor(R.color.yellow));
        } else {
            holder.txtIsRead.setText("Sudah dilihat");
            holder.txtIsRead.setTextColor(context.getResources().getColor(R.color.green));
        }
        holder.tv_describe.setText(dataNotification.get(position).getDescription());
        holder.tv_date.setText(changeDateFormat("yyyy-MM-dd hh:mm:ss", "dd MMM yyyy hh:mm", dataNotification.get(position).getUpdated_at()));
        holder.layoutDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                preferences = context.getSharedPreferences("borrower_profil", MODE_PRIVATE);
                String idBorrower = preferences.getString("idBorrower", null);
                Integer idNotification = dataNotification.get(position).getId();
                if (dataNotification.get(position).getId_loan() != null) {
                    Integer idLoan = dataNotification.get(position).getId_loan();
                    updateNotification(idBorrower, idNotification, idLoan);
                } else {

                }
            }
        });

        return convertView;
    }

    private class ViewHolder {
        protected TextView tv_date, txtIsRead, tv_describe;
        protected RelativeLayout layoutDate;
    }

    private String changeDateFormat(String currentFormat, String requiredFormat, String dateString) {
        String result = "";
        SimpleDateFormat formatterOld = new SimpleDateFormat(currentFormat, Locale.getDefault());
        SimpleDateFormat formatterNew = new SimpleDateFormat(requiredFormat, Locale.getDefault());
        Date date = null;
        try {
            date = formatterOld.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (date != null) {
            result = formatterNew.format(date);
        }
        return result;
    }

    private void updateNotification(String idBorrower, Integer idNotification, Integer idLoan) {
        mApiInterface = RetrofitClient.getClient().create(BorrowerApi.class);
        final Integer loanId = idLoan;
        final String borrowerId = idBorrower;

        Call<Borrowers> changeHP = mApiInterface.updateNotification(idBorrower, idNotification);
        changeHP.enqueue(new Callback<Borrowers>() {
            @Override
            public void onResponse(Response<Borrowers> response, Retrofit retrofit) {
                if (response.isSuccess()) {
                    getLoanStatus(borrowerId, loanId);
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Log.d("AKSES KE SERVER GAGAL", t.getMessage());
            }
        });
    }

    private void getLoanStatus(String borrowerId, Integer loanId) {
        final Integer idLoan = loanId;
        Call<Borrowers> getLoanStatus = mApiInterface.getLoanStatus(borrowerId, loanId);
        getLoanStatus.enqueue(new Callback<Borrowers>() {
            @Override
            public void onResponse(Response<Borrowers> response, Retrofit retrofit) {
                if (response.isSuccess()) {
                    int a = 0;
                    Intent i = new Intent(context, NotYet.class);
                    i.putExtra("loanStatus", response.body().getListLoan().get(a).getLoan_status());
                    i.putExtra("idLoan", idLoan.toString());
                    context.startActivity(i);
                }
            }
            @Override
            public void onFailure(Throwable t) {
                Log.d("AKSES KE SERVER GAGAL",t.getMessage());
            }
        });
    }
}