package com.anafulus.project.Login;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.alimuzaffar.lib.pin.PinEntryEditText;
import com.anafulus.project.Config.AuthApi;
import com.anafulus.project.Config.RetrofitClient;
import com.anafulus.project.Models.Auths;
import com.anafulus.project.Models.Borrowers;
import com.anafulus.project.OtpLoginPhone;
import com.anafulus.project.PageDashboard;
import com.anafulus.project.R;
import com.anafulus.project.Register.DataRegister;


import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;


public class OtpChangeLogin extends AppCompatActivity {

    Toolbar toolbar;
    private PinEntryEditText phoneCode;
    private TextView text_failed, newPhoneNumber;
    Button button_verify;
    String string;
    AuthApi mApiInterface;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);

        //INITIAL
        text_failed = findViewById(R.id.text_failed);
        newPhoneNumber = findViewById(R.id.number);
        button_verify = findViewById(R.id.button_verify);
        phoneCode = findViewById(R.id.text_otp);

        mApiInterface = RetrofitClient.getClient().create(AuthApi.class);

        //GET text from login page
        String phoneNumber = getIntent().getExtras().getString("phoneNumber");
        String stringPhoneCode = getIntent().getExtras().getString("phoneCode");

        phoneCode.setText(stringPhoneCode);
        newPhoneNumber.setText(phoneNumber);

        //Toolbar & Button Back
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("OTP");
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(OtpChangeLogin.this, ChangePonselLogin.class);
                startActivity(i);
                finish();
            }
        });

        //CHECK otp
        if (phoneCode != null) {
            phoneCode.setOnPinEnteredListener(new PinEntryEditText.OnPinEnteredListener() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onPinEntered(CharSequence str) {
                    if (str.toString().equals("1234")) {
                        text_failed.setText("");
                    } else {
                        text_failed.setText("Kode yang Anda masukkan salah");
                    }
                }
            });

            //BUTTON Verify
            button_verify.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    verifyPhoneCode();
                }
            });
        }
    }
    private void verifyPhoneCode(){
        String stringPhoneCode = getIntent().getExtras().getString("phoneCode");
        String pinPhoneCode =phoneCode.getText().toString();
        if (stringPhoneCode.equals(pinPhoneCode)) {
            Toast.makeText(getApplicationContext(),"Pin Benar", Toast.LENGTH_LONG).show();
            changePhoneNumber();
        } else {
            Toast.makeText(getApplicationContext(),"Pin Salah", Toast.LENGTH_LONG).show();
        }
    }

    private void changePhoneNumber(){
        String new_phone_number = getIntent().getExtras().getString("phoneNumber");
        String last_phone_number = getIntent().getExtras().getString("lastPhone");
        String borrower_local_id = getIntent().getExtras().getString("localId");
        String family_card_id = getIntent().getExtras().getString("familyCard");

        Call<Auths> postKontakCall = mApiInterface.changePhoneNumber(new_phone_number, last_phone_number, borrower_local_id, family_card_id);
        postKontakCall.enqueue(new Callback<Auths>() {
            @Override
            public void onResponse(Response<Auths> response, Retrofit retrofit) {
                Auths authResponse = (Auths)response.body();
                if (!authResponse.isErr()){
                    userLogin();
                } else {
                    Toast.makeText(OtpChangeLogin.this, authResponse.getMsg(),Toast.LENGTH_LONG).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            final Intent mainIntent = new Intent(getApplicationContext(), DataRegister.class);
                            startActivity(mainIntent);
                            finish();
                        }
                    }, 5000);

                }
            }
            @Override
            public void onFailure(Throwable t) {
                Log.d("AKSES KE SERVER GAGAL",t.getMessage());
                Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
            }
        });
    }
    private void userLogin(){
        String phoneNumber = getIntent().getExtras().getString("phoneNumber");
        Call<Borrowers> postKontakCall = mApiInterface.userLogin(phoneNumber);
        postKontakCall.enqueue(new Callback<Borrowers>() {
            @Override
            public void onResponse(Response<Borrowers> response, Retrofit retrofit) {
                if (response.isSuccess()) {
                    Toast.makeText(OtpChangeLogin.this, "Selamat Datang", Toast.LENGTH_LONG).show();
                    Intent i = new Intent(OtpChangeLogin.this, PageDashboard.class);
                    startActivity(i);
                    finish();
                } else {
//                    Toast.makeText(OtpChangeLogin.this, authResponse.getMsg(),Toast.LENGTH_LONG).show();
                }
            }
            @Override
            public void onFailure(Throwable t) {
                Log.d("AKSES KE SERVER GAGAL",t.getMessage());
                Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
            }
        });
    }
    @Override
    public void onBackPressed() {
        Intent i = new Intent(OtpChangeLogin.this, ChangePonselLogin.class);
        startActivity(i);
        finish();
    }
}
