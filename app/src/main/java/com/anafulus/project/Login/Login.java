package com.anafulus.project.Login;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.anafulus.project.OtpLoginPhone;
import com.anafulus.project.R;
import com.anafulus.project.Register.Register;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

public class Login extends AppCompatActivity {
    SharedPreferences preferences;

    FirebaseAuth mAuth;
    private EditText number;
    private TextView text_regist, text_forgot;
    private Button btn_login, btn_hp;
    private Button btn_fb;
    private ProgressBar loading;
    private ProgressDialog loadingBar;
    private Toolbar toolbar;
    String codeSent;
//    private CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        preferences = getSharedPreferences("borrower_profil",MODE_PRIVATE);

        mAuth = FirebaseAuth.getInstance();

        //INITIAL
        // phone number
        number = findViewById(R.id.number);

        //Toolbar & Button Back
        toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Masuk");


        //LINK to change phone number
        text_forgot = findViewById(R.id.text_forgot);
        text_forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Login.this, ChangePonselLogin.class);
                startActivity(i);
                finish();
            }
        });

        //LINK to register account
        text_regist = findViewById(R.id.text_regist);
        text_regist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Login.this, Register.class);
                startActivity(i);
                finish();
            }
        });

        //BUTTON for login to dashboard
        btn_login = findViewById(R.id.btn_login);
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendVerificationCode();
            }
        });
    }

    private void sendVerificationCode(){
        String stringNumber = number.getText().toString();
            if (validNumber(stringNumber)) {
                PhoneAuthProvider.getInstance().verifyPhoneNumber(
                    "+62" + stringNumber.substring(1),        // Phone number to verify
                    60,                 // Timeout duration
                    TimeUnit.SECONDS,   // Unit of timeout
                    this,               // Activity (for callback binding)
                    mCallbacks);        // OnVerificationStateChangedCallbacks
                } else {
                    number.setError("Nomor Handphone Salah");
                }
    }

    PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
            Log.d("Berhasil, status", phoneAuthCredential.getSmsCode());
            String stringNumber = number.getText().toString();
            Intent i = new Intent(Login.this, OtpLoginPhone.class);
            i.putExtra("phoneCode", phoneAuthCredential.getSmsCode());
            i.putExtra("number", stringNumber);
            startActivity(i);
            finish();
        }

        @Override
        public void onVerificationFailed(FirebaseException e) {
            Log.d("Gagal, status", e.getMessage());
        }

        @Override
        public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            super.onCodeSent(s, forceResendingToken);
            codeSent = s;

        }
    };

        //Regex Phone Number
        private boolean validNumber(String phone) {
            boolean check = false;
            if (!Pattern.matches("[a-zA-Z]+", phone)){
                if (phone.length()<10||phone.length()>13){
                    check = false;
                    number.setError("Nomor Telepon Salah");
                }else {
                    check = true;
                }
            }else {
                check = false;
            }
            return check;
        }

    }

