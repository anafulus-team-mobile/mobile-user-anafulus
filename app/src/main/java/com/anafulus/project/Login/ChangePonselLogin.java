package com.anafulus.project.Login;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.anafulus.project.OtpLoginPhone;
import com.anafulus.project.R;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

public class ChangePonselLogin extends AppCompatActivity {

    private Toolbar toolbar;
    private Button btn_change;
    private EditText newPhoneNumber, lastPhoneNumber, familyCardId, borrowerLocalId;
    String codeSent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.change_ponsel);

        //INITIAL
        toolbar = findViewById(R.id.toolbar);
        btn_change = findViewById(R.id.btn_change);

        newPhoneNumber = findViewById(R.id.newPhoneNumber);
        lastPhoneNumber = findViewById(R.id.lastPhoneNumber);
        familyCardId = findViewById(R.id.familyCardId);
        borrowerLocalId = findViewById(R.id.borrowerLocalId);

        //TOOLBAR
        //Toolbar
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Ubah Nomor Ponsel");
//        toolbar.setNavigationIcon(R.drawable.ic_back);
//        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent i = new Intent(ChangePonsel.this, ChangePersonal.class);
//                startActivity(i);
//                finish();
//            }
//        });

        //BUTTON
        btn_change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendVerificationCode();
            }
        });
    }

    private void sendVerificationCode(){
        String phoneNumber = newPhoneNumber.getText().toString();
        if (validNumber(phoneNumber)) {
            PhoneAuthProvider.getInstance().verifyPhoneNumber(
                    "+62" + phoneNumber.substring(1),        // Phone number to verify
                    60,                 // Timeout duration
                    TimeUnit.SECONDS,   // Unit of timeout
                    this,               // Activity (for callback binding)
                    mCallbacks);        // OnVerificationStateChangedCallbacks
        } else {
            newPhoneNumber.setError("Nomor Handphone Salah");
        }
    }

    PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
            Log.d("Berhasil, status", phoneAuthCredential.getSmsCode());
            Intent i = new Intent(ChangePonselLogin.this, OtpChangeLogin.class);
            String phoneNumber = newPhoneNumber.getText().toString();
            String lastPhone = lastPhoneNumber.getText().toString();
            String localId = borrowerLocalId.getText().toString();
            String familyCard = familyCardId.getText().toString();
            i.putExtra("phoneCode", phoneAuthCredential.getSmsCode());
            i.putExtra("phoneNumber", phoneNumber);
            i.putExtra("lastPhone", lastPhone);
            i.putExtra("localId", localId);
            i.putExtra("familyCard", familyCard);
            startActivity(i);
            finish();
        }

        @Override
        public void onVerificationFailed(FirebaseException e) {
            Log.d("Gagal, status", e.getMessage());
        }

        @Override
        public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            super.onCodeSent(s, forceResendingToken);
            codeSent = s;

        }
    };

    private boolean validNumber(String phone) {
        boolean check = false;
        if (!Pattern.matches("[a-zA-Z]+", phone)){
            if (phone.length()<10||phone.length()>13){
                check = false;
                newPhoneNumber.setError("Nomor Telepon Salah");
            }else {
                check = true;
            }
        }else {
            check = false;
        }
        return check;
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(ChangePonselLogin.this, Login.class);
        startActivity(i);
        finish();
    }
}
