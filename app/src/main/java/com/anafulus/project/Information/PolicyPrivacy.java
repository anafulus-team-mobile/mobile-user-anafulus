package com.anafulus.project.Information;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.anafulus.project.PageInfo;
import com.anafulus.project.R;

public class PolicyPrivacy extends AppCompatActivity {

    private Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_company);

        //Toolbar
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Kebijakan Pivasi");
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(PolicyPrivacy.this, PageInfo.class);
                startActivity(i);
                finish();
            }
        });

        WebView web = (WebView) findViewById(R.id.web_view);
        web.loadUrl("http://www.anafulus.co.id/");
        web.setWebViewClient(new WebViewClient());

    }

    public void onBackPressed() {
        Intent i = new Intent(PolicyPrivacy.this, PageInfo.class);
        startActivity(i);
        finish();
    }
}
