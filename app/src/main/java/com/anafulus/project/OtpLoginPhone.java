package com.anafulus.project;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.alimuzaffar.lib.pin.PinEntryEditText;
import com.anafulus.project.Config.AuthApi;
import com.anafulus.project.Config.RetrofitClient;
import com.anafulus.project.Login.Login;
import com.anafulus.project.Models.Auths;
import com.anafulus.project.Models.Borrowers;
import com.google.firebase.auth.FirebaseAuth;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class OtpLoginPhone extends AppCompatActivity {
    FirebaseAuth mAuth;
    SharedPreferences preferences;
    Typeface tf;
    Toolbar toolbar;
    private PinEntryEditText pinEntry;
    private TextView text_failed, number;
    Button button_verify;
    String string;
    AuthApi mApiInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);

        mAuth = FirebaseAuth.getInstance();
        mApiInterface = RetrofitClient.getClient().create(AuthApi.class);

        preferences = getSharedPreferences("borrower_profil",MODE_PRIVATE);

        //INITIAL
        text_failed = findViewById(R.id.text_failed);
        number = findViewById(R.id.number);
        button_verify = findViewById(R.id.button_verify);
        pinEntry = findViewById(R.id.text_otp);

        //GET text from login page
        string = getIntent().getExtras().getString("number");
        String stringPhoneCode = getIntent().getExtras().getString("phoneCode");
        pinEntry.setText(stringPhoneCode);
        number.setText(string);

        //Toolbar & Button Back
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Masuk");
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(OtpLoginPhone.this, Login.class);
                i.putExtra("finish", true);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                finish();
            }
        });

        //CHECK otp
//        if (pinEntry != null) {
//            pinEntry.setOnPinEnteredListener(new PinEntryEditText.OnPinEnteredListener() {
//                @SuppressLint("SetTextI18n")
//                @Override
//                public void onPinEntered(CharSequence str) {
//                    if (str.toString().equals("1234")) {
//                        text_failed.setText("");
//                    } else {
//                        text_failed.setText("Kode yang Anda masukkan salah");
//                    }
//                }
//            });

        //BUTTON Verify
        button_verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verifyPhoneCode();
            }
        });
    }


    private void verifyPhoneCode(){
        String stringPhoneCode = getIntent().getExtras().getString("phoneCode");
        String pinPhoneCode =pinEntry.getText().toString();
        if (stringPhoneCode.equals(pinPhoneCode)) {
            userLogin();
        } else {
            Toast.makeText(getApplicationContext(),"Pin Salah", Toast.LENGTH_LONG).show();
        }
    }
    private void userLogin(){
        String phoneNumber= getIntent().getExtras().getString("number");
        Call<Borrowers> postKontakCall = mApiInterface.userLogin(phoneNumber);
        postKontakCall.enqueue(new Callback<Borrowers>() {
            @Override
            public void onResponse(Response<Borrowers> response, Retrofit retrofit) {
                if (!response.isSuccess()) {
                    SharedPreferences.Editor editor = preferences.edit();
                    int a = 0;
                    editor.putString("idBorrower",response.body().getListDataBorrower().get(a).getId_borrower().toString());
                    editor.putBoolean("isLogin",true);
                    editor.commit();
                    Toast.makeText(OtpLoginPhone.this, response.body().getMsg(), Toast.LENGTH_LONG).show();
                    Intent i = new Intent(OtpLoginPhone.this, PageDashboard.class);
                    startActivity(i);
                    finish();
                } else {
                    Toast.makeText(OtpLoginPhone.this, response.body().getMsg(),Toast.LENGTH_LONG).show();
                }
            }
            @Override
            public void onFailure(Throwable t) {
                Log.d("AKSES KE SERVER GAGAL",t.getMessage());
                Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
            }
        });
    }
};