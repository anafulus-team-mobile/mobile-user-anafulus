package com.anafulus.project.Payment;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.Toast;

import com.anafulus.project.Help.ExpandableListAdapter;
import com.anafulus.project.Help.Help;
import com.anafulus.project.History.PageHistory;
import com.anafulus.project.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PaymentMethod extends AppCompatActivity {

    private Toolbar toolbar;

    ExpandableListView expandableListView;
    ExpandableListAdapter expandableListAdapter;
    List<String> groupList;
    HashMap<String, List<String>> childListofGroup;
    private int previousGroup = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.payment_method);
        findViews();
        init();
        setListener();

        //Toolbar
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Tata Cara Pembayaran");
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(PaymentMethod.this, Payment.class);
                startActivity(i);
                finish();
            }
        });
    }

    private void findViews() {
        expandableListView = findViewById(R.id.expand);
    }

    private void init() {
        // load list data
        loadListData();

        //  set adapter
        expandableListAdapter = new ExpandableListAdapter(this, groupList,
                childListofGroup);
        expandableListView.setAdapter(expandableListAdapter);
    }

    private void setListener() {
        // Listview Group click listener
        expandableListView.setOnGroupClickListener(new ExpandableListView.
                OnGroupClickListener() {

            @Override
            public boolean onGroupClick(ExpandableListView parent, View v,
                                        int groupPosition, long id) {
                Toast.makeText(PaymentMethod.this, "Group Clicked" + groupList.get(groupPosition), Toast.LENGTH_SHORT).show();
//                Toast.makeText(activity,
//                        "Group Clicked " + groupList.get(groupPosition),
//                        Toast.LENGTH_SHORT).show();

                // only one group is populate using this
                if (expandableListView.isGroupExpanded(groupPosition)) {
                    expandableListView.collapseGroup(groupPosition);
                    previousGroup = -1;
                } else {
                    expandableListView.expandGroup(groupPosition);
                    if (previousGroup != -1) {
                        expandableListView.collapseGroup(previousGroup);
                    }
                    previousGroup = groupPosition;
                }
                return true;
            }
        });

        // Listview Group expanded listener
        expandableListView.setOnGroupExpandListener(new ExpandableListView.
                OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {
                Toast.makeText(PaymentMethod.this, groupList.get(groupPosition) + "Expanded", Toast.LENGTH_SHORT).show();
//                Toast.makeText(activity,
//                        groupList.get(groupPosition) +
//                                " Expanded", Toast.LENGTH_SHORT).show();
            }
        });

        // Listview Group collasped listener
        expandableListView.setOnGroupCollapseListener(new ExpandableListView.
                OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {
                Toast.makeText(PaymentMethod.this, groupList.get(groupPosition) + "Collapse", Toast.LENGTH_SHORT).show();
//                Toast.makeText(activity,
//                        groupList.get(groupPosition) +
//                                " Collapsed", Toast.LENGTH_SHORT).show();

            }
        });

        // Listview on child click listener
        expandableListView.setOnChildClickListener(new ExpandableListView.
                OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                Toast.makeText(PaymentMethod.this, groupList.get(groupPosition)
                        + ":"
                        + childListofGroup.get(groupList.get(groupPosition)).get(childPosition), Toast.LENGTH_SHORT).show();
//                Toast.makeText(
//                        activity,
//                        groupList.get(groupPosition)
//                                + " : "
//                                + childListofGroup.get(
//                                groupList.get(groupPosition)).get(
//                                childPosition), Toast.LENGTH_SHORT)
//                        .show();

                return false;
            }
        });
    }

        private void loadListData () {
            groupList = new ArrayList<String>();
            childListofGroup = new HashMap<String, List<String>>();

            // Adding group header data
            groupList.add("ATM BCA / Transfer");
            groupList.add("M-BCA");
            groupList.add("Klik BCA");

            // Adding child data
            List<String> transfer = new ArrayList<String>();
            transfer.add("reguler. Pilih Transfer Ke BCA Virtual Account");
            transfer.add("2. Masukkan nomor virtual account dan klik correct");
            transfer.add("3. Masukkan jumlah pembayaran lalu klik OK");
            transfer.add("4. Cek Kembali nama anda dan jumlah pembayaran");
            transfer.add("5. Ikuti langkahnya sampai pembayaran selesai");

            List<String> payment = new ArrayList<String>();
            payment.add("cara bayar cicilan");

            List<String> status = new ArrayList<String>();
            status.add("melihat status pinjaman");


            // Group header, Child data
            childListofGroup.put(groupList.get(0), transfer);
            childListofGroup.put(groupList.get(1), payment);
            childListofGroup.put(groupList.get(2), status);
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(PaymentMethod.this, Payment.class);
        startActivity(i);
        finish();
    }
}
