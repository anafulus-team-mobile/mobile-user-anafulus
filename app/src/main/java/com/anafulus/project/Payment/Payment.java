package com.anafulus.project.Payment;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.anafulus.project.History.PageHistory;
import com.anafulus.project.R;

public class Payment extends AppCompatActivity {

    private Toolbar toolbar;
    private Button bca, permata, cimb, pos, indomaret, alfa, kospin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.payment);

        String idBorrower = getIntent().getExtras().getString("idBorrower");
        String totalPenalty = getIntent().getExtras().getString("totalPenalty");
        String reason = getIntent().getExtras().getString("reason");

        Log.d("idBorrower " , idBorrower);
        Log.d("totalPenalty " , totalPenalty);
        Log.d("reason " , reason);

        //Toolbar
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Virtual Account");
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Payment.this, PageHistory.class);
                startActivity(i);
                finish();
            }
        });

        //Button BCA
        bca = findViewById(R.id.bca);
        bca.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Payment.this, PaymentMethod.class);
                startActivity(i);
                finish();
            }
        });

        //Button Permata
        permata = findViewById(R.id.permata);
        permata.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Payment.this, PaymentMethod.class);
                startActivity(i);
                finish();
            }
        });

        //Button CIMB
        cimb = findViewById(R.id.cimb);
        cimb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Payment.this, PaymentMethod.class);
                startActivity(i);
                finish();
            }
        });

        //Button POS
        pos = findViewById(R.id.pos);
        pos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Payment.this, PaymentMethod.class);
                startActivity(i);
                finish();
            }
        });

        //Button Indomart
        indomaret = findViewById(R.id.indomaret);
        indomaret.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Payment.this, PaymentMethod.class);
                startActivity(i);
                finish();
            }
        });

        //Button Alfa
        alfa = findViewById(R.id.alfa);
        alfa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Payment.this, PaymentMethod.class);
                startActivity(i);
                finish();
            }
        });

        //Button Kospin
        kospin = findViewById(R.id.kospin);
        kospin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Payment.this, PaymentMethod.class);
                startActivity(i);
                finish();
            }
        });

    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(Payment.this, PageHistory.class);
        startActivity(i);
        finish();
    }
}
