package com.anafulus.project.Business;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.anafulus.project.PageDashboard;
import com.anafulus.project.R;

public class BusinessEntity extends AppCompatActivity {

    private TextView text2;
    private Toolbar toolbar;
    private Button btn_change;
    String string;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.business_entity);

        //Toolbar & Button Back
        toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Pinjaman Badan Usaha");
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(BusinessEntity.this, PageDashboard.class);
                startActivity(i);
                finish();
            }
        });

        //Button Change
        btn_change = findViewById(R.id.btn_change);
        btn_change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(BusinessEntity.this, "Berhasil, Mohon Tunggu Telepon dari Pihak Kami", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(BusinessEntity.this, PageDashboard.class);
                startActivity(i);
                finish();
            }
        });

    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(BusinessEntity.this, PageDashboard.class);
        startActivity(i);
        finish();
    }
}
