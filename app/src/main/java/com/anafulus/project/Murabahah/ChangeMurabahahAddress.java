package com.anafulus.project.Murabahah;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.anafulus.project.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ChangeMurabahahAddress extends AppCompatActivity {

    private Toolbar toolbar;
    private Spinner spn_province, spn_city, spn_district, spn_kelurahan;
    private Button btn_save;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.change_address);

        //Toolbar & Button Back
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Ubah / Lengkapi Alamat");
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ChangeMurabahahAddress.this, MurabahahAddress.class);
                startActivity(i);
                finish();
            }
        });

        //Spinner Province
        spn_province = findViewById(R.id.province);

        // Initializing a String Array
        String[] province = new String[]{
                "Provinsi...",
                "DKI Jakarta",
                "Jawa Barat",
                "Jawa Tengah",
                "Jawa Timur",
                "Nusa Tenggara Timur"
        };

        final List<String> provinceList = new ArrayList<>(Arrays.asList(province));

        // Initializing an ArrayAdapter
        final ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                this, R.layout.spinner_item, provinceList) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                } else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if (position == 0) {
                    // Set the hint text color gray
                    tv.setTextColor(Color.GRAY);
                } else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };

        //Spinner Education
        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
        spn_province.setAdapter(spinnerArrayAdapter);
        spn_province.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItemText = (String) parent.getItemAtPosition(position);
                // If user change the default selection
                // First item is disable and it is used for hint
                if (position == 1) {
                    Toast.makeText(ChangeMurabahahAddress.this, "Jakarta", Toast.LENGTH_SHORT).show();
                } else if (position == 2) {
                    Toast.makeText(ChangeMurabahahAddress.this, "Jawa Barat", Toast.LENGTH_SHORT).show();
                } else if (position == 3) {
                    Toast.makeText(ChangeMurabahahAddress.this, "Jawa Tengah", Toast.LENGTH_SHORT).show();
                } else if (position == 4){
                    Toast.makeText(ChangeMurabahahAddress.this, "Jawa Timur", Toast.LENGTH_SHORT).show();
                } else if (position == 5){
                    Toast.makeText(ChangeMurabahahAddress.this, "Nusa Tenggara Timur", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //Spinner Province
        spn_city = findViewById(R.id.city);

        // Initializing a String Array
        String[] city = new String[]{
                "Kota...",
                "DKI Jakarta",
                "Jawa Barat",
                "Jawa Tengah",
                "Jawa Timur",
                "Nusa Tenggara Timur"
        };

        final List<String> cityList = new ArrayList<>(Arrays.asList(city));

        // Initializing an ArrayAdapter
        final ArrayAdapter<String> spinnerArrayAdapter2 = new ArrayAdapter<String>(
                this, R.layout.spinner_item, cityList) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                } else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if (position == 0) {
                    // Set the hint text color gray
                    tv.setTextColor(Color.GRAY);
                } else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };

        //Spinner City
        spinnerArrayAdapter2.setDropDownViewResource(R.layout.spinner_item);
        spn_city.setAdapter(spinnerArrayAdapter2);
        spn_city.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItemText = (String) parent.getItemAtPosition(position);
                // If user change the default selection
                // First item is disable and it is used for hint
                if (position == 1) {
                    Toast.makeText(ChangeMurabahahAddress.this, "Jakarta Utara", Toast.LENGTH_SHORT).show();
                } else if (position == 2) {
                    Toast.makeText(ChangeMurabahahAddress.this, "Bogor", Toast.LENGTH_SHORT).show();
                } else if (position == 3) {
                    Toast.makeText(ChangeMurabahahAddress.this, "Yogyakarta", Toast.LENGTH_SHORT).show();
                } else if (position == 4){
                    Toast.makeText(ChangeMurabahahAddress.this, "Surabaya", Toast.LENGTH_SHORT).show();
                } else if (position == 5){
                    Toast.makeText(ChangeMurabahahAddress.this, "Lombok", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //Spinner District
        spn_district = findViewById(R.id.district);

        // Initializing a String Array
        String[] district = new String[]{
                "Kecamatan...",
                "Tanjung Priok",
                "Bogor Barat",
                "Ungaran",
                "Sambikerep",
                "Aesesa Selatan"
        };

        final List<String> districtList = new ArrayList<>(Arrays.asList(district));

        // Initializing an ArrayAdapter
        final ArrayAdapter<String> spinnerArrayAdapter3 = new ArrayAdapter<String>(
                this, R.layout.spinner_item, districtList) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                } else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if (position == 0) {
                    // Set the hint text color gray
                    tv.setTextColor(Color.GRAY);
                } else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };

        //Spinner City
        spinnerArrayAdapter3.setDropDownViewResource(R.layout.spinner_item);
        spn_district.setAdapter(spinnerArrayAdapter3);
        spn_district.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItemText = (String) parent.getItemAtPosition(position);
                // If user change the default selection
                // First item is disable and it is used for hint
                if (position == 1) {
                    Toast.makeText(ChangeMurabahahAddress.this, "", Toast.LENGTH_SHORT).show();
                } else if (position == 2) {
                    Toast.makeText(ChangeMurabahahAddress.this, "", Toast.LENGTH_SHORT).show();
                } else if (position == 3) {
                    Toast.makeText(ChangeMurabahahAddress.this, "", Toast.LENGTH_SHORT).show();
                } else if (position == 4){
                    Toast.makeText(ChangeMurabahahAddress.this, "", Toast.LENGTH_SHORT).show();
                } else if (position == 5){
                    Toast.makeText(ChangeMurabahahAddress.this, "", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //Spinner Kelurahan
        spn_kelurahan = findViewById(R.id.kelurahan);

        // Initializing a String Array
        String[] kelurahan = new String[]{
                "Kelurahan...",
                "Sunter Agung",
                "Pademangan",
                "Mangga Besar",
                "Palmerah",
                "Slipi"
        };

        final List<String> kelurahanList = new ArrayList<>(Arrays.asList(kelurahan));

        // Initializing an ArrayAdapter
        final ArrayAdapter<String> spinnerArrayAdapter4 = new ArrayAdapter<String>(
                this, R.layout.spinner_item, kelurahanList) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                } else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if (position == 0) {
                    // Set the hint text color gray
                    tv.setTextColor(Color.GRAY);
                } else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };

        //Spinner City
        spinnerArrayAdapter4.setDropDownViewResource(R.layout.spinner_item);
        spn_kelurahan.setAdapter(spinnerArrayAdapter4);
        spn_kelurahan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItemText = (String) parent.getItemAtPosition(position);
                // If user change the default selection
                // First item is disable and it is used for hint
                if (position == 1) {
                    Toast.makeText(ChangeMurabahahAddress.this, "", Toast.LENGTH_SHORT).show();
                } else if (position == 2) {
                    Toast.makeText(ChangeMurabahahAddress.this, "", Toast.LENGTH_SHORT).show();
                } else if (position == 3) {
                    Toast.makeText(ChangeMurabahahAddress.this, "", Toast.LENGTH_SHORT).show();
                } else if (position == 4){
                    Toast.makeText(ChangeMurabahahAddress.this, "", Toast.LENGTH_SHORT).show();
                } else if (position == 5){
                    Toast.makeText(ChangeMurabahahAddress.this, "", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //Button Change
        btn_save = findViewById(R.id.btn_change);
        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ChangeMurabahahAddress.this, MurabahahRequirement.class);
                startActivity(i);
                finish();
            }
        });

    }
}
