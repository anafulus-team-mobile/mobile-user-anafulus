package com.anafulus.project.Murabahah;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.alimuzaffar.lib.pin.PinEntryEditText;
import com.anafulus.project.Profile.ChangePonsel;
import com.anafulus.project.R;


public class OtpChangeProfile2 extends AppCompatActivity {

    Toolbar toolbar;
    private PinEntryEditText pinEntry;
    private TextView text_failed, number;
    Button button_verify;
    String string;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);

        //INITIAL
        text_failed = findViewById(R.id.text_failed);
        number = findViewById(R.id.number);
        button_verify = findViewById(R.id.button_verify);
        pinEntry = findViewById(R.id.text_otp);

        //GET text from login page
        string = getIntent().getExtras().getString("number");
        number.setText(string);

        //Toolbar & Button Back
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("OTP");
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(OtpChangeProfile2.this, ChangePonsel.class);
                startActivity(i);
                finish();
            }
        });

        //CHECK otp
        if (pinEntry != null) {
            pinEntry.setOnPinEnteredListener(new PinEntryEditText.OnPinEnteredListener() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onPinEntered(CharSequence str) {
                    if (str.toString().equals("1234")) {
                        text_failed.setText("");
                    } else {
                        text_failed.setText("Kode yang Anda masukkan salah");
                    }
                }
            });

            //BUTTON Verify
            button_verify.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(OtpChangeProfile2.this, "Nomor ponsel berhasil di ubah", Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(OtpChangeProfile2.this, MurabahahPersonalData.class);
                    startActivity(i);
                    finish();
                }
            });
        }
    }
}
