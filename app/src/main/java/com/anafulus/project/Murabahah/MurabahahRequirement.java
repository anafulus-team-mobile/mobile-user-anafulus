package com.anafulus.project.Murabahah;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.anafulus.project.Config.BorrowerApi;
import com.anafulus.project.Config.RetrofitClient;
import com.anafulus.project.Models.Borrowers;
import com.anafulus.project.R;
import com.kofigyan.stateprogressbar.StateProgressBar;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class MurabahahRequirement extends AppCompatActivity {
    SharedPreferences preferences;
    BorrowerApi mApiInterface;

    private TextView selfieImage, localIdImage, familyCardImage, salarySlipImage, homeImage, familyName, familyPhoneNumber, familyRelationship;
    private Toolbar toolbar;
    private Button btnCompleteProfil, btn_change, btnContinue;
    String[] descriptionData = {"Data Diri", "Alamat", "Persyaratan","Kode Agen"};

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reguler_requirement);
        preferences = getSharedPreferences("borrower_profil", Context.MODE_PRIVATE);
        mApiInterface = RetrofitClient.getClient().create(BorrowerApi.class);
        getRequirementData();

        selfieImage = findViewById(R.id.selfieLocalIdImage);
        localIdImage = findViewById(R.id.localIdImage);
        familyCardImage = findViewById(R.id.familyCardImage);
        salarySlipImage = findViewById(R.id.salaryImage);
        homeImage = findViewById(R.id.homeImage);
        familyName = findViewById(R.id.familyName);
        familyPhoneNumber = findViewById(R.id.familyPhoneNumber);
        familyRelationship = findViewById(R.id.familyRelationship);

        //Toolbar & Button Back
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Data Persyaratan");
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MurabahahRequirement.this, MurabahahAddress.class);
                i.putExtra("loan_category", "Perorangan");
                i.putExtra("loan_type", "Murabahah");
                i.putExtra("id_borrower", getIntent().getExtras().getString("id_borrower"));
                i.putExtra("loan_principal", getIntent().getExtras().getString("loan_principal"));
                i.putExtra("tenor", getIntent().getExtras().getString("tenor"));
                i.putExtra("loan_purpose", getIntent().getExtras().getString("loan_purpose"));
                i.putExtra("loan_status", "Belum disetujui");
                i.putExtra("installment_nominal", getIntent().getExtras().getString("installment_nominal"));
                i.putExtra("installment_type",getIntent().getExtras().getString("installment_type"));
                startActivity(i);
                finish();
            }
        });

        //Stepview Status
        StateProgressBar stateProgressBar = (StateProgressBar) findViewById(R.id.stepview);
        stateProgressBar.setStateDescriptionData(descriptionData);

        //Button Complete
        btnCompleteProfil = findViewById(R.id.btn_complete);
        btnCompleteProfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MurabahahRequirement.this, ChangeMurabahahRequirement.class);
                startActivity(i);
                finish();
            }
        });

        //Button Change
        btn_change = findViewById(R.id.btn_change);
        btn_change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MurabahahRequirement.this, ChangeMurabahahRequirement.class);
                startActivity(i);
                finish();
            }
        });

        //Button Continue
        btnContinue = findViewById(R.id.btn_continue);
        btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MurabahahRequirement.this, AgentCodeMurabahah.class);
                i.putExtra("loan_category", "Perorangan");
                i.putExtra("loan_type", "Murabahah");
                i.putExtra("id_borrower", getIntent().getExtras().getString("id_borrower"));
                i.putExtra("loan_principal", getIntent().getExtras().getString("loan_principal"));
                i.putExtra("tenor", getIntent().getExtras().getString("tenor"));
                i.putExtra("loan_purpose", getIntent().getExtras().getString("loan_purpose"));
                i.putExtra("loan_status", "Belum disetujui");
                i.putExtra("installment_nominal", getIntent().getExtras().getString("installment_nominal"));
                i.putExtra("installment_type",getIntent().getExtras().getString("installment_type"));
                startActivity(i);
                finish();
            }
        });
    }

    public void onBackPressed() {
        Intent i = new Intent(MurabahahRequirement.this, MurabahahAddress.class);
        i.putExtra("loan_category", "Perorangan");
        i.putExtra("loan_type", "Murabahah");
        i.putExtra("id_borrower", getIntent().getExtras().getString("id_borrower"));
        i.putExtra("loan_principal", getIntent().getExtras().getString("loan_principal"));
        i.putExtra("tenor", getIntent().getExtras().getString("tenor"));
        i.putExtra("loan_purpose", getIntent().getExtras().getString("loan_purpose"));
        i.putExtra("loan_status", "Belum disetujui");
        i.putExtra("installment_nominal", getIntent().getExtras().getString("installment_nominal"));
        i.putExtra("installment_type",getIntent().getExtras().getString("installment_type"));
        startActivity(i);
        finish();
    }
    private void getRequirementData() {
        String idBorrower= preferences.getString("idBorrower",null);
        Call<Borrowers> postKontakCall = mApiInterface.dataBorrower(idBorrower);
        postKontakCall.enqueue(new Callback<Borrowers>() {
            @Override
            public void onResponse(Response<Borrowers> response, Retrofit retrofit) {
                int i =0 ;
                String strSelfieImage = response.body().getListDataBorrower().get(i).getId_selfie_image();
                String strLocalIdImage = response.body().getListDataBorrower().get(i).getLocal_id_image();
                String strFamilyCardIdImage = response.body().getListDataBorrower().get(i).getFamily_card_image();
                String strSalaryImage = response.body().getListDataBorrower().get(i).getSalary_slip_image();
                String strHomeImage = response.body().getListDataBorrower().get(i).getHome_image();
                String strFamilyName = response.body().getListDataBorrower().get(i).getFamily_name();
                String strFamilyPhoneNumber = response.body().getListDataBorrower().get(i).getFamily_phone_number();
                String strFamilyRelationship = response.body().getListDataBorrower().get(i).getFamily_relationship();

                if(strSelfieImage == null) {
                    selfieImage.setText("Belum Lengkap");
                } else{
                    selfieImage.setText("Sudah Lengkap");
                    selfieImage.setTextColor(getResources().getColor(R.color.blue));
                }

                if(strLocalIdImage == null) {
                    localIdImage.setText("Belum Lengkap");
                } else{
                    localIdImage.setText("Sudah Lengkap");
                    localIdImage.setTextColor(getResources().getColor(R.color.blue));
                }

                if(strFamilyCardIdImage== null) {
                    familyCardImage.setText("Belum Lengkap");
                } else{
                    familyCardImage.setText("Sudah Lengkap");
                    familyCardImage.setTextColor(getResources().getColor(R.color.blue));
                }

                if(strSalaryImage== null) {
                    salarySlipImage.setText("Belum Lengkap");
                } else{
                    salarySlipImage.setText("Sudah Lengkap");
                    salarySlipImage.setTextColor(getResources().getColor(R.color.blue));
                }

                if(strHomeImage== null) {
                    homeImage.setText("Belum Lengkap");
                } else{
                    homeImage.setText("Sudah Lengkap");
                    homeImage.setTextColor(getResources().getColor(R.color.blue));

                }

                familyName.setText(strFamilyName);
                familyPhoneNumber.setText(strFamilyPhoneNumber);
                familyRelationship.setText(strFamilyRelationship);

                if ((strSelfieImage == null) || (strLocalIdImage== null) || (strFamilyCardIdImage== null) || (strSalaryImage== null) || (strHomeImage== null) || (strFamilyName== null) || (strFamilyPhoneNumber== null) || (strFamilyRelationship== null)) {
                    btnCompleteProfil.setVisibility(View.VISIBLE);
                    btnContinue.setEnabled(false);
                    btnContinue.setBackgroundColor(getResources().getColor(R.color.grey));
                } else {
                    btnCompleteProfil.setVisibility(View.INVISIBLE);
                    btnContinue.setEnabled(true);
                    btnContinue.setBackgroundColor(getResources().getColor(R.color.green));
                }
            }
            @Override
            public void onFailure(Throwable t) {
                Log.d("AKSES KE SERVER GAGAL",t.getMessage());
            }
        });
    }
}
