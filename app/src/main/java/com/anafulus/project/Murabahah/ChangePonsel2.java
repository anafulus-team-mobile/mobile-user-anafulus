package com.anafulus.project.Murabahah;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.anafulus.project.R;

public class ChangePonsel2 extends AppCompatActivity {

    private Toolbar toolbar;
    private Button btn_change;
    private EditText number_new;
    String string;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.change_ponsel);

        //INITIAL
        toolbar = findViewById(R.id.toolbar);
        btn_change = findViewById(R.id.btn_change);
        number_new = findViewById(R.id.newPhoneNumber);

        //TOOLBAR
        //Toolbar
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Ubah Nomor Ponsel");
//        toolbar.setNavigationIcon(R.drawable.ic_back);
//        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent i = new Intent(ChangePonsel.this, ChangePersonal.class);
//                startActivity(i);
//                finish();
//            }
//        });

        //BUTTON
        btn_change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ChangePonsel2.this, OtpChangeProfile2.class);
                string = number_new.getText().toString();
                i.putExtra("number", string);
                startActivity(i);
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(ChangePonsel2.this, ChangeMurabahahPersonal.class);
        startActivity(i);
        finish();
    }
}
