package com.anafulus.project.Murabahah;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.anafulus.project.R;

public class ChangeMurabahahRequirement extends AppCompatActivity {

    private Toolbar toolbar;
    private ImageButton layout_selfie, layout_ktp, layout_family, layout_house, layout_sallary;
    private Button btn_change;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.change_data_requirement);

        //Toolbar & Button Back
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Ubah / Lengkapi Data Persyaratan");
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ChangeMurabahahRequirement.this, MurabahahRequirement.class);
                startActivity(i);
                finish();
            }
        });

        //Button Selfie
        layout_selfie = findViewById(R.id.btn_selfie);
        layout_selfie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent("android.media.action.IMAGE_CAPTURE");
                startActivity(i);
            }
        });

        //Button Ktp
        layout_ktp = findViewById(R.id.btn_ktp);
        layout_ktp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent("android.media.action.IMAGE_CAPTURE");
                startActivity(i);
            }
        });

        //Button House
        layout_house = findViewById(R.id.btn_house);
        layout_house.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent("android.media.action.IMAGE_CAPTURE");
                startActivity(i);
            }
        });

        //Button Sallary
        layout_sallary = findViewById(R.id.btn_sallary);
        layout_sallary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent("android.media.action.IMAGE_CAPTURE");
                startActivity(i);
            }
        });

        //Button Change / Save
        btn_change = findViewById(R.id.btn_change);
        btn_change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ChangeMurabahahRequirement.this, AgentCodeMurabahah.class);
                startActivity(i);
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(ChangeMurabahahRequirement.this, MurabahahRequirement.class);
        startActivity(i);
        finish();
    }
}
