package com.anafulus.project.Murabahah;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import com.anafulus.project.R;

import java.util.Calendar;


public class ForgotPinMurabahah extends AppCompatActivity {

    private static final String TAG = "ForgotPinMurabahah";
    private EditText number;
    private TextView displaydate;
    private Button btn_reset;
    private Toolbar toolbar;
    private DatePickerDialog.OnDateSetListener dateSetListener;
    String string;

    Typeface tf;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgot_pin);

        //Toolbar & Button Back
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Lupa PIN");
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ForgotPinMurabahah.this, PinConfirmMurabahah.class);
                startActivity(i);
                finish();
            }
        });

        //Birthdate
        displaydate = findViewById(R.id.date);
        displaydate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar calendar = Calendar.getInstance();
                int year = calendar.get(Calendar.YEAR);
                int month = calendar.get(Calendar.MONTH);
                int day = calendar.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(ForgotPinMurabahah.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                                month = month +1;
                                displaydate.setText(day + "/" + month + "/" + year);
                            }
                        }, year, month, day);
                dialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                dialog.show();
            }
        });

        //Button Reset PIN
        btn_reset = findViewById(R.id.btn_reset);
        btn_reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ForgotPinMurabahah.this, OtpForgotPinMurabahah.class);
                string = number.getText().toString();
                i.putExtra("number", string);
                startActivity(i);
                finish();
            }
        });

        //EditText Number Phone
        number = findViewById(R.id.number);
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(ForgotPinMurabahah.this, PinConfirmMurabahah.class);
        startActivity(i);
        finish();
    }
}
