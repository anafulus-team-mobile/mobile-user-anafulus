package com.anafulus.project.Murabahah;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.anafulus.project.Config.BorrowerApi;
import com.anafulus.project.Config.RetrofitClient;
import com.anafulus.project.Models.Borrowers;
import com.anafulus.project.R;
import com.kofigyan.stateprogressbar.StateProgressBar;

import java.text.NumberFormat;
import java.util.Locale;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class MurabahahPersonalData extends AppCompatActivity {
    SharedPreferences preferences;
    BorrowerApi mApiInterface;
    private TextView nameBorrower, emailBorrower, noHpBorrower, gender, birthDate, lastEducation, localId, familyId, npwpId, income, lastJob;

    private Button btnCompleteProfil, btn_change, btn_continue;
    private Toolbar toolbar;
    private TextView text_condition;
    String string;
    String[] descriptionData = {"Data Diri", "Alamat", "Persyaratan","Kode Agen"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reguler_personal_data);
        preferences = getSharedPreferences("borrower_profil", Context.MODE_PRIVATE);
        mApiInterface = RetrofitClient.getClient().create(BorrowerApi.class);
        getProfilData();
        nameBorrower = findViewById(R.id.name);
        emailBorrower = findViewById(R.id.email);
        noHpBorrower = findViewById(R.id.phoneNumber);
        gender = findViewById(R.id.gender);
        birthDate = findViewById(R.id.birthDate);
        lastEducation = findViewById(R.id.lastEducation);
        localId = findViewById(R.id.localId);
        familyId = findViewById(R.id.familyCardId);
        npwpId = findViewById(R.id.npwp);
        income = findViewById(R.id.income);
        lastJob = findViewById(R.id.lastJob);

        //Toolbar & Button Back
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Data Diri");
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MurabahahPersonalData.this, Murabahah.class);
                startActivity(i);
                finish();
            }
        });

        //Stepview Status
        StateProgressBar stateProgressBar = (StateProgressBar) findViewById(R.id.stepview);
        stateProgressBar.setStateDescriptionData(descriptionData);

        //Text Condition
        text_condition = findViewById(R.id.text_condition);

        //Button Complete
        btnCompleteProfil = findViewById(R.id.btnCompleteProfil);
        btnCompleteProfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MurabahahPersonalData.this, ChangeMurabahahPersonal.class);
                startActivity(i);
                finish();
            }
        });

        //Button Change
        btn_change = findViewById(R.id.btn_change);
        btn_change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MurabahahPersonalData.this, ChangeMurabahahPersonal.class);
                i.putExtra("name", nameBorrower.getText().toString());
                i.putExtra("email", emailBorrower.getText().toString());
                i.putExtra("phone_number", noHpBorrower.getText().toString());
                i.putExtra("gender", gender.getText().toString());
                i.putExtra("birthdate", birthDate.getText().toString());
                i.putExtra("last_education", lastEducation.getText().toString());
                i.putExtra("local_id", localId.getText().toString());
                i.putExtra("family_id", familyId.getText().toString());
                i.putExtra("npwp_id", npwpId.getText().toString());
                if(income.getText().toString().equals("")){
                    i.putExtra("income", "0");
                } else{
                    String xRupiah = income.getText().toString().substring(1);
                    String doubleRupiah = xRupiah.substring(1);
                    i.putExtra("income", doubleRupiah.replaceAll("[-+.^:,]", ""));
                }
                i.putExtra("lastjob", lastJob.getText().toString());
                i.putExtra("loan_category", "Perorangan");
                i.putExtra("loan_type", "Murabahah");
                i.putExtra("id_borrower", getIntent().getExtras().getString("id_borrower"));
                i.putExtra("loan_principal", getIntent().getExtras().getString("loan_principal"));
                i.putExtra("tenor", getIntent().getExtras().getString("tenor"));
                i.putExtra("loan_purpose", getIntent().getExtras().getString("loan_purpose"));
                i.putExtra("loan_status", "Belum disetujui");
                i.putExtra("installment_nominal", getIntent().getExtras().getString("installment_nominal"));
                i.putExtra("installment_type",getIntent().getExtras().getString("installment_type"));

                startActivity(i);
                finish();
            }
        });

        //Button Continue
        btn_continue = findViewById(R.id.btn_continue);
        btn_continue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MurabahahPersonalData.this, MurabahahAddress.class);
                i.putExtra("loan_category", "Perorangan");
                i.putExtra("loan_type", "Murabahah");
                i.putExtra("id_borrower", getIntent().getExtras().getString("id_borrower"));
                i.putExtra("loan_principal", getIntent().getExtras().getString("loan_principal"));
                i.putExtra("tenor", getIntent().getExtras().getString("tenor"));
                i.putExtra("loan_purpose", getIntent().getExtras().getString("loan_purpose"));
                i.putExtra("loan_status", "Belum disetujui");
                i.putExtra("installment_nominal", getIntent().getExtras().getString("installment_nominal"));
                i.putExtra("installment_type",getIntent().getExtras().getString("installment_type"));
                startActivity(i);
                finish();
            }
        });
    }

    public void onBackPressed(){
        Intent i = new Intent(MurabahahPersonalData.this, Murabahah.class);
        i.putExtra("loan_category", "Perorangan");
        i.putExtra("loan_type", "Murabahah");
        i.putExtra("id_borrower", getIntent().getExtras().getString("id_borrower"));
        i.putExtra("loan_principal", getIntent().getExtras().getString("loan_principal"));
        i.putExtra("tenor", getIntent().getExtras().getString("tenor"));
        i.putExtra("loan_purpose", getIntent().getExtras().getString("loan_purpose"));
        i.putExtra("loan_status", "Belum disetujui");
        i.putExtra("installment_nominal", getIntent().getExtras().getString("installment_nominal"));
        i.putExtra("installment_type",getIntent().getExtras().getString("installment_type"));
        startActivity(i);
        finish();
    }
    private void getProfilData() {
        String idBorrower= preferences.getString("idBorrower",null);
        Call<Borrowers> postKontakCall = mApiInterface.dataBorrower(idBorrower);
        postKontakCall.enqueue(new Callback<Borrowers>() {
            @Override
            public void onResponse(Response<Borrowers> response, Retrofit retrofit) {
                int i =0 ;
                Locale localeID = new Locale("in", "ID");
                NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);

                String strName = response.body().getListDataBorrower().get(i).getName();
                String strEmail = response.body().getListDataBorrower().get(i).getEmail();
                String strHP = response.body().getListDataBorrower().get(i).getPhone_number();
                String strGender = response.body().getListDataBorrower().get(i).getGender();
                String strBirthDate = response.body().getListDataBorrower().get(i).getBirth_date();
                String strLastEducation = response.body().getListDataBorrower().get(i).getLast_education();
                String strLocalId = response.body().getListDataBorrower().get(i).getBorrower_local_id().toString();
                String strFamilyId = response.body().getListDataBorrower().get(i).getFamily_card_id().toString();
                String strNpwpId = response.body().getListDataBorrower().get(i).getPrincipal_taxpayer_id().toString();
                double doubleIncome = Double.parseDouble(response.body().getListDataBorrower().get(i).getIncome().toString());
                String strIncome = formatRupiah.format((double)doubleIncome);
                String strLasJob = response.body().getListDataBorrower().get(i).getLast_job();

                nameBorrower.setText(strName);
                emailBorrower.setText(strEmail);
                noHpBorrower.setText(strHP);
                gender.setText(strGender);
                birthDate.setText(strBirthDate);
                lastEducation.setText(strLastEducation);
                if(strLocalId.equals("0")){
                    localId.setText("");
                } else{
                    localId.setText(strLocalId);
                }
                if(strFamilyId.equals("0")){
                    familyId.setText("");
                } else{
                    familyId.setText(strFamilyId);
                }
                if(strNpwpId.equals("0")){
                    npwpId.setText("");
                } else{
                    npwpId.setText(strNpwpId);
                }
                if(strIncome.equals("0")){
                    income.setText("");
                } else{
                    income.setText(strIncome);
                }


                lastJob.setText(strLasJob);

                if ((strName.isEmpty()) || (strEmail.isEmpty()) || (strHP.isEmpty()) || (strGender == null) || (strBirthDate == null) || (strLastEducation==null) || (strFamilyId.isEmpty()) || (strNpwpId.isEmpty()) || (strIncome.isEmpty()) || (strLasJob==null)) {
                    btnCompleteProfil.setVisibility(View.VISIBLE);
                    btn_continue.setEnabled(false);
                    btn_continue.setBackgroundColor(getResources().getColor(R.color.grey));
                } else {
                    btnCompleteProfil.setVisibility(View.INVISIBLE);
                    btn_continue.setEnabled(true);
                    btn_continue.setBackgroundColor(getResources().getColor(R.color.green));
                }
            }
            @Override
            public void onFailure(Throwable t) {
                Log.d("AKSES KE SERVER GAGAL",t.getMessage());
            }
        });
    }
}
