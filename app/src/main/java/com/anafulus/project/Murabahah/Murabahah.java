package com.anafulus.project.Murabahah;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.anafulus.project.PageDashboard;
import com.anafulus.project.R;
import com.anafulus.project.helpers.NumberTextWatcher;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class Murabahah extends AppCompatActivity {
    SharedPreferences preferences;
    private TextView txtDetLoanPrincipal, txtDetTenor, txtDetInstallment, txtDetTotalInstallment;
    private Toolbar toolbar;
    private EditText txtloanPrincipal, txtReason;
    private LinearLayout layout_result;
    private Button btn_count, btn_borrow;
    private Spinner spinnerTenor, spinnerInstallment;
    String string;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.murabahah);
        preferences = getSharedPreferences("borrower_profil",MODE_PRIVATE);

        //Toolbar & Button Back
        toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Simulasi Pinjaman");
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Murabahah.this, PageDashboard.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                finish();
            }
        });

        txtloanPrincipal = findViewById(R.id.loanPrincipal);
        spinnerTenor = findViewById(R.id.spinner_tenor);
        spinnerInstallment = findViewById(R.id.installment);
        txtReason = findViewById(R.id.reason);

        Locale locale = new Locale("es", "AR"); // For example Argentina
        int numDecs = 2; // Let's use 2 decimals
        TextWatcher tw = new NumberTextWatcher(txtloanPrincipal, locale, numDecs);
        txtloanPrincipal.addTextChangedListener(tw);

        txtloanPrincipal.requestFocus();
        txtloanPrincipal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layout_result.setVisibility(View.GONE);
            }
        });

        // Initializing a String Array
        String[] plants = new String[]{
                "Pilih Tenor...",
                "1 minggu","2 minggu","3 minggu","4 minggu","5 minggu","6 minggu","7 minggu","8 minggu","9 minggu","10 minggu","11 minggu",
                "11 minggu","12 minggu","13 minggu","14 minggu","15 minggu","16 minggu","17 minggu","19 minggu","20 minggu",
                "21 Minggu","22 minggu","23 minggu","24 minggu"
        };

        final List<String> plantsList = new ArrayList<>(Arrays.asList(plants));

        // Initializing an ArrayAdapter
        final ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                this,R.layout.spinner_item,plantsList){
            @Override
            public boolean isEnabled(int position){
                if(position == 0)
                {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                }
                else
                {
                    return true;
                }
            }
            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if(position == 0){
                    // Set the hint text color gray
                    tv.setTextColor(Color.GRAY);
                }
                else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };
        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
        spinnerTenor.setAdapter(spinnerArrayAdapter);
        spinnerTenor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItemText = (String) parent.getItemAtPosition(position);
                // If user change the default selection
                // First item is disable and it is used for hint
                if (position == 1){
                    Toast.makeText(Murabahah.this, "Bulan Dipilih", Toast.LENGTH_SHORT).show();
                } else if(position == 2 ){
                    Toast.makeText(Murabahah.this, "Minggu Dipilih", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //Spinner Type
        // Initializing a String Array
        String[] plants2 = new String[]{
                "Pilih Tipe Cicilan...",
                "Bulanan",
                "Mingguan"
        };

        final List<String> plantsList2 = new ArrayList<>(Arrays.asList(plants2));

        // Initializing an ArrayAdapter
        final ArrayAdapter<String> spinnerArrayAdapter2 = new ArrayAdapter<String>(
                this,R.layout.spinner_item,plantsList2){
            @Override
            public boolean isEnabled(int position){
                if(position == 0)
                {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                }
                else
                {
                    return true;
                }
            }
            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if(position == 0){
                    // Set the hint text color gray
                    tv.setTextColor(Color.GRAY);
                }
                else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };
        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
        spinnerInstallment.setAdapter(spinnerArrayAdapter2);
        spinnerInstallment.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItemText = (String) parent.getItemAtPosition(position);
                // If user change the default selection
                // First item is disable and it is used for hint
                if (position == 1){
                    Toast.makeText(Murabahah.this, "Bulanan Dipilih", Toast.LENGTH_SHORT).show();
                } else if(position == 2 ){
                    Toast.makeText(Murabahah.this, "Mingguan Dipilih", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //Layout result simulation
        layout_result = findViewById(R.id.layout_result);
        layout_result.setVisibility(View.GONE);

        //Button count
        btn_count = findViewById(R.id.btnCount);
        btn_count.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Integer minloanPrincipal = 500000;
                Integer maxloanPrincipal = 50000000;
                if((txtloanPrincipal.getText().toString().equals(""))) {
                    txtloanPrincipal.setError("Total Pinjaman belum diisi");
                }else if(Integer.parseInt(txtloanPrincipal.getText().toString().replaceAll("[-+.^:,]", "")) < minloanPrincipal){
                    txtloanPrincipal.setError("Total Pinjaman melewati batas minimum");
                }else if(Integer.parseInt(txtloanPrincipal.getText().toString().replaceAll("[-+.^:,]", "")) > maxloanPrincipal){
                    txtloanPrincipal.setError("Total Pinjaman melewati batas maximum");
                }else if (txtReason.getText().toString().isEmpty()){
                    txtReason.setError("Alasan belum diisi");
                } else {
                    String tenorStatusArray[] = spinnerTenor.getSelectedItem().toString().split(" ");
                    String tenorValueInWeek = tenorStatusArray[0].toString();

                    if ((Integer.parseInt(txtloanPrincipal.getText().toString().replaceAll("[-+.^:,]", "")) <= maxloanPrincipal) && (Integer.parseInt(txtloanPrincipal.getText().toString().replaceAll("[-+.^:,]", "")) >= minloanPrincipal) && (!txtReason.getText().toString().isEmpty()) && (!tenorValueInWeek.isEmpty()) && (!txtloanPrincipal.getText().toString().isEmpty())) {
                        layout_result.setVisibility(View.VISIBLE);//
                        txtDetLoanPrincipal = findViewById(R.id.detLoanPrincipal);
                        txtDetTenor = findViewById(R.id.detTenor);
                        txtDetInstallment = findViewById(R.id.detInstallment);
                        txtDetTotalInstallment = findViewById(R.id.detTotalInstallment);

                        String loanPrincipal = txtloanPrincipal.getText().toString().replaceAll("[-+.^:,]", "");
                        String valueTenor = tenorValueInWeek;

                        double bunga = 0.24;

                        Double totalInstallment = null;
                        if(String.valueOf(spinnerInstallment.getSelectedItem()).equals("Bulanan")){
                            Integer totalMounthPerWeek = 12 ;
                            String tenorLoanInMounth = valueTenor;
                            Double result = installmentCalculation(loanPrincipal, String.valueOf(bunga), tenorLoanInMounth, totalMounthPerWeek);
                            totalInstallment = result;
                        } else {
                            Integer totalWeekPerYear = 52;
                            String tenorStatus[] = spinnerTenor.getSelectedItem().toString().split(" ");
                            String tenorLoanInWeek = tenorStatus[0].toString();
                            Double result = installmentCalculation(loanPrincipal, String.valueOf(bunga), tenorLoanInWeek, totalWeekPerYear);
                            totalInstallment = result;
                        }

                        Locale localeID = new Locale("in", "ID");
                        NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);

                        txtDetLoanPrincipal.setText(formatRupiah.format((double)Double.parseDouble(loanPrincipal)));
                        txtDetTenor.setText(spinnerTenor.getSelectedItem().toString());
                        txtDetInstallment.setText(String.valueOf(spinnerInstallment.getSelectedItem()));

                        if (String.valueOf(spinnerInstallment.getSelectedItem()).equals("Mingguan")){
                            txtDetTotalInstallment.setText(formatRupiah.format((double)Math.round(totalInstallment)) + " / " + "Minggu");
                        } else {
                            txtDetTotalInstallment.setText(formatRupiah.format((double)Math.round(totalInstallment)) + " / " + "Bulan");
                        }
                    }
                }
            }
        });

        //Button Borrow
        btn_borrow = findViewById(R.id.btn_borrow);
        btn_borrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Murabahah.this, MurabahahPersonalData.class);
                i.putExtra("loan_category", "Perorangan");
                i.putExtra("loan_type", "Murabahah");
                i.putExtra("id_borrower", preferences.getString("idBorrower",null));
                String xRupiah = txtDetLoanPrincipal.getText().toString().substring(1);
                String doubleRupiah = xRupiah.substring(1);
                i.putExtra("loan_principal", doubleRupiah.replaceAll("[-+.^:,]", ""));
                i.putExtra("tenor", txtDetTenor.getText().toString());
                i.putExtra("loan_purpose", txtReason.getText().toString());
                i.putExtra("loan_status", "Belum disetujui");
                i.putExtra("installment_type", txtDetInstallment.getText().toString());
                String[] items = txtDetTotalInstallment.getText().toString().split(" / ");
                i.putExtra("installment_nominal", items[0]);
                startActivity(i);
                finish();
            }
        });
    }

    public void onBackPressed(){
        Intent i = new Intent(Murabahah.this, PageDashboard.class);
        i.putExtra("finish", true);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
        finish();
    }

    // perhitungan simulasi pinjaman tergantung jenis cicilan, jika bjenis cicilan bulanan tenor diubah ke bulan, jika mingguan tenor diubah ke minggu
    //jumlah cicilan = (loanPrincipal * ( 1 + (( bunga * tenorLoan ) / totalWeekPerYear ))) / tenorLoan
    private double installmentCalculation (String loanPrincipal, String bunga, String tenorLoan, Integer totalWeekPerYear){

        double totalInstallment = (Integer.parseInt(loanPrincipal) * ( 1 + (( 0.24 * Integer.parseInt(tenorLoan)) / totalWeekPerYear ))) / Integer.parseInt(tenorLoan);
        return totalInstallment;
    }
}
