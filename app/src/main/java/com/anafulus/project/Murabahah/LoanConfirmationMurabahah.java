package com.anafulus.project.Murabahah;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.anafulus.project.R;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

public class LoanConfirmationMurabahah extends AppCompatActivity {
    private TextView txtLoanPrincipal, txtTenor, txtTypeInstallment, txtTypeLoan, txtInstallment, txtNameAgent, txtCodeAgent, txtPhoneNumberAgent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.loan_confirmation);

        //Toolbar & Button Back
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Konfirmasi Pinjaman");
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LoanConfirmationMurabahah.this, AgentCodeMurabahah.class);
                i.putExtra("loan_category", "Perorangan");
                i.putExtra("loan_type", "Murabahah");
                i.putExtra("id_borrower", getIntent().getExtras().getString("id_borrower"));
                i.putExtra("loan_principal", getIntent().getExtras().getString("loan_principal"));
                i.putExtra("tenor", getIntent().getExtras().getString("tenor"));
                i.putExtra("loan_purpose", getIntent().getExtras().getString("loan_purpose"));
                i.putExtra("loan_status", "Belum disetujui");
                i.putExtra("installment_nominal", getIntent().getExtras().getString("installment_nominal"));
                i.putExtra("installment_type",getIntent().getExtras().getString("installment_type"));
                startActivity(i);
                finish();
            }
        });

         txtLoanPrincipal = findViewById(R.id.loanPrincipal);
         txtTenor = findViewById(R.id.tenor);
         txtTypeInstallment = findViewById(R.id.typeInstallment);
         txtTypeLoan = findViewById(R.id.typeLoan);
         txtInstallment = findViewById(R.id.installment);
         txtNameAgent = findViewById(R.id.nameAgen);
         txtCodeAgent = findViewById(R.id.codeAgent);
         txtPhoneNumberAgent = findViewById(R.id.phoneNumberAgent);

        Locale localeID = new Locale("in", "ID");
        NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);

        txtNameAgent.setText(getIntent().getExtras().getString("name_agent"));
        txtCodeAgent.setText(getIntent().getExtras().getString("code_agent"));
        txtPhoneNumberAgent.setText(getIntent().getExtras().getString("phone_number_agent"));

        txtLoanPrincipal.setText(formatRupiah.format((double)Double.parseDouble(getIntent().getExtras().getString("loan_principal"))));
        txtTenor.setText(getIntent().getExtras().getString("tenor"));
        txtTypeInstallment.setText(getIntent().getExtras().getString("installment_type"));
        txtTypeLoan.setText("Murabahah");
        if (getIntent().getExtras().getString("installment_type").equals("Mingguan")){
            txtInstallment.setText(getIntent().getExtras().getString("installment_nominal") + " / " + "Minggu");
        } else {
            txtInstallment.setText(getIntent().getExtras().getString("installment_nominal") + " / " + "Bulan");
        }

        //BUTTON SUBMIT
        Button submit = findViewById(R.id.submit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            Intent i = new Intent(LoanConfirmationMurabahah.this, PinConfirmMurabahah.class);
            i.putExtra("loan_category", "Perorangan");
            i.putExtra("loan_type", "Murabahah");
            i.putExtra("id_borrower", getIntent().getExtras().getString("id_borrower"));
            i.putExtra("loan_principal", getIntent().getExtras().getString("loan_principal"));
            i.putExtra("tenor", getIntent().getExtras().getString("tenor"));
            i.putExtra("loan_purpose", getIntent().getExtras().getString("loan_purpose"));
            i.putExtra("loan_status", "Belum disetujui");
            String xRupiah = getIntent().getExtras().getString("installment_nominal").substring(1);
            String doubleRupiah = xRupiah.substring(1);
            i.putExtra("installment_nominal", doubleRupiah.replaceAll("[-+.^:,]", ""));
            i.putExtra("installment_type",getIntent().getExtras().getString("installment_type"));
            i.putExtra("id_agent",getIntent().getExtras().getString("id_agent"));
            startActivity(i);
            finish();
            }
        });

        //BUTTON CANCEL
        Button cancel = findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LoanConfirmationMurabahah.this, AgentCodeMurabahah.class);
                i.putExtra("loan_category", "Perorangan");
                i.putExtra("loan_type", "Murabahah");
                i.putExtra("id_borrower", getIntent().getExtras().getString("id_borrower"));
                i.putExtra("loan_principal", getIntent().getExtras().getString("loan_principal"));
                i.putExtra("tenor", getIntent().getExtras().getString("tenor"));
                i.putExtra("loan_purpose", getIntent().getExtras().getString("loan_purpose"));
                i.putExtra("loan_status", "Belum disetujui");
                i.putExtra("installment_nominal", getIntent().getExtras().getString("installment_nominal"));
                i.putExtra("installment_type",getIntent().getExtras().getString("installment_type"));
                startActivity(i);
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(LoanConfirmationMurabahah.this, AgentCodeMurabahah.class);
        i.putExtra("loan_category", "Perorangan");
        i.putExtra("loan_type", "Murabahah");
        i.putExtra("id_borrower", getIntent().getExtras().getString("id_borrower"));
        i.putExtra("loan_principal", getIntent().getExtras().getString("loan_principal"));
        i.putExtra("tenor", getIntent().getExtras().getString("tenor"));
        i.putExtra("loan_purpose", getIntent().getExtras().getString("loan_purpose"));
        i.putExtra("loan_status", "Belum disetujui");
        i.putExtra("installment_nominal", getIntent().getExtras().getString("installment_nominal"));
        i.putExtra("installment_type",getIntent().getExtras().getString("installment_type"));
        startActivity(i);
        finish();
    }

}
