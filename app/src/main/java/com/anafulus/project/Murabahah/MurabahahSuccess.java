package com.anafulus.project.Murabahah;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.anafulus.project.PageDashboard;
import com.anafulus.project.R;

public class MurabahahSuccess extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.success);

        //BUTTON BACK TO DASHBOARD
        Button back = findViewById(R.id.btn_back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MurabahahSuccess.this, PageDashboard.class);
                startActivity(i);
                finish();
            }
        });

    }

    @Override
    public void onBackPressed() {

    }
}
