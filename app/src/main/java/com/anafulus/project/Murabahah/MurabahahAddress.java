package com.anafulus.project.Murabahah;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.anafulus.project.Config.BorrowerApi;
import com.anafulus.project.Config.RetrofitClient;
import com.anafulus.project.Models.Borrowers;
import com.anafulus.project.R;
import com.kofigyan.stateprogressbar.StateProgressBar;

import java.text.NumberFormat;
import java.util.Locale;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class MurabahahAddress extends AppCompatActivity {
    SharedPreferences preferences;
    BorrowerApi mApiInterface;

    private TextView address, addressDomicile;
    private Toolbar toolbar;
    private Button btnCompleteProfil, btn_change, btnContinue;
    String[] descriptionData = {"Data Diri", "Alamat", "Persyaratan","Kode Agen"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reguler_address);
        preferences = getSharedPreferences("borrower_profil", Context.MODE_PRIVATE);
        mApiInterface = RetrofitClient.getClient().create(BorrowerApi.class);
        getAddressData();

        address = findViewById(R.id.address);
        addressDomicile = findViewById(R.id.addressDomicile);

        //Toolbar & Button Back
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Alamat");
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MurabahahAddress.this, MurabahahPersonalData.class);
                i.putExtra("loan_category", "Perorangan");
                i.putExtra("loan_type", "Murabahah");
                i.putExtra("id_borrower", getIntent().getExtras().getString("id_borrower"));
                i.putExtra("loan_principal", getIntent().getExtras().getString("loan_principal"));
                i.putExtra("tenor", getIntent().getExtras().getString("tenor"));
                i.putExtra("loan_purpose", getIntent().getExtras().getString("loan_purpose"));
                i.putExtra("loan_status", "Belum disetujui");
                i.putExtra("installment_nominal", getIntent().getExtras().getString("installment_nominal"));
                i.putExtra("installment_type",getIntent().getExtras().getString("installment_type"));
                startActivity(i);
                finish();
            }
        });

        //Stepview Status
        StateProgressBar stateProgressBar = (StateProgressBar) findViewById(R.id.stepview);
        stateProgressBar.setStateDescriptionData(descriptionData);

        //Button Complete
        btnCompleteProfil = findViewById(R.id.btn_complete);
        btnCompleteProfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MurabahahAddress.this, ChangeMurabahahAddress.class);
                startActivity(i);

            }
        });

        //Button Change
        btn_change = findViewById(R.id.btn_change);
        btn_change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MurabahahAddress.this, ChangeMurabahahAddress.class);
                startActivity(i);
            }
        });

        //Button Continue
        btnContinue = findViewById(R.id.btn_continue);
        btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MurabahahAddress.this, MurabahahRequirement.class);
                i.putExtra("loan_category", "Perorangan");
                i.putExtra("loan_type", "Murabahah");
                i.putExtra("id_borrower", getIntent().getExtras().getString("id_borrower"));
                i.putExtra("loan_principal", getIntent().getExtras().getString("loan_principal"));
                i.putExtra("tenor", getIntent().getExtras().getString("tenor"));
                i.putExtra("loan_purpose", getIntent().getExtras().getString("loan_purpose"));
                i.putExtra("loan_status", "Belum disetujui");
                i.putExtra("installment_nominal", getIntent().getExtras().getString("installment_nominal"));
                i.putExtra("installment_type",getIntent().getExtras().getString("installment_type"));
                startActivity(i);
            }
        });

        }
    public void onBackPressed () {
        Intent i = new Intent(MurabahahAddress.this, MurabahahPersonalData.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        i.putExtra("loan_category", "Perorangan");
        i.putExtra("loan_type", "Murabahah");
        i.putExtra("id_borrower", getIntent().getExtras().getString("id_borrower"));
        i.putExtra("loan_principal", getIntent().getExtras().getString("loan_principal"));
        i.putExtra("tenor", getIntent().getExtras().getString("tenor"));
        i.putExtra("loan_purpose", getIntent().getExtras().getString("loan_purpose"));
        i.putExtra("loan_status", "Belum disetujui");
        i.putExtra("installment_nominal", getIntent().getExtras().getString("installment_nominal"));
        i.putExtra("installment_type",getIntent().getExtras().getString("installment_type"));
        startActivity(i);
        finish();
    }
    private void getAddressData() {
        String idBorrower= preferences.getString("idBorrower",null);
        Call<Borrowers> postKontakCall = mApiInterface.dataBorrower(idBorrower);
        postKontakCall.enqueue(new Callback<Borrowers>() {
            @Override
            public void onResponse(Response<Borrowers> response, Retrofit retrofit) {
                int i =0 ;
                String strAddress = "";
                if (response.body().getListDataBorrower().get(i).getVillage_name()==null){
                    strAddress ="";
                } else {
                    strAddress = response.body().getListDataBorrower().get(i).getId_card_address() + ", " + response.body().getListDataBorrower().get(i).getVillage_name() + ", " +response.body().getListDataBorrower().get(i).getSub_district_name() + ", " + response.body().getListDataBorrower().get(i).getRegency_name() + ", " + response.body().getListDataBorrower().get(i).getProvince_name();
                }

                String strAdressDomicile = response.body().getListDataBorrower().get(i).getDomicile_address();

                address.setText(strAddress);
                addressDomicile.setText(strAdressDomicile);

                if ((strAddress.isEmpty()) || (strAdressDomicile.isEmpty())) {
                    btnCompleteProfil.setVisibility(View.VISIBLE);
                    btnContinue.setEnabled(false);
                    btnContinue.setBackgroundColor(getResources().getColor(R.color.grey));
                } else {
                    btnCompleteProfil.setVisibility(View.INVISIBLE);
                    btnContinue.setEnabled(true);
                    btnContinue.setBackgroundColor(getResources().getColor(R.color.green));
                }
            }
            @Override
            public void onFailure(Throwable t) {
                Log.d("AKSES KE SERVER GAGAL",t.getMessage());
            }
        });
    }
}
