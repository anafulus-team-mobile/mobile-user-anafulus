package com.anafulus.project.Murabahah;

public class AdapterAgentMurabahah {
    private String check;
    private String phone;
    private String name;

    public AdapterAgentMurabahah(String check, String phone, String name) {
        this.check = check;
        this.phone = phone;
        this.name = name;
    }

    public String getCheck() {
        return check;
    }

    public void setCheck(String check) {
        this.check = check;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public  String getName (){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }
}
