package com.anafulus.project.Reguler;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.anafulus.project.Config.AgentApi;
import com.anafulus.project.Config.BorrowerApi;
import com.anafulus.project.Config.RetrofitClient;
import com.anafulus.project.Models.Agent;
import com.anafulus.project.Models.Agents;
import com.anafulus.project.Models.Borrowers;
import com.anafulus.project.Murabahah.AgentFoundMurabahah;
import com.anafulus.project.Murabahah.AgentListAdapterMurabahah;
import com.anafulus.project.Murabahah.LoanConfirmationMurabahah;
import com.anafulus.project.R;

import java.util.ArrayList;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class AgentFoundReguler extends AppCompatActivity {
    SharedPreferences preferences;
    SharedPreferences preferencesAgentChecked;
    BorrowerApi mApiInterface;
    AgentApi mApiInterfaceAgent;

    private TextView txtProvincyName, txtRegencyName, txtSubdistrictName, txtWarning;
    private RelativeLayout layoutFound, layoutNotFound;
    private Toolbar toolbar;
    private Button btn_call, btn_continue;
    private ListView lv_agent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.agent_found);

        preferencesAgentChecked = getSharedPreferences("agent_checked",MODE_PRIVATE);
        preferences = getSharedPreferences("borrower_profil", Context.MODE_PRIVATE);
        mApiInterface = RetrofitClient.getClient().create(BorrowerApi.class);
        mApiInterfaceAgent = RetrofitClient.getClient().create(AgentApi.class);

        getBorrowerData();

        txtProvincyName = findViewById(R.id.provincyName);
        txtRegencyName = findViewById(R.id.RegencyName);
        txtSubdistrictName = findViewById(R.id.subdistrictName);
        layoutFound = findViewById(R.id.layout_found);
        layoutNotFound = findViewById(R.id.layout_notfound);
        txtWarning = findViewById(R.id.warning);

        //Toolbar
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Agen Ditemukan");
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(AgentFoundReguler.this, AgentCodeReguler.class);
                i.putExtra("loan_category", "Perorangan");
                i.putExtra("loan_type", "Murabahah");
                i.putExtra("id_borrower", getIntent().getExtras().getString("id_borrower"));
                i.putExtra("loan_principal", getIntent().getExtras().getString("loan_principal"));
                i.putExtra("tenor", getIntent().getExtras().getString("tenor"));
                i.putExtra("loan_purpose", getIntent().getExtras().getString("loan_purpose"));
                i.putExtra("loan_status", "Belum disetujui");
                i.putExtra("installment_nominal", getIntent().getExtras().getString("installment_nominal"));
                i.putExtra("installment_type",getIntent().getExtras().getString("installment_type"));
                startActivity(i);
                finish();
            }
        });

        //Button Continue
        btn_continue = findViewById(R.id.btn_continue);
        btn_continue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (preferencesAgentChecked.getAll().isEmpty()) {
                    txtWarning.setVisibility(View.VISIBLE);
                } else {
                    txtWarning.setVisibility(View.GONE);
                    Intent i = new Intent(AgentFoundReguler.this, LoanConfirmationReguler.class);
                    i.putExtra("loan_category", "Perorangan");
                    i.putExtra("loan_type", "Murabahah");
                    i.putExtra("id_borrower", getIntent().getExtras().getString("id_borrower"));
                    i.putExtra("loan_principal", getIntent().getExtras().getString("loan_principal"));
                    i.putExtra("tenor", getIntent().getExtras().getString("tenor"));
                    i.putExtra("loan_purpose", getIntent().getExtras().getString("loan_purpose"));
                    i.putExtra("loan_status", "Belum disetujui");
                    i.putExtra("installment_nominal", getIntent().getExtras().getString("installment_nominal"));
                    i.putExtra("installment_type",getIntent().getExtras().getString("installment_type"));
                    i.putExtra("name_agent",preferencesAgentChecked.getString("name",null));
                    i.putExtra("code_agent",preferencesAgentChecked.getString("code_agent",null));
                    i.putExtra("phone_number_agent",preferencesAgentChecked.getString("phone_number_agent",null));
                    i.putExtra("id_agent",preferencesAgentChecked.getString("id",null));
                    startActivity(i);
                    finish();
                    SharedPreferences.Editor editor = preferencesAgentChecked.edit();
                    editor.clear();
                    editor.commit();
                }
            }
        });
    }

    //Button Back Smartphone
    public void onBackPressed() {
        Intent i = new Intent(AgentFoundReguler.this, AgentCodeReguler.class);
        i.putExtra("loan_category", "Perorangan");
        i.putExtra("loan_type", "Murabahah");
        i.putExtra("id_borrower", getIntent().getExtras().getString("id_borrower"));
        i.putExtra("loan_principal", getIntent().getExtras().getString("loan_principal"));
        i.putExtra("tenor", getIntent().getExtras().getString("tenor"));
        i.putExtra("loan_purpose", getIntent().getExtras().getString("loan_purpose"));
        i.putExtra("loan_status", "Belum disetujui");
        i.putExtra("installment_nominal", getIntent().getExtras().getString("installment_nominal"));
        i.putExtra("installment_type",getIntent().getExtras().getString("installment_type"));
        startActivity(i);
        finish();
    }

    private void getBorrowerData() {
        String idBorrower= preferences.getString("idBorrower",null);
        Call<Borrowers> postKontakCall = mApiInterface.dataBorrower(idBorrower);
        postKontakCall.enqueue(new Callback<Borrowers>() {
            @Override
            public void onResponse(Response<Borrowers> response, Retrofit retrofit) {
                int i =0 ;
                String strProvincyName = response.body().getListDataBorrower().get(i).getProvince_name();
                String strRegencyName = response.body().getListDataBorrower().get(i).getRegency_name();
                String strSubdistrictName = response.body().getListDataBorrower().get(i).getSub_district_name();
                getAgentData(response.body().getListDataBorrower().get(i).getId_sub_district().toString());

                txtProvincyName.setText(strProvincyName);
                txtRegencyName.setText(strRegencyName);
                txtSubdistrictName.setText(strSubdistrictName);

            }
            @Override
            public void onFailure(Throwable t) {
                Log.d("AKSES KE SERVER GAGAL",t.getMessage());
            }
        });
    }
    private void getAgentData(String idSubdistrict) {
        Call<Agents> getDataAgent = mApiInterfaceAgent.dataAgent(idSubdistrict);
        getDataAgent.enqueue(new Callback<Agents>() {
            @Override
            public void onResponse(Response<Agents> response, Retrofit retrofit) {
                int i =0 ;
                if ((response.body().getListDataAgent()==null)){
                    layoutFound.setVisibility(View.GONE);
                    layoutNotFound.setVisibility(View.VISIBLE);
                } else {
                    layoutFound.setVisibility(View.VISIBLE);
                    layoutNotFound.setVisibility(View.GONE);
                    ArrayList<Agent> modelAgentArrayList = new ArrayList<>();
                    for (int a = 0; a < response.body().getListDataAgent().size(); a++) {

                        Agent agentView = new Agent();
                        agentView.setId(response.body().getListDataAgent().get(a).getId());
                        agentView.setName(response.body().getListDataAgent().get(a).getName());
                        agentView.setAgent_code(response.body().getListDataAgent().get(a).getAgent_code());
                        agentView.setPhone_number(response.body().getListDataAgent().get(a).getPhone_number());
                        modelAgentArrayList.add(agentView);
                    }
                    makeToList(modelAgentArrayList);
                }

            }
            @Override
            public void onFailure(Throwable t) {
                Log.d("AKSES KE SERVER GAGAL",t.getMessage());
            }
        });
    }

    private void makeToList(ArrayList<Agent> dataAgent) {
        ListView listViewNotify;
        AgentListAdapterReguler agentListAdapterMurabahah;
        listViewNotify = findViewById(R.id.lv_agent);
        agentListAdapterMurabahah = new AgentListAdapterReguler(this, dataAgent);
        listViewNotify.setAdapter(agentListAdapterMurabahah);
    }
}
