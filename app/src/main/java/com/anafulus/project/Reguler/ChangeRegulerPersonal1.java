package com.anafulus.project.Reguler;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.anafulus.project.Config.BorrowerApi;
import com.anafulus.project.Config.RetrofitClient;
import com.anafulus.project.Models.Borrowers;
import com.anafulus.project.Murabahah.ChangeMurabahahPersonal;
import com.anafulus.project.Murabahah.MurabahahPersonalData;
import com.anafulus.project.R;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class ChangeRegulerPersonal1 extends AppCompatActivity {
    SharedPreferences preferences;
    BorrowerApi mApiInterface;

    private static final String TAG = "ChangeRegulerPersonal1";
    private TextView displaydate, txtName, txtPhoneNumber, genderTxt, txtBirtDate;
    private Toolbar toolbar;
    private Button btn_change, btn_save;
    private Spinner spnGender, spnEducation;
    private CheckBox isSwasta, isNegeri, isGuruDosen, isOther;
    private EditText txtEmailBorrower, txtLocalId, txtFamilyId, txtNpwpId, txtIncome, txtLastJob;
    private DatePickerDialog.OnDateSetListener dateSetListener;
    String string;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.change_data);

        mApiInterface = RetrofitClient.getClient().create(BorrowerApi.class);

        preferences = getSharedPreferences("borrower_profil",MODE_PRIVATE);

        Locale localeID = new Locale("in", "ID");
        NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);


        //INITIAL
        txtName = findViewById(R.id.name);
        txtBirtDate = findViewById(R.id.txtBirthDate);
        displaydate = findViewById(R.id.birthDate);
        spnGender = findViewById(R.id.gender);
        genderTxt = findViewById(R.id.genderTxt);
        txtPhoneNumber = findViewById(R.id.phoneNumber);
        spnEducation = findViewById(R.id.lastEducation);
        txtLocalId = findViewById(R.id.localId);
        txtFamilyId = findViewById(R.id.familyCardId);
        txtNpwpId = findViewById(R.id.npwp);
        txtIncome = findViewById(R.id.financeBalance);
        txtEmailBorrower = findViewById(R.id.email);
        isSwasta = findViewById(R.id.isSwasta);
        isNegeri = findViewById(R.id.isNegeri);
        isGuruDosen = findViewById(R.id.isGuruDosen);
        isOther = findViewById(R.id.isOther);
        txtLastJob = findViewById(R.id.otherJob);

        txtName.setText(getIntent().getExtras().getString("name"));
        txtEmailBorrower.setText(getIntent().getExtras().getString("email"));
        txtPhoneNumber.setText(getIntent().getExtras().getString("phone_number"));
        if (getIntent().getExtras().getString("gender").isEmpty()){
            spnGender.setVisibility(View.VISIBLE);
            genderTxt.setVisibility(View.GONE);
        } else {
            spnGender.setVisibility(View.GONE);
            genderTxt.setVisibility(View.VISIBLE);
            genderTxt.setText(getIntent().getExtras().getString("gender"));
        }
        if (getIntent().getExtras().getString("birthdate").isEmpty()) {
            displaydate.setVisibility(View.VISIBLE);
            txtBirtDate.setVisibility(View.GONE);
        } else {
            displaydate.setVisibility(View.GONE);
            txtBirtDate.setVisibility(View.VISIBLE);
            txtBirtDate.setText(getIntent().getExtras().getString("birthdate"));
        }
        if(getIntent().getExtras().getString("local_id").equals("0")){
            txtLocalId.setText("");
        } else{
            txtLocalId.setText(getIntent().getExtras().getString("local_id"));
        }
        if(getIntent().getExtras().getString("family_id").equals("0")){
            txtFamilyId.setText("");
        } else{
            txtFamilyId.setText(getIntent().getExtras().getString("family_id"));
        }
        if(getIntent().getExtras().getString("npwp_id").equals("0")){
            txtNpwpId.setText("");
        } else{
            txtNpwpId.setText(getIntent().getExtras().getString("npwp_id"));
        }
        if(getIntent().getExtras().getString("income").equals("0")){
            txtIncome.setText("");
        } else{
            txtIncome.setText(getIntent().getExtras().getString("income"));
        }
        if (getIntent().getExtras().getString("lastjob").equals("Pegawai Swasta")) {
            isSwasta.setChecked(true);
            isGuruDosen.setChecked(false);
            isNegeri.setChecked(false);
            isOther.setChecked(false);
            txtLastJob.setEnabled(false);
            txtLastJob.setVisibility(View.GONE);
        } else if(getIntent().getExtras().getString("lastjob").equals("Pegawai Negeri")){
            isSwasta.setChecked(false);
            isGuruDosen.setChecked(false);
            isNegeri.setChecked(true);
            isOther.setChecked(false);
            txtLastJob.setEnabled(false);
            txtLastJob.setVisibility(View.GONE);
        } else if (getIntent().getExtras().getString("lastjob").equals("Guru/Dosen")){
            isSwasta.setChecked(false);
            isGuruDosen.setChecked(true);
            isNegeri.setChecked(false);
            isOther.setChecked(false);
            txtLastJob.setEnabled(false);
            txtLastJob.setVisibility(View.GONE);
        } else if (getIntent().getExtras().getString("lastjob").isEmpty()){
            isSwasta.setChecked(false);
            isGuruDosen.setChecked(false);
            isNegeri.setChecked(false);
            isOther.setChecked(false);
            txtLastJob.setEnabled(false);
            txtLastJob.setVisibility(View.GONE);
        } else {
            isSwasta.setChecked(false);
            isGuruDosen.setChecked(false);
            isNegeri.setChecked(false);
            isOther.setChecked(true);
            txtLastJob.setVisibility(View.VISIBLE);
            txtLastJob.setEnabled(true);
            txtLastJob.setText(getIntent().getExtras().getString("lastjob"));
        }

        isSwasta.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                {
                    isSwasta.setChecked(true);
                    isNegeri.setChecked(false);
                    isGuruDosen.setChecked(false);
                    isOther.setChecked(false);
                    txtLastJob.setVisibility(View.GONE);
                    txtLastJob.setEnabled(false);
                }
            }
        });
        isNegeri.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                {
                    isSwasta.setChecked(false);
                    isGuruDosen.setChecked(false);
                    isNegeri.setChecked(true);
                    isOther.setChecked(false);
                    txtLastJob.setVisibility(View.GONE);
                    txtLastJob.setEnabled(false);
                }
            }
        });
        isGuruDosen.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                {
                    isSwasta.setChecked(false);
                    isNegeri.setChecked(false);
                    isGuruDosen.setChecked(true);
                    isOther.setChecked(false);
                    txtLastJob.setVisibility(View.GONE);
                    txtLastJob.setEnabled(false);
                }
            }
        });
        isOther.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                {
                    isSwasta.setChecked(false);
                    isNegeri.setChecked(false);
                    isGuruDosen.setChecked(false);
                    isOther.setChecked(true);
                    txtLastJob.setVisibility(View.VISIBLE);
                    txtLastJob.setEnabled(true);
                }
            }
        });

        //Toolbar
        //Toolbar & Button Back
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Lengkapi Data Diri");
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ChangeRegulerPersonal1.this, RegulerPersonalData.class);
                i.putExtra("loan_category", "Perorangan");
                i.putExtra("loan_type", "Reguler");
                i.putExtra("id_borrower", getIntent().getExtras().getString("id_borrower"));
                i.putExtra("loan_principal", getIntent().getExtras().getString("loan_principal"));
                i.putExtra("tenor", getIntent().getExtras().getString("tenor"));
                i.putExtra("loan_purpose", "");
                i.putExtra("loan_status", "Belum disetujui");
                i.putExtra("installment_nominal", getIntent().getExtras().getString("installment_nominal"));
                i.putExtra("installment_type",getIntent().getExtras().getString("installment_type"));
                startActivity(i);
                finish();
            }
        });

        //Birthdate
        displaydate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar calendar = Calendar.getInstance();
                int year = calendar.get(Calendar.YEAR);
                int month = calendar.get(Calendar.MONTH);
                int day = calendar.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(ChangeRegulerPersonal1.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                                month = month +1;
                                displaydate.setText(day + "/" + month + "/" + year);
                            }
                        }, year, month, day);
                dialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                dialog.show();
            }
        });

        //BUTTON Change Phone Number
        btn_change = findViewById(R.id.btn_change);
        btn_change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ChangeRegulerPersonal1.this, ChangePonsel1.class);
                startActivity(i);
                finish();
            }
        });

        //Button Save Change
        btn_save = findViewById(R.id.btn_save);
        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String strIdBorrower = preferences.getString("idBorrower", null);
                String name = txtName.getText().toString();
                String phoneNumber = txtPhoneNumber.getText().toString();
                String email = txtEmailBorrower.getText().toString();
                String gender = (getIntent().getExtras().getString("gender").isEmpty()) ? String.valueOf(spnGender.getSelectedItem()) : genderTxt.getText().toString();
                String birthDateBorrower = (getIntent().getExtras().getString("birthdate").isEmpty()) ? String.valueOf(displaydate.getText()) : txtBirtDate.getText().toString();
                String lastJob = "";
                if(isSwasta.isChecked()) {
                    lastJob = "Pegawai Swasta";
                } else if (isNegeri.isChecked()){
                    lastJob ="Pegawai Negeri";
                } else if (isGuruDosen.isChecked()){
                    lastJob = "Guru/Dosen";
                } else if (isOther.isChecked()){
                    lastJob = txtLastJob.getText().toString();
                }
                String education = String.valueOf(spnEducation.getSelectedItem());

                Integer localId = 0;
                Integer familyId = 0;
                Integer npwpId = 0;
                Integer income = 0;
                if(getIntent().getExtras().getString("local_id").equals("")){
                    localId = 0;
                } else {
                    localId = Integer.parseInt(getIntent().getExtras().getString("local_id"));
                }
                if(getIntent().getExtras().getString("family_id").equals("")){
                    familyId = 0;
                } else {
                    familyId = Integer.parseInt(getIntent().getExtras().getString("family_id"));
                }
                if(getIntent().getExtras().getString("npwp_id").equals("")){
                    npwpId = 0;
                } else {
                    npwpId = Integer.parseInt(getIntent().getExtras().getString("npwp_id"));
                }
                if(getIntent().getExtras().getString("income").equals("")){
                    income = 0;
                } else {
                    income = Integer.parseInt(getIntent().getExtras().getString("income"));
                }
                updateProfil(strIdBorrower, name, phoneNumber, email, birthDateBorrower, gender, lastJob, education, localId, familyId, npwpId, income);
            }
        });

        // Initializing a String Array
        String[] gender = new String[]{
                "Jenis Kelamin...",
                "Laki - Laki",
                "Perempuan"
        };

        final List<String> genderList = new ArrayList<>(Arrays.asList(gender));

        // Initializing an ArrayAdapter
        final ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                this, R.layout.spinner_item, genderList) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                } else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if (position == 0) {
                    // Set the hint text color gray
                    tv.setTextColor(Color.GRAY);
                } else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };

        //Spinner Gender
        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
        spnGender.setAdapter(spinnerArrayAdapter);
        spnGender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItemText = (String) parent.getItemAtPosition(position);
                // If user change the default selection
                // First item is disable and it is used for hint
                if (position == 1) {
                    Toast.makeText(ChangeRegulerPersonal1.this, "Laki - laki Dipilih", Toast.LENGTH_SHORT).show();
                } else if (position == 2) {
                    Toast.makeText(ChangeRegulerPersonal1.this, "Perempuan Dipilih", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        int spinnerPosition = spinnerArrayAdapter.getPosition("Laki-laki");
        spnGender.setSelection(spinnerPosition);

        //Spinner Education
        spnEducation = findViewById(R.id.lastEducation);

        // Initializing a String Array
        String[] education = new String[]{
                "Pendidikan...",
                "SD",
                "SMP",
                "SMA",
                "Sarjana S1"
        };

        final List<String> educationList = new ArrayList<>(Arrays.asList(education));

        // Initializing an ArrayAdapter
        final ArrayAdapter<String> spinnerArrayAdapter2 = new ArrayAdapter<String>(
                this, R.layout.spinner_item, educationList) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                } else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if (position == 0) {
                    // Set the hint text color gray
                    tv.setTextColor(Color.GRAY);
                } else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };

        //Spinner Education
        spinnerArrayAdapter2.setDropDownViewResource(R.layout.spinner_item);
        spnEducation.setAdapter(spinnerArrayAdapter2);
        if (getIntent().getExtras().getString("last_education").equals("SD")){
            spnEducation.setSelection(1);
        } else if(getIntent().getExtras().getString("last_education").equals("SMP")){
            spnEducation.setSelection(2);
        } else if(getIntent().getExtras().getString("last_education").equals("SMA")){
            spnEducation.setSelection(3);
        } else if(getIntent().getExtras().getString("last_education").equals("Sarjana S1")){
            spnEducation.setSelection(4);
        } else {
            spnEducation.setSelection(0);
        }

        spnEducation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItemText = (String) parent.getItemAtPosition(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    private void updateProfil(String idBorrower, String name, String phoneNumber, String email, String birtDate, String gender, String lastJob, String education, Integer localId, Integer familyId, Integer npwpId, Integer income) {
        Call<Borrowers> borrower = mApiInterface.updateProfil(idBorrower, name, phoneNumber, email, birtDate, gender, lastJob, education, localId, familyId, npwpId, income);
        borrower.enqueue(new Callback<Borrowers>() {
            @Override
            public void onResponse(Response<Borrowers> response, Retrofit retrofit) {
                if (!response.isSuccess()) {
                    Toast.makeText(ChangeRegulerPersonal1.this, "Gagal Memperbaharui Data", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(ChangeRegulerPersonal1.this, "Berhasil Memperbaharui Data", Toast.LENGTH_LONG).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            final Intent i = new Intent(ChangeRegulerPersonal1.this, RegulerPersonalData.class);
                            i.putExtra("loan_category", "Perorangan");
                            i.putExtra("loan_type", "Murabahah");
                            i.putExtra("id_borrower", getIntent().getExtras().getString("id_borrower"));
                            i.putExtra("loan_principal", getIntent().getExtras().getString("loan_principal"));
                            i.putExtra("tenor", getIntent().getExtras().getString("tenor"));
                            i.putExtra("loan_purpose", getIntent().getExtras().getString("loan_purpose"));
                            i.putExtra("loan_status", "Belum disetujui");
                            i.putExtra("installment_nominal", getIntent().getExtras().getString("installment_nominal"));
                            i.putExtra("installment_type",getIntent().getExtras().getString("installment_type"));
                            startActivity(i);
                            finish();
                        }
                    }, 1000);
                }
            }
            @Override
            public void onFailure(Throwable t) {
                Log.d("AKSES KE SERVER GAGAL", t.getMessage());
                Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
            }
        });
    }
    @Override
    public void onBackPressed() {
        Intent i = new Intent(ChangeRegulerPersonal1.this, RegulerPersonalData.class);
        i.putExtra("loan_category", "Perorangan");
        i.putExtra("loan_type", "Reguler");
        i.putExtra("id_borrower", getIntent().getExtras().getString("id_borrower"));
        i.putExtra("loan_principal", getIntent().getExtras().getString("loan_principal"));
        i.putExtra("tenor", getIntent().getExtras().getString("tenor"));
        i.putExtra("loan_purpose", "");
        i.putExtra("loan_status", "Belum disetujui");
        i.putExtra("installment_nominal", getIntent().getExtras().getString("installment_nominal"));
        i.putExtra("installment_type",getIntent().getExtras().getString("installment_type"));
        startActivity(i);
        finish();
    }
}
