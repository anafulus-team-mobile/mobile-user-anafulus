package com.anafulus.project.Reguler;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.alimuzaffar.lib.pin.PinEntryEditText;
import com.anafulus.project.Config.BorrowerApi;
import com.anafulus.project.Config.RetrofitClient;
import com.anafulus.project.Models.Borrowers;
import com.anafulus.project.Murabahah.MurabahahSuccess;
import com.anafulus.project.Murabahah.PinConfirmMurabahah;
import com.anafulus.project.PageDashboard;
import com.anafulus.project.R;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class PinConfirmReguler extends AppCompatActivity {
    BorrowerApi mApiInterface;
    private PinEntryEditText edtxtPin;

    private Toolbar toolbar;
    private TextView text_forgot;
    private Button btn_change;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pin_confirm);
        mApiInterface = RetrofitClient.getClient().create(BorrowerApi.class);
        edtxtPin = findViewById(R.id.pinTxt);

        //Toolbar
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Konfirmasi PIN Anda");
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(PinConfirmReguler.this, AgentCodeReguler.class);
                i.putExtra("loan_category", "Perorangan");
                i.putExtra("loan_type", "Murabahah");
                i.putExtra("id_borrower", getIntent().getExtras().getString("id_borrower"));
                i.putExtra("loan_principal", getIntent().getExtras().getString("loan_principal"));
                i.putExtra("tenor", getIntent().getExtras().getString("tenor"));
                i.putExtra("loan_purpose", getIntent().getExtras().getString("loan_purpose"));
                i.putExtra("loan_status", "Belum disetujui");
                i.putExtra("installment_nominal", getIntent().getExtras().getString("installment_nominal"));
                i.putExtra("installment_type",getIntent().getExtras().getString("installment_type"));
                startActivity(i);
                finish();
            }
        });

        //TextView Forgot PIN
        text_forgot = findViewById(R.id.text_forgot);
        text_forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(PinConfirmReguler.this, ForgotPinReguler.class);
                startActivity(i);
                finish();
            }
        });

        //Button confirm pin
        btn_change = findViewById(R.id.btn_change);
        btn_change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String pinConfirmation = edtxtPin.getText().toString();
                String strIdBorrower = getIntent().getExtras().getString("id_borrower");
                checkPin(strIdBorrower, pinConfirmation);

//                Toast.makeText(PinConfirmReguler.this, "Pinjaman berhasil diajukan", Toast.LENGTH_SHORT).show();
//                Intent i = new Intent(PinConfirmReguler.this, PageDashboard.class);
//                i.putExtra("finish", true);
//                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//                startActivity(i);
//                finish();
            }
        });

    }

    private void checkPin(String idBorrower, String pin) {
        Call<Borrowers> borrower = mApiInterface.checkPin(idBorrower, pin);
        borrower.enqueue(new Callback<Borrowers>() {
            @Override
            public void onResponse(Response<Borrowers> response, Retrofit retrofit) {
                if (!response.isSuccess()) {
                    edtxtPin.setError("Pin salah");
                    Toast.makeText(PinConfirmReguler.this, "Pin Salah", Toast.LENGTH_LONG).show();
                } else {
                    String loan_category = "Perorangan";
                    String loan_type = "Reguler";
                    Integer id_borrower = Integer.parseInt(getIntent().getExtras().getString("id_borrower"));
                    Integer loan_principal = Integer.parseInt(getIntent().getExtras().getString("loan_principal"));
                    String tenor = getIntent().getExtras().getString("tenor");
                    String loan_purpose = getIntent().getExtras().getString("loan_purpose");
                    String loan_status = "Belum disetujui";
                    Integer installment_nominal = Integer.parseInt(getIntent().getExtras().getString("installment_nominal"));
                    String installment_type = getIntent().getExtras().getString("installment_type");
                    Integer id_agent = Integer.parseInt(getIntent().getExtras().getString("id_agent"));

                    createLoan(loan_category, loan_type, id_borrower, loan_principal, tenor, loan_purpose, loan_status, installment_nominal, installment_type, id_agent);
                }
            }
            @Override
            public void onFailure(Throwable t) {
                Log.d("AKSES KE SERVER GAGAL", t.getMessage());
//                Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
            }
        });
    }
    private void createLoan(String loanCategory, String loanType, Integer idBorrower, Integer loanPrincipal, String tenor, String loanPurpose, String loanStatus, Integer installmentNominal, String installmentType, Integer idAgent) {
        Call<Borrowers> borrower = mApiInterface.createLoan(idBorrower, idAgent, loanCategory, loanType, loanPrincipal, tenor, installmentNominal, loanStatus, loanPurpose, installmentType);
        borrower.enqueue(new Callback<Borrowers>() {
            @Override
            public void onResponse(Response<Borrowers> response, Retrofit retrofit) {
                if (!response.isSuccess()) {
                    Toast.makeText(PinConfirmReguler.this, "Gagal Melakukan Pinjaman", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(PinConfirmReguler.this, "Pinjaman berhasil diajukan", Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(PinConfirmReguler.this, MurabahahSuccess.class);
                    i.putExtra("finish", true);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                    finish();
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Log.d("AKSES KE SERVER GAGAL", t.getMessage());
                Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
            }
        });
    }
    public void onBackPressed() {
        Intent i = new Intent(PinConfirmReguler.this, AgentCodeReguler.class);
        i.putExtra("loan_category", "Perorangan");
        i.putExtra("loan_type", "Murabahah");
        i.putExtra("id_borrower", getIntent().getExtras().getString("id_borrower"));
        i.putExtra("loan_principal", getIntent().getExtras().getString("loan_principal"));
        i.putExtra("tenor", getIntent().getExtras().getString("tenor"));
        i.putExtra("loan_purpose", getIntent().getExtras().getString("loan_purpose"));
        i.putExtra("loan_status", "Belum disetujui");
        i.putExtra("installment_nominal", getIntent().getExtras().getString("installment_nominal"));
        i.putExtra("installment_type",getIntent().getExtras().getString("installment_type"));
        startActivity(i);
        finish();
    }
}
