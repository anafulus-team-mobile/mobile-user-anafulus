package com.anafulus.project.Reguler;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.RadioButton;
import android.widget.TextView;

import com.anafulus.project.Models.Agent;
import com.anafulus.project.Murabahah.AdapterAgentMurabahah;
import com.anafulus.project.Murabahah.AgentListAdapterMurabahah;
import com.anafulus.project.R;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

public class AgentListAdapterReguler extends BaseAdapter {
    SharedPreferences preferencesAgentChecked;
    private Context context;
    private ArrayList<Agent> dataAgent;
    private LayoutInflater mInflater;
    int selectedPosition =   -1;

    public AgentListAdapterReguler(Context context, ArrayList<Agent> dataAgent) {
        this.context = context;
        this.dataAgent = dataAgent;
        mInflater = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getViewTypeCount() {
        return dataAgent.size();
    }

    @Override

    public int getItemViewType(int position) {

        return position;
    }

    @Override
    public int getCount() {
        return dataAgent.size();
    }

    @Override
    public Object getItem(int position) {

        return dataAgent.get(position);

    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final AgentListAdapterReguler.ViewHolder holder;
        preferencesAgentChecked = context.getSharedPreferences("agent_checked",MODE_PRIVATE);

        if (convertView == null) {
            holder = new AgentListAdapterReguler.ViewHolder();
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView = inflater.inflate(R.layout.adapter_agent_murabahah, null, true);

            holder.check = convertView.findViewById(R.id.check);
            holder.phoneNumberAgent = convertView.findViewById(R.id.phone);
            holder.codeAgent = convertView.findViewById(R.id.codeAgen);
            holder.nameAgent = convertView.findViewById(R.id.name);

            convertView.setTag(holder);
        } else {
            // the getTag returns the viewHolder object set as a tag to the view
            holder = (AgentListAdapterReguler.ViewHolder) convertView.getTag();
        }

        holder.nameAgent.setText(dataAgent.get(position).getName());
        holder.phoneNumberAgent.setText(dataAgent.get(position).getPhone_number());
        holder.codeAgent.setText(dataAgent.get(position).getAgent_code());

        holder.check.setChecked(position == selectedPosition);
        holder.check.setTag(position);
        holder.check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedPosition = (Integer) view.getTag();
                SharedPreferences.Editor editor = preferencesAgentChecked.edit();
                editor.putString("id",dataAgent.get(selectedPosition).getId().toString());
                editor.putString("name",dataAgent.get(selectedPosition).getName());
                editor.putString("code_agent",dataAgent.get(selectedPosition).getAgent_code());
                editor.putString("phone_number_agent",dataAgent.get(selectedPosition).getPhone_number());
                editor.commit();
                notifyDataSetInvalidated();
                notifyDataSetChanged();
            }
        });
        return convertView;
    }

    private class ViewHolder {
        protected TextView phoneNumberAgent, codeAgent, nameAgent;
        protected RadioButton check;
    }
}