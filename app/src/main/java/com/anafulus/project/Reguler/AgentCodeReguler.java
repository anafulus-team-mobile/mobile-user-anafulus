package com.anafulus.project.Reguler;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.anafulus.project.Config.AgentApi;
import com.anafulus.project.Config.RetrofitClient;
import com.anafulus.project.Models.Agents;
import com.anafulus.project.Murabahah.AgentCodeMurabahah;
import com.anafulus.project.Murabahah.LoanConfirmationMurabahah;
import com.anafulus.project.R;
import com.kofigyan.stateprogressbar.StateProgressBar;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class AgentCodeReguler extends AppCompatActivity {
    AgentApi mApiInterface;
    SharedPreferences preferences;
    private EditText edttxtAgentCode;

    private Toolbar toolbar;
    private Button btn_continue, btn_search;
    private EditText code;

    String[] descriptionData = {"Data Diri", "Alamat", "Persyaratan","Kode Agen"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.agent_code);
        mApiInterface = RetrofitClient.getClient().create(AgentApi.class);
        preferences = getSharedPreferences("borrower_profil",MODE_PRIVATE);

        //Toolbar
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Masukkan Kode Agen");
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(AgentCodeReguler.this, RegulerRequirement.class);
                i.putExtra("loan_category", "Perorangan");
                i.putExtra("loan_type", "Reguler");
                i.putExtra("id_borrower", getIntent().getExtras().getString("id_borrower"));
                i.putExtra("loan_principal", getIntent().getExtras().getString("loan_principal"));
                i.putExtra("tenor", getIntent().getExtras().getString("tenor"));
                i.putExtra("loan_purpose", getIntent().getExtras().getString("loan_purpose"));
                i.putExtra("loan_status", "Belum disetujui");
                i.putExtra("installment_nominal", getIntent().getExtras().getString("installment_nominal"));
                i.putExtra("installment_type",getIntent().getExtras().getString("installment_type"));
                startActivity(i);
                finish();
            }
        });

        //Stepview Status
        StateProgressBar stateProgressBar = (StateProgressBar) findViewById(R.id.stepview);
        stateProgressBar.setStateDescriptionData(descriptionData);

        //Edittext Agent Code
        code = findViewById(R.id.codeAgent);

        //Button Continue
        btn_continue = findViewById(R.id.btn_continue);
        btn_continue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edttxtAgentCode = findViewById(R.id.codeAgent);
                String strCodeAgent = edttxtAgentCode.getText().toString();
                getAgent(strCodeAgent);
            }
        });

        //Button Search
        btn_search = findViewById(R.id.btn_search);
        btn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(AgentCodeReguler.this, AgentFoundReguler.class);
                i.putExtra("loan_category", "Perorangan");
                i.putExtra("loan_type", "Murabahah");
                i.putExtra("id_borrower", getIntent().getExtras().getString("id_borrower"));
                i.putExtra("loan_principal", getIntent().getExtras().getString("loan_principal"));
                i.putExtra("tenor", getIntent().getExtras().getString("tenor"));
                i.putExtra("loan_purpose", getIntent().getExtras().getString("loan_purpose"));
                i.putExtra("loan_status", "Belum disetujui");
                i.putExtra("installment_nominal", getIntent().getExtras().getString("installment_nominal"));
                i.putExtra("installment_type",getIntent().getExtras().getString("installment_type"));
                startActivity(i);
                finish();
            }
        });

    }

    //Button Back Smartphone
    public void onBackPressed() {
        Intent i = new Intent(AgentCodeReguler.this, RegulerRequirement.class);
        i.putExtra("loan_category", "Perorangan");
        i.putExtra("loan_type", "Reguler");
        i.putExtra("id_borrower", getIntent().getExtras().getString("id_borrower"));
        i.putExtra("loan_principal", getIntent().getExtras().getString("loan_principal"));
        i.putExtra("tenor", getIntent().getExtras().getString("tenor"));
        i.putExtra("loan_purpose", getIntent().getExtras().getString("loan_purpose"));
        i.putExtra("loan_status", "Belum disetujui");
        i.putExtra("installment_nominal", getIntent().getExtras().getString("installment_nominal"));
        i.putExtra("installment_type",getIntent().getExtras().getString("installment_type"));
        startActivity(i);
        finish();

    }
    private void getAgent(final String agentCode) {
        Call<Agents> getAgent = mApiInterface.dataAgentbyCodeAgent(agentCode);
        getAgent.enqueue(new Callback<Agents>() {
            @Override
            public void onResponse(Response<Agents> response, Retrofit retrofit) {
                if (!response.isSuccess()) {
                    edttxtAgentCode.setError("Agent dengan kode agen " + agentCode + " tidak ditemukan");
                } else {
                    Intent i = new Intent(AgentCodeReguler.this, LoanConfirmationReguler.class);
                    i.putExtra("loan_category", "Perorangan");
                    i.putExtra("loan_type", "Reguler");
                    i.putExtra("id_borrower", getIntent().getExtras().getString("id_borrower"));
                    i.putExtra("loan_principal", getIntent().getExtras().getString("loan_principal"));
                    i.putExtra("tenor", getIntent().getExtras().getString("tenor"));
                    i.putExtra("loan_purpose", getIntent().getExtras().getString("loan_purpose"));
                    i.putExtra("loan_status", "Belum disetujui");
                    i.putExtra("installment_nominal", getIntent().getExtras().getString("installment_nominal"));
                    i.putExtra("installment_type",getIntent().getExtras().getString("installment_type"));
                    int x =0 ;
                    i.putExtra("id_agent",response.body().getListDataAgent().get(x).getId().toString());
                    i.putExtra("name_agent",response.body().getListDataAgent().get(x).getName());
                    i.putExtra("code_agent",response.body().getListDataAgent().get(x).getAgent_code());
                    i.putExtra("phone_number_agent",response.body().getListDataAgent().get(x).getPhone_number());
                    startActivity(i);
                    finish();
                }
            }
            @Override
            public void onFailure(Throwable t) {
                Log.d("AKSES KE SERVER GAGAL",t.getMessage());
            }
        });
    }
}
