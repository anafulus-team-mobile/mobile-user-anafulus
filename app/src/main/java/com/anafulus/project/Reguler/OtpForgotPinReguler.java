package com.anafulus.project.Reguler;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.anafulus.project.R;

public class OtpForgotPinReguler extends AppCompatActivity {

    String string;
    private TextView number;
    private Toolbar toolbar;
    private Button btn_verify;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);

        //GET text from forgot pin page
        number = findViewById(R.id.number);
        string = getIntent().getExtras().getString("number");
        number.setText(string);

        //Button Verify
        btn_verify = findViewById(R.id.button_verify);
        btn_verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(OtpForgotPinReguler.this, ChangePinReguler.class);
                startActivity(i);
                finish();
            }
        });

    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(OtpForgotPinReguler.this, ForgotPinReguler.class);
        startActivity(i);
        finish();
    }
}
