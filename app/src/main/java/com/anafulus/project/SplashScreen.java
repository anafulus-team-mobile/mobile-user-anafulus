package com.anafulus.project;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.anafulus.project.Login.Login;

public class SplashScreen extends AppCompatActivity {
    SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                preferences = getSharedPreferences("borrower_profil",MODE_PRIVATE);

                if (preferences.getAll().isEmpty()){
                    final Intent mainIntent = new Intent(getApplicationContext(), Login.class);
                    startActivity(mainIntent);
                    finish();
                } else {
                    final Intent mainIntent = new Intent(getApplicationContext(), PageDashboard.class);
                    startActivity(mainIntent);
                    finish();
                }
            }
        }, 3000);
    }
}
