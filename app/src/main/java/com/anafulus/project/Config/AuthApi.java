package com.anafulus.project.Config;

import com.anafulus.project.Models.Auths;
import com.anafulus.project.Models.Borrowers;

import retrofit.Call;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;
import retrofit.http.PUT;

public interface AuthApi {

    @FormUrlEncoded
    @POST("login")
    Call<Borrowers> userLogin(
            @Field("phone_number") String phone_number
    );

    @FormUrlEncoded
    @PUT("change-phoneNumber")
    Call<Auths> changePhoneNumber(
            @Field("new_phone_number") String new_phone_number,
            @Field("last_phone_number") String last_phone_number,
            @Field("borrower_local_id") String borrower_local_id,
            @Field("family_card_id") String family_card_id
    );
}
