package com.anafulus.project.Config;

import com.anafulus.project.Models.Auths;
import com.anafulus.project.Models.Borrowers;
import com.squareup.okhttp.RequestBody;

import java.util.Date;

import retrofit.Call;
import retrofit.http.DELETE;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Part;
import retrofit.http.Path;
import retrofit.http.Query;

public interface BorrowerApi {

//    @FormUrlEncoded
    @GET("view-profile/{id_borrower}")
    Call<Borrowers> dataBorrower(
            @Path("id_borrower") String idBorrower
    );

    @FormUrlEncoded
    @PUT("change-pin/{id_borrower}")
    Call<Borrowers> changePin(
            @Path("id_borrower") String idBorrower,
            @Field("last_pin") String stringOldPin,
            @Field("new_pin") String stringNewPin
    );

    @FormUrlEncoded
    @PUT("update-phoneNumber/{id_borrower}")
    Call<Borrowers> changePhoneNumber(
            @Path("id_borrower") String idBorrower,
            @Field("last_phone_number") String lastHP,
            @Field("new_phone_number") String newHP
    );

    @GET("check-pin/{id_borrower}")
    Call<Borrowers> checkPin(
            @Path("id_borrower") String idBorrower,
            @Query("confirm_pin") String pin
    );

    @FormUrlEncoded
    @PUT("update-personalData/{id_borrower}")
    Call<Borrowers> updateProfil(
            @Path("id_borrower") String idBorrower,
            @Field("name") String name,
            @Field("phone_number") String phoneNumber,
            @Field("email") String email,
            @Field("birth_date") String birthDate,
            @Field("gender") String gender,
            @Field("last_job") String lastJob,
            @Field("last_education") String education,
            @Field("borrower_local_id") Integer localId,
            @Field("family_card_id") Integer familyId,
            @Field("principal_taxpayer_id") Integer npwpId,
            @Field("income") Integer income
    );

    @FormUrlEncoded
    @POST("loan/create")
    Call<Borrowers> createLoan(
            @Field("id_borrower") Integer idBorrower,
            @Field("id_agent") Integer idAgent,
            @Field("loan_category") String loanCategory,
            @Field("loan_type") String loanType,
            @Field("loan_principal") Integer loanPrincipal,
            @Field("tenor") String tenor,
            @Field("installment_nominal") Integer installmentNominal,
            @Field("loan_status") String loanStatus,
            @Field("loan_purpose") String loanPurpose,
            @Field("installment_type") String installmentType
    );

    // belum selesai
    @GET("view-lastLoanStatus/{id}")
    Call<Borrowers> getLastLoan(
            @Path("id") String idBorrower
    );

    @GET("filterByStatus/{id}")
    Call<Borrowers> getLatestLoanByStatus(
            @Path("id") String idBorrower,
            @Query("loan_status") String loanStatus
    );

    @GET("filterAllByStatus/{id}")
    Call<Borrowers> getAllLoanByStatus(
            @Path("id") String idBorrower,
            @Query("loan_status") String loanStatus
    );

    @GET("loanByStatus/{id}")
    Call<Borrowers> getHistoryLoanByStatus(
            @Path("id") String idBorrower,
            @Query("id_loan") Integer idLoan,
            @Query("loan_status") String loanStatus
    );

    @GET("isRead-notification/{id}")
    Call<Borrowers> getNotification(
            @Path("id") String idBorrower,
            @Query("is_read") Integer isRead
    );

    @GET("notification-view-loan-status/{id}")
    Call<Borrowers> getLoanStatus(
            @Path("id") String idBorrower,
            @Query("id_loan") Integer idLoan
    );


    @FormUrlEncoded
    @PUT("update-readNotification/{id_borrower}")
    Call<Borrowers> updateNotification(
            @Path("id_borrower") String idBorrower,
            @Field("id_notification") Integer lastHP
    );


    @FormUrlEncoded
    @POST("register")
    Call<Borrowers> registerBorrower(
            @Field("name") String name,
            @Field("email") String email,
            @Field("phone_number") String phone_number,
            @Field("pin") String pin
    );

    @FormUrlEncoded
    @POST("create-bankAccount/{id_borrower}")
    Call<Borrowers> saveBankAccount(
            @Path("id_borrower") String strIdBorrower,
            @Field("bank_account_name") String bankAccountName,
            @Field("bank_account_number") Integer bankAccountNumber,
            @Field("bank_branch") String bankBranch,
            @Field("bank_name") String bankName
    );

    @GET("view-bankAccount/{id}")
    Call<Borrowers> getBankAccount(
            @Path("id") String idBorrower
    );

    @GET("view-detail-bankAccount/{id}")
    Call<Borrowers> getDetailBankAccount(
            @Path("id") String idBankAccount
    );


    @DELETE("delete-bankAccount/{id}")
    Call<Borrowers> deleteBankAccount(
            @Path("id") String id
    );

    @Multipart
    @POST("update-requirement/{id}")
    Call<Borrowers> updateRequirement(
            @Path("id") String id,
            @Part("_method") RequestBody method,
//            @Part("id_selfie_image\"; filename=\"myfile.jpg\" ") String file,
            @Part("family_relationship") RequestBody familyRelationship,
            @Part("family_name") RequestBody familyName,
            @Part("family_phone_number") RequestBody familyPhoneNumber
    );

//    @Multipart
//    @POST("/imagefolder/index.php")
//    Call<UploadObject> uploadFile(@Part MultipartBody.Part file, @Part("name") RequestBody name);

    // Address
    @GET("list-provinces")
    Call<Borrowers> getListProvinces();

    @GET("list-regencies/{idProvince}")
    Call<Borrowers> getListRegencies(
            @Path("idProvince") Integer idProvince
    );

    @GET("list-subdistricts/{idRegence}")
    Call<Borrowers> getListSubdistrics(
            @Path("idRegence") Integer idRegence
    );
    @GET("list-village/{idSubdistrict}")
    Call<Borrowers> getListVillages(
            @Path("idSubdistrict") Integer idSubdistrict
    );

    @FormUrlEncoded
    @PUT("update-address/{id_borrower}")
    Call<Borrowers> updateAddress(
            @Path("id_borrower") String idBorrower,
            @Field("id_village") Integer idVillage,
            @Field("id_card_address") String address,
            @Field("domicile_address") String domicileAddress



    );



}
