package com.anafulus.project.Config;

import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

public class RetrofitClient {
    private static final String BASE_URL = "http://192.168.42.139:8000/api/mobile-borrower/";
    private static Retrofit retrofit = null;
    public static Retrofit getClient() {
        if (retrofit==null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

}
