package com.anafulus.project.Config;

import com.anafulus.project.Models.Agents;
import com.anafulus.project.Models.Borrowers;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Path;

public interface AgentApi {

    @GET("view-by-subdistrict/{idSubdistrict}")
    Call<Agents> dataAgent(
            @Path("idSubdistrict") String idSubdistrict
    );

    @GET("view-by-agent-code/{agentCode}")
    Call<Agents> dataAgentbyCodeAgent(
            @Path("agentCode") String agentCode
    );
}
