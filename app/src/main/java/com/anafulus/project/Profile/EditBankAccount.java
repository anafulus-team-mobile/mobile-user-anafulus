package com.anafulus.project.Profile;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.anafulus.project.Config.BorrowerApi;
import com.anafulus.project.Config.RetrofitClient;
import com.anafulus.project.Models.Borrowers;
import com.anafulus.project.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class EditBankAccount extends AppCompatActivity {
    SharedPreferences preferences;
    BorrowerApi mApiInterface;

    private Toolbar toolbar;
    private Spinner spn_bankname;
    private Button btn_change;
    private TextView nameAccount, bankAccountNumber, branch, warning;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.change_bank_account);
        preferences = getSharedPreferences("borrower_profil", MODE_PRIVATE);
        mApiInterface = RetrofitClient.getClient().create(BorrowerApi.class);

        //INITIAL
        toolbar = findViewById(R.id.toolbar);

        //TOOLBAR
        //Toolbar
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Nomor Rekening");
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(EditBankAccount.this, PageData.class);
                i.putExtra("tab", "3");
                startActivity(i);
                finish();
            }
        });

        getDetailBankAccount(preferences.getString("idBorrower", null), getIntent().getExtras().getString("idBankAccount"));

        nameAccount = findViewById(R.id.nameAccount);
        bankAccountNumber = findViewById(R.id.bankAccountNumber);
        branch = findViewById(R.id.branch);
        warning = findViewById(R.id.warning);
        //Spinner Bank Name
        spn_bankname = findViewById(R.id.bankname);

        // Initializing a String Array
        String[] bank = new String[]{
                "Nama Bank...",
                "BCA",
                "Mandiri",
                "CIMB Niaga",
                "BRI",
                "BNI"
        };

        final List<String> bankList = new ArrayList<>(Arrays.asList(bank));

        // Initializing an ArrayAdapter
        final ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                this, R.layout.spinner_item, bankList) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                } else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if (position == 0) {
                    // Set the hint text color gray
                    tv.setTextColor(Color.GRAY);
                } else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };

        //Spinner City
        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
        spn_bankname.setAdapter(spinnerArrayAdapter);
        spn_bankname.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItemText = (String) parent.getItemAtPosition(position);
                warning.setVisibility(View.GONE);

//                // If user change the default selection
//                // First item is disable and it is used for hint
//                if (position == 1) {
//                    Toast.makeText(ChangeBankAccount.this, "BCA", Toast.LENGTH_SHORT).show();
//                } else if (position == 2) {
//                    Toast.makeText(ChangeBankAccount.this, "Mandiri", Toast.LENGTH_SHORT).show();
//                } else if (position == 3) {
//                    Toast.makeText(ChangeBankAccount.this, "CIMB Niaga", Toast.LENGTH_SHORT).show();
//                } else if (position == 4) {
//                    Toast.makeText(ChangeBankAccount.this, "BRI", Toast.LENGTH_SHORT).show();
//                } else if (position == 5) {
//                    Toast.makeText(ChangeBankAccount.this, "BNI", Toast.LENGTH_SHORT).show();
//                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        //Button Save/Change
        btn_change = findViewById(R.id.btn_change);
        btn_change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ((nameAccount.getText().toString().equals("")) && (bankAccountNumber.getText().toString().equals("")) && (spn_bankname.getSelectedItem().toString().equals("Nama Bank..."))) {
                    nameAccount.setError("Isi nama akun bank anda");
                    bankAccountNumber.setError("Isi nomor rekening bank anda");
                    warning.setVisibility(View.VISIBLE);
                    warning.setText("Pilih jenis bank anda");
                } else if (nameAccount.getText().toString().equals("")) {
                    nameAccount.setError("Isi nama akun bank anda");
                } else if (bankAccountNumber.getText().toString().equals("")) {
                    bankAccountNumber.setError("Isi nomor rekening bank anda");
                } else if (spn_bankname.getSelectedItem().toString().equals("Nama Bank...")) {
                    warning.setVisibility(View.VISIBLE);
                    warning.setText("Pilih jenis bank anda");
                } else {
                    String strIdBorrower = preferences.getString("idBorrower", null);
                    checkBankAccountUsed(strIdBorrower);
                }

            }
        });
    }

    private void getDetailBankAccount(String idBorrower, String idBankAccount) {
        Log.d("idBanknya",idBankAccount);
        Call<Borrowers> borrower = mApiInterface.getDetailBankAccount(idBankAccount);
        borrower.enqueue(new Callback<Borrowers>() {
            @Override
            public void onResponse(Response<Borrowers> response, Retrofit retrofit) {
                if (response.isSuccess()) {
                    int i = 0;
                    nameAccount.setText(response.body().getListBankAccount().get(i).getBank_account_name());
                    bankAccountNumber.setText(response.body().getListBankAccount().get(i).getBank_account_number().toString());
                    branch.setText(response.body().getListBankAccount().get(i).getBank_branch());

                }
            }

            @Override
            public void onFailure(Throwable t) {
                Log.d("AKSES KE SERVER GAGAL", t.getMessage());
                Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void checkBankAccountUsed(String idBorrower) {
        Call<Borrowers> borrower = mApiInterface.getBankAccount(idBorrower);
        borrower.enqueue(new Callback<Borrowers>() {
            @Override
            public void onResponse(Response<Borrowers> response, Retrofit retrofit) {
                if (response.isSuccess()) {
                    List<Object> nameBank = new ArrayList<Object>();

                    for (int a = 0; a < response.body().getListBankAccount().size(); a++) {
                        nameBank.add(response.body().getListBankAccount().get(a).getBank_name());
                    }
                    if (nameBank.contains(spn_bankname.getSelectedItem().toString())) {
                        warning.setVisibility(View.VISIBLE);
                        warning.setText("Bank " + spn_bankname.getSelectedItem().toString() + " telah anda gunakan sebelumnya");
                    } else {
                        Intent i = new Intent(EditBankAccount.this, PinChangeBankAccount.class);
                        i.putExtra("bank_account_name", nameAccount.getText().toString());
                        i.putExtra("bank_account_number", bankAccountNumber.getText().toString());
                        i.putExtra("bank_branch", branch.getText().toString());
                        i.putExtra("bank_name", spn_bankname.getSelectedItem().toString());
                        startActivity(i);
                        finish();
                    }

                }
            }

            @Override
            public void onFailure(Throwable t) {
                Log.d("AKSES KE SERVER GAGAL", t.getMessage());
                Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
            }
        });
    }


    @Override
    public void onBackPressed() {
        Intent i = new Intent(EditBankAccount.this, PageData.class);
        i.putExtra("tab", "3");
        startActivity(i);
        finish();
    }
}