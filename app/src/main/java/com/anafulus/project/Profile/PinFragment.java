package com.anafulus.project.Profile;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.anafulus.project.Config.BorrowerApi;
import com.anafulus.project.Config.RetrofitClient;
import com.anafulus.project.Models.Auths;
import com.anafulus.project.Models.Borrowers;
import com.anafulus.project.R;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class PinFragment extends Fragment {

    private Button btn_change;
    private TextView oldPin, newPin;
    SharedPreferences preferences;
    BorrowerApi mApiInterface;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.pin_fragment, container, false);
        preferences = getActivity().getSharedPreferences("borrower_profil", Context.MODE_PRIVATE);
        mApiInterface = RetrofitClient.getClient().create(BorrowerApi.class);

        oldPin = view.findViewById(R.id.oldPin);
        newPin = view.findViewById(R.id.newPin);

        //INITIAL
        btn_change = view.findViewById(R.id.buttonChangePin);


        //BUTTON
        btn_change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String stringOldPin = oldPin.getText().toString();
                String stringNewPin = newPin.getText().toString();
                String idBorrower= preferences.getString("idBorrower",null);

                if (stringNewPin.equals(stringOldPin)) {
                    newPin.setError("Pin baru tidak bisa sama dengan pin lama");
                } else {
                    changePin(stringOldPin, stringNewPin, idBorrower);
                }
            }
        });
        return view;
    }
    private void changePin(String stringOldPin, String stringNewPin, String idBorrower){
        Call<Borrowers> changePinBorrower = mApiInterface.changePin(idBorrower, stringOldPin, stringNewPin);
        changePinBorrower.enqueue(new Callback<Borrowers>() {
            @Override
            public void onResponse(Response<Borrowers> response, Retrofit retrofit) {
                if (!response.isSuccess()){
                    oldPin.setError("Pin lama tidak sesuai");
                    Toast.makeText(getContext(), "Pin lama tidak sesuai", Toast.LENGTH_SHORT).show();
                } else {
                    Intent i = new Intent(getActivity(), PageData.class);
                    Toast.makeText(getContext(), "Pin Berhasil di Ubah", Toast.LENGTH_SHORT).show();
                    startActivity(i);
                    getActivity().finish();
                }
            }
            @Override
            public void onFailure(Throwable t) {
                Log.d("AKSES KE SERVER GAGAL",t.getMessage());
            }
        });
    }
}
