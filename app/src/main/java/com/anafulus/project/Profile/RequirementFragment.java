package com.anafulus.project.Profile;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.anafulus.project.Config.BorrowerApi;
import com.anafulus.project.Config.RetrofitClient;
import com.anafulus.project.Models.Borrowers;
import com.anafulus.project.R;

import java.text.NumberFormat;
import java.util.Locale;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class RequirementFragment extends Fragment {
    SharedPreferences preferences;
    BorrowerApi mApiInterface;

    private Button btn_change;
    private TextView selfieImage, localIdImage, familyCardImage, salarySlipImage, homeImage, familyName, familyPhoneNumber, familyRelationship;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.requirement_fragment, container, false);
        preferences = getActivity().getSharedPreferences("borrower_profil", Context.MODE_PRIVATE);
        mApiInterface = RetrofitClient.getClient().create(BorrowerApi.class);
        getRequirementData();

        selfieImage = view.findViewById(R.id.id_selfie_image);
        localIdImage= view.findViewById(R.id.local_id_image);
        familyCardImage = view.findViewById(R.id.family_card_image);
        salarySlipImage = view.findViewById(R.id.salary_slip_image);
        homeImage = view.findViewById(R.id.imageHome);
        familyName = view.findViewById(R.id.familyName);
        familyPhoneNumber = view.findViewById(R.id.familyPhoneNumber);
        familyRelationship = view.findViewById(R.id.familyRelationship);

        //INITIAl
        btn_change = view.findViewById(R.id.btn_change);

        //BUTTON
        btn_change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i  = new Intent(getActivity(), ChangeDataRequirement.class);
                startActivity(i);
                getActivity().finish();
            }
        });

        return view;
    }
    private void getRequirementData() {
        String idBorrower= preferences.getString("idBorrower",null);
        Call<Borrowers> postKontakCall = mApiInterface.dataBorrower(idBorrower);
        postKontakCall.enqueue(new Callback<Borrowers>() {
            @Override
            public void onResponse(Response<Borrowers> response, Retrofit retrofit) {
                int i =0 ;
                if(response.body().getListDataBorrower().get(i).getId_selfie_image()==null) {
                    selfieImage.setText("Belum Lengkap");
                } else{
                    selfieImage.setText("Sudah Lengkap");
                }

                if(response.body().getListDataBorrower().get(i).getLocal_id_image()==null) {
                    localIdImage.setText("Belum Lengkap");
                } else{
                    localIdImage.setText("Sudah Lengkap");
                }

                if(response.body().getListDataBorrower().get(i).getFamily_card_image()==null) {
                    familyCardImage.setText("Belum Lengkap");
                } else{
                    familyCardImage.setText("Sudah Lengkap");
                }

                if(response.body().getListDataBorrower().get(i).getSalary_slip_image()==null) {
                    salarySlipImage.setText("Belum Lengkap");
                } else{
                    salarySlipImage.setText("Sudah Lengkap");
                }

                if(response.body().getListDataBorrower().get(i).getHome_image()==null) {
                    homeImage.setText("Belum Lengkap");
                } else{
                    homeImage.setText("Sudah Lengkap");
                }

                familyName.setText(response.body().getListDataBorrower().get(i).getFamily_name());
                familyPhoneNumber.setText(response.body().getListDataBorrower().get(i).getFamily_phone_number());
                familyRelationship.setText(response.body().getListDataBorrower().get(i).getFamily_relationship());
            }
            @Override
            public void onFailure(Throwable t) {
                Log.d("AKSES KE SERVER GAGAL",t.getMessage());
            }
        });
    }
}