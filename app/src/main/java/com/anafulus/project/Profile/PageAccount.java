package com.anafulus.project.Profile;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.anafulus.project.Help.Help;
import com.anafulus.project.Login.Login;
import com.anafulus.project.PageDashboard;
import com.anafulus.project.History.PageHistory;
import com.anafulus.project.PageInfo;
import com.anafulus.project.Notify.PageNotify;
import com.anafulus.project.R;
import com.anafulus.project.Withdrawal.WithDraw;

import java.text.NumberFormat;
import java.util.Locale;

public class PageAccount extends AppCompatActivity {
    SharedPreferences preferences;

    private Button logout, data, address, requirement, bank_account, change_pin, withdraw;
    private Toolbar toolbar;
    private TextView txtNameBorrower, txtEmailBorrower, txtNoHpBorrower, txtFinanceBalance, txtRating, txtRatingDesc;
    String string;
    private boolean doubleTap = false;
    protected static final int CAMERA_REQUEST = 0;
    protected static final int GALLERY_PICTURE = 1;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.page_account);
        preferences = getSharedPreferences("borrower_profil",MODE_PRIVATE);

        txtNameBorrower = (TextView)findViewById(R.id.name);
        txtEmailBorrower = (TextView)findViewById(R.id.email);
        txtNoHpBorrower = (TextView)findViewById(R.id.nomorHandphone);
        txtFinanceBalance = (TextView)findViewById(R.id.financeBalance);
        txtRating = (TextView)findViewById(R.id.rating);
        txtRatingDesc = (TextView)findViewById(R.id.ratingDesc);
        withdraw = findViewById(R.id.withdraw);

        //BUTTON TAKE PICTURE PROFILE
        ImageView take = findViewById(R.id.photo);
        take.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MediaStore.INTENT_ACTION_STILL_IMAGE_CAMERA);
                startActivity(i);
            }
        });

        Locale localeID = new Locale("in", "ID");
        NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);

        txtNameBorrower.setText(preferences.getString("name",null));
        txtEmailBorrower.setText(preferences.getString("email",null));
        txtNoHpBorrower.setText(preferences.getString("phone_number",null));

        if (preferences.getString("rating",null) == null ){
            txtRating.setText(" Belum ada penilaian ");
            txtRatingDesc.setText(" Belum ada penilaian ");
        } else {
            txtRating.setText(preferences.getString("rating",null));
            txtRatingDesc.setText(preferences.getString("rating_desc",null));
        }
        if (preferences.getString("rating",null) == null) {
            txtRating.setText(" Belum ada penilaian ");
        } else if (preferences.getString("rating",null).equals("Baik")) {
            txtRating.setBackgroundResource(R.drawable.rating_good);
        } else if (preferences.getString("rating",null).equals("Cukup Baik")){
            txtRating.setBackgroundResource(R.drawable.rating_pretty_good);
        } else{
            txtRating.setBackgroundResource(R.drawable.rating_bad);
        }

        if (preferences.getString("finance_balance",null).equals("0")){
            txtFinanceBalance.setText("Rp. -");
            withdraw.setBackgroundColor(getResources().getColor(R.color.grey));
            withdraw.setEnabled(false);
        } else {
            double b = Double.parseDouble(preferences.getString("finance_balance",null));
            txtFinanceBalance.setText(formatRupiah.format((double)b));
        }
        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottomNavView_Bar);
        Menu menu = bottomNavigationView.getMenu();
        MenuItem menuItem = menu.getItem(4);
        menuItem.setChecked(true);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.nav_dashboard:
                        Intent a = new Intent(PageAccount.this, PageDashboard.class);
                        startActivity(a);
                        finish();
                        break;

                    case R.id.nav_notify:
                        Intent b = new Intent(PageAccount.this, PageNotify.class);
                        startActivity(b);
                        finish();
                        break;

                    case R.id.nav_info:
                        Intent c = new Intent(PageAccount.this, PageInfo.class);
                        startActivity(c);
                        finish();
                        break;

                    case R.id.nav_history:
                        Intent d = new Intent(PageAccount.this, PageHistory.class);
                        startActivity(d);
                        finish();
                        break;

                    case R.id.nav_account:
                        break;
                }
                return false;
            }
        });

        //Button Logout
        logout = findViewById(R.id.buttonLogout);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder (PageAccount.this, R.style.CustomAlertDialog);
                View view = getLayoutInflater().inflate(R.layout.popup_cancel, null);
                builder.setCancelable(false);
                builder.setView(view);
                Button buttonNo = view.findViewById(R.id.btn_no);
                Button buttonYes = view.findViewById(R.id.btn_yes);
                final AlertDialog dialog = builder.create();

                TextView textQuestion = view.findViewById(R.id.question);
                textQuestion.setText("Apakah anda yakin ingin keluar?");
                textQuestion.getLineSpacingExtra();
                buttonNo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                buttonYes.setText("Ya, Keluar");
                buttonYes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.clear();
                        editor.commit();
                        Intent i = new Intent(PageAccount.this, Login.class);
                        startActivity(i);
                        finish();
                    }
                });

                dialog.show();
            }
        });

        //Toolbar
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Akun Saya");

        //Button Data
        data = findViewById(R.id.data);
        data.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(PageAccount.this, PageData.class);
//                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                i.putExtra("tab", "0");
                startActivity(i);
                finish();
            }
        });

        //Button Address
        address = findViewById(R.id.address);
        address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(PageAccount.this, PageData.class);
                i.putExtra("tab", "1");
                startActivity(i);
                finish();
            }
        });

        //Button Data Requirement
        requirement = findViewById(R.id.requirement);
        requirement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(PageAccount.this, PageData.class);
                i.putExtra("tab", "2");
                startActivity(i);
                finish();
            }
        });

        //Button Bank Account
        bank_account = findViewById(R.id.bank_account);
        bank_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(PageAccount.this, PageData.class);
                i.putExtra("tab", "3");
                startActivity(i);
                finish();
            }
        });

        //Button Change PIN
        change_pin = findViewById(R.id.change_pin);
        change_pin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(PageAccount.this, PageData.class);
                i.putExtra("tab", "4");
                startActivity(i);
                finish();
            }
        });
        //Button Withdraw
        withdraw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(PageAccount.this, WithDraw.class);
                startActivity(i);
                finish();
            }
        });
    }

    //BUTTON HELP
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.help_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_help:
                Intent i = new Intent(PageAccount.this, Help.class);
                startActivity(i);
        }
        return super.onOptionsItemSelected(item);
    }

    //Double Tap For Exit App
    @Override
    public void onBackPressed() {
        if (doubleTap){
            super.onBackPressed();
        }else {
            Toast.makeText(this, "Ketuk Tombol Back 2 Kali Untuk Keluar Aplikasi", Toast.LENGTH_SHORT).show();
            doubleTap = true;
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    doubleTap = false;
                }
            },500);
        }
    }
}

