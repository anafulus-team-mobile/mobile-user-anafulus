package com.anafulus.project.Profile;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.anafulus.project.Login.Login;
import com.anafulus.project.OtpLoginPhone;
import com.anafulus.project.R;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

public class ChangePonsel extends AppCompatActivity {

    private Toolbar toolbar;
    private Button btn_change;
    private EditText lastPhoneNumber, newPhoneNumber;
    String string;
    String codeSent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.change_ponsel_after_login);

        //INITIAL
        toolbar = findViewById(R.id.toolbar);
        btn_change = findViewById(R.id.btn_change);
        newPhoneNumber = findViewById(R.id.newPhoneNumber);
        lastPhoneNumber = findViewById(R.id.lastPhoneNumber);

        //TOOLBAR
        //Toolbar
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Ubah Nomor Ponsel");
//        toolbar.setNavigationIcon(R.drawable.ic_back);
//        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent i = new Intent(ChangePonsel.this, ChangePersonal.class);
//                startActivity(i);
//                finish();
//            }
//        });

        //BUTTON
        btn_change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendVerificationCode();
            }
        });
    }

    private void sendVerificationCode(){
        String stringNumber = newPhoneNumber.getText().toString();
        if (validNumber(stringNumber)) {
            PhoneAuthProvider.getInstance().verifyPhoneNumber(
                    "+62" + stringNumber.substring(1),60,TimeUnit.SECONDS,this,mCallbacks);
        } else {
            newPhoneNumber.setError("Nomor Handphone Salah");
        }
    }

    //Regex Phone Number
    private boolean validNumber(String phone) {
        boolean check = false;
        if (!Pattern.matches("[a-zA-Z]+", phone)){
            if (phone.length()<10||phone.length()>13){
                check = false;
                newPhoneNumber.setError("Nomor Telepon Salah");
            }else {
                check = true;
            }
        }else {
            check = false;
        }
        return check;
    }

    PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
            Log.d("Berhasil, status", phoneAuthCredential.getSmsCode());
            Intent i = new Intent(ChangePonsel.this, OtpChangePhoneNumber.class);
            String newHP= newPhoneNumber.getText().toString();
            String lastHP= lastPhoneNumber.getText().toString();
            i.putExtra("number", newHP);
            i.putExtra("lastHp", lastHP);
            i.putExtra("phoneCode", phoneAuthCredential.getSmsCode());
            startActivity(i);
            finish();
        }

        @Override
        public void onVerificationFailed(FirebaseException e) {
            Log.d("Gagal, status", e.getMessage());
        }

        @Override
        public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            super.onCodeSent(s, forceResendingToken);
            codeSent = s;

        }
    };


    @Override
    public void onBackPressed() {
        Intent i = new Intent(ChangePonsel.this, ChangePersonal.class);
        startActivity(i);
        finish();
    }
}
