package com.anafulus.project.Profile;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.anafulus.project.Config.BorrowerApi;
import com.anafulus.project.Config.RetrofitClient;
import com.anafulus.project.Models.Borrowers;
import com.anafulus.project.R;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class AddressFragment extends Fragment {
    SharedPreferences preferences;
    BorrowerApi mApiInterface;

    private Button btn_change;
    private Toolbar toolbar;
    private TextView address, domicileAddress;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.address_fragment, container,false);
        preferences = getActivity().getSharedPreferences("borrower_profil", Context.MODE_PRIVATE);
        mApiInterface = RetrofitClient.getClient().create(BorrowerApi.class);

        getAddressData();

        address = view.findViewById(R.id.address);
        domicileAddress = view.findViewById(R.id.domicileAddress);

        //INITIAL
        btn_change = view.findViewById(R.id.btn_change);


        //Button Change
        btn_change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), ChangeAddress.class);
                startActivity(i);
                getActivity().finish();
            }
        });

        return view;
    }

    private void getAddressData() {
        String idBorrower= preferences.getString("idBorrower",null);
        Call<Borrowers> getAddress = mApiInterface.dataBorrower(idBorrower);
        getAddress.enqueue(new Callback<Borrowers>() {
            @Override
            public void onResponse(Response<Borrowers> response, Retrofit retrofit) {
                int i =0 ;
                String strAddress = "";
                if (response.body().getListDataBorrower().get(i).getVillage_name()==null){
                    strAddress ="";
                } else {
                    strAddress = response.body().getListDataBorrower().get(i).getId_card_address() + ", " + response.body().getListDataBorrower().get(i).getVillage_name() + ", " +response.body().getListDataBorrower().get(i).getSub_district_name() + ", " + response.body().getListDataBorrower().get(i).getRegency_name() + ", " + response.body().getListDataBorrower().get(i).getProvince_name();
                }

                String strAdressDomicile = response.body().getListDataBorrower().get(i).getDomicile_address();

                address.setText(strAddress);
                domicileAddress.setText(strAdressDomicile);
            }
            @Override
            public void onFailure(Throwable t) {
                Log.d("AKSES KE SERVER GAGAL",t.getMessage());
            }
        });
    }
}
