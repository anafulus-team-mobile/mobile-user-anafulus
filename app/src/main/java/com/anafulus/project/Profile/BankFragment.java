package com.anafulus.project.Profile;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.anafulus.project.Config.BorrowerApi;
import com.anafulus.project.Config.RetrofitClient;
import com.anafulus.project.Models.BankAccount;
import com.anafulus.project.Models.Borrowers;
import com.anafulus.project.R;
import com.anafulus.project.Profile.BankAdapter;
import com.anafulus.project.Withdrawal.BankItem;

import java.util.ArrayList;
import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class BankFragment extends Fragment {

    ListView listView;
    List<BankItem> bankItems;
    BorrowerApi mApiInterface;
    SharedPreferences preferences;
    private Button btn_change;
    private boolean isMaxBank;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.bank_fragment, container, false);
        mApiInterface = RetrofitClient.getClient().create(BorrowerApi.class);
        preferences = getActivity().getSharedPreferences("borrower_profil", Context.MODE_PRIVATE);
        isMaxBank = false;
        getBankAccount(view);

        //BUTTON ADD BANK
        btn_change = view.findViewById(R.id.btn_change);
        btn_change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isMaxBank == true){
                    Toast.makeText(getActivity(), "Total rekening Bank maksimal 3", Toast.LENGTH_LONG).show();
//                    btn_change.setEnabled(false);
                } else {
                    Intent i = new Intent(getActivity(), ChangeBankAccount.class);
                    startActivity(i);
                    getActivity().finish();
                }
            }
        });

        return view;
    }

    private void getBankAccount(final View view) {
        final View views = view;
        String idBorrower = preferences.getString("idBorrower",null);
        Call<Borrowers> borrower = mApiInterface.getBankAccount(idBorrower);
        borrower.enqueue(new Callback<Borrowers>() {
            @Override
            public void onResponse(Response<Borrowers> response, Retrofit retrofit) {
                if (response.isSuccess()) {
                    RelativeLayout imageLayout = view.findViewById(R.id.layout_image);
                    TextView warning = view.findViewById(R.id.warning);
                    if(response.body().getListBankAccount()==null){
                        imageLayout.setVisibility(View.VISIBLE);
                        warning.setVisibility(View.VISIBLE);
                    } else {
                        imageLayout.setVisibility(View.GONE);
                        warning.setVisibility(View.GONE);
                        ArrayList<BankAccount> modelAgentArrayList = new ArrayList<>();
                        for (int a = 0; a < response.body().getListBankAccount().size(); a++) {
                            BankAccount bankAccountView = new BankAccount();
                            bankAccountView.setId(response.body().getListBankAccount().get(a).getId());
                            bankAccountView.setBank_account_name(response.body().getListBankAccount().get(a).getBank_account_name());
                            bankAccountView.setBank_account_number(response.body().getListBankAccount().get(a).getBank_account_number());
                            bankAccountView.setBank_name(response.body().getListBankAccount().get(a).getBank_name());
                            modelAgentArrayList.add(bankAccountView);
                        }
                        makeToList(modelAgentArrayList, views);
                    }
                    if(response.body().getListBankAccount().size() == 3){
                            isMaxBank = true;
                    }


                }
            }

            @Override
            public void onFailure(Throwable t) {
                Log.d("AKSES KE SERVER GAGAL", t.getMessage());
//                Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
            }
        });
    }
    private void makeToList(ArrayList<BankAccount> dataBank, View view) {
        ListView listViewNotify;
        BankAdapter bankAdapter;
        listViewNotify = view.findViewById(R.id.listviewBank);
        bankAdapter = new BankAdapter(getActivity(), dataBank);
        listViewNotify.setAdapter(bankAdapter);
    }

}