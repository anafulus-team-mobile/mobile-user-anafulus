package com.anafulus.project.Profile;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.anafulus.project.Config.BorrowerApi;
import com.anafulus.project.Config.RetrofitClient;
import com.anafulus.project.Models.Borrowers;
import com.anafulus.project.Murabahah.ChangeMurabahahAddress;
import com.anafulus.project.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class ChangeAddress extends AppCompatActivity {

    private Toolbar toolbar;
    private EditText address, domicileAddress;
    private Spinner spn_province, spn_city, spn_district, spn_kelurahan;
    private Button btn_save;
    List<String> regencies = new ArrayList<>();
    List<String> subdistrics = new ArrayList<>();
    List<String> villages = new ArrayList<>();
    private String idVillage;
    BorrowerApi mApiInterface;
    private CheckBox checkboxDomicileAddress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.change_address);
        mApiInterface = RetrofitClient.getClient().create(BorrowerApi.class);

        getProvinces();

        //Toolbar & Button Back
        toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Alamat");
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ChangeAddress.this, PageData.class);
                i.putExtra("tab", "1");
                startActivity(i);
                finish();
            }
        });
        checkboxDomicileAddress = findViewById(R.id.checkboxDomicileAddress);
        address = findViewById(R.id.address);
        domicileAddress = findViewById(R.id.domicile);
        //Spinner Province
        spn_province = findViewById(R.id.province);

        checkboxDomicileAddress.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    domicileAddress.setText(address.getText().toString());
                    domicileAddress.setEnabled(false);
                    domicileAddress.setTextColor(getResources().getColor(R.color.grey));
                } else {
                    domicileAddress.setText("");
                    domicileAddress.setEnabled(true);
                    domicileAddress.setTextColor(getResources().getColor(R.color.black));
                }
            }
        });

        //Spinner City
        spn_city = findViewById(R.id.city);

        regencies.add("-- Pilih Kota --");

        final ArrayAdapter<String> spinnerArrayAdapter2 = new ArrayAdapter<String>(
                ChangeAddress.this, R.layout.spinner_item, regencies) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                } else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if (position == 0) {
                    // Set the hint text color gray
                    tv.setTextColor(Color.GRAY);
                } else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };

        spinnerArrayAdapter2.setDropDownViewResource(R.layout.spinner_item);
        spn_city.setAdapter(spinnerArrayAdapter2);


        //Spinner District
        spn_district = findViewById(R.id.district);
        subdistrics.add("-- Pilih Kecamatan --");
        // Initializing an ArrayAdapter
        final ArrayAdapter<String> spinnerArrayAdapter3 = new ArrayAdapter<String>(
                this, R.layout.spinner_item, subdistrics) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                } else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if (position == 0) {
                    // Set the hint text color gray
                    tv.setTextColor(Color.GRAY);
                } else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };
        spinnerArrayAdapter3.setDropDownViewResource(R.layout.spinner_item);
        spn_district.setAdapter(spinnerArrayAdapter3);


        //Spinner Kelurahan
        villages.add("-- Pilih Kelurahan --");
        spn_kelurahan = findViewById(R.id.kelurahan);

        // Initializing an ArrayAdapter
        final ArrayAdapter<String> spinnerArrayAdapter4 = new ArrayAdapter<String>(
                this, R.layout.spinner_item, villages) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                } else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if (position == 0) {
                    // Set the hint text color gray
                    tv.setTextColor(Color.GRAY);
                } else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };


        //Spinner City
        spinnerArrayAdapter4.setDropDownViewResource(R.layout.spinner_item);
        spn_kelurahan.setAdapter(spinnerArrayAdapter4);

        //Button Save
        btn_save = findViewById(R.id.btn_change);
        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ChangeAddress.this, PinChangeAddress.class);
                i.putExtra("id_village", idVillage);
                i.putExtra("address", address.getText().toString());
                i.putExtra("domicileAddress", domicileAddress.getText().toString());
                startActivity(i);
                finish();
            }
        });

    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(ChangeAddress.this, PageData.class);
        i.putExtra("tab", "1");
        startActivity(i);
        finish();
    }

    private void getProvinces() {
        Call<Borrowers> getDataLoan = mApiInterface.getListProvinces();
        getDataLoan.enqueue(new Callback<Borrowers>() {
            @Override
            public void onResponse(Response<Borrowers> response, Retrofit retrofit) {
                final List<String> provinces = new ArrayList<>();
                provinces.add("-- Pilih Provincy --");
                for (int a = 0; a < response.body().getListDataProvinces().size(); a++) {
                    provinces.add(response.body().getListDataProvinces().get(a).getProvinceName());
                }
                // Initializing an ArrayAdapter
                final ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                        ChangeAddress.this, R.layout.spinner_item, provinces) {
                    @Override
                    public boolean isEnabled(int position) {
                        if (position == 0) {
                            // Disable the first item from Spinner
                            // First item will be use for hint
                            return false;
                        } else {
                            return true;
                        }
                    }

                    @Override
                    public View getDropDownView(int position, View convertView,
                                                ViewGroup parent) {
                        View view = super.getDropDownView(position, convertView, parent);
                        TextView tv = (TextView) view;
                        if (position == 0) {
                            // Set the hint text color gray
                            tv.setTextColor(Color.GRAY);
                        } else {
                            tv.setTextColor(Color.BLACK);
                        }
                        return view;
                    }
                };

                spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
                spn_province.setAdapter(spinnerArrayAdapter);
                spn_province.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        String selectedItemText = (String) parent.getItemAtPosition(position);
                        if(!String.valueOf(position).equals("0")){
                            Toast.makeText(ChangeAddress.this, selectedItemText + " dipilih", Toast.LENGTH_SHORT).show();
                            getRegencies(position);
                        }
                    }
                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                });

            }
            @Override
            public void onFailure(Throwable t) {
                Log.d("AKSES KE SERVER GAGAL",t.getMessage());
                Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void getRegencies(Integer position){
        Call<Borrowers> getDataLoan = mApiInterface.getListRegencies(position);
        getDataLoan.enqueue(new Callback<Borrowers>() {
            @Override
            public void onResponse(Response<Borrowers> response, Retrofit retrofit) {
                for (int a = 0; a < response.body().getListDataRegencies().size(); a++) {
                    regencies.add(response.body().getListDataRegencies().get(a).getRegencyName());
                }
                spn_city.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        String selectedItemText = (String) parent.getItemAtPosition(position);
                        Toast.makeText(ChangeAddress.this, selectedItemText + " dipilih", Toast.LENGTH_SHORT).show();
                        getSubdistrict(position);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }
            @Override
            public void onFailure(Throwable t) {
                Log.d("AKSES KE SERVER GAGAL",t.getMessage());
                Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
            }
        });
    };

    private void getSubdistrict(Integer position){
        Call<Borrowers> getDataLoan = mApiInterface.getListSubdistrics(position);
        getDataLoan.enqueue(new Callback<Borrowers>() {
            @Override
            public void onResponse(Response<Borrowers> response, Retrofit retrofit) {
                for (int a = 0; a < response.body().getListDataSubdistrics().size(); a++) {
                    subdistrics.add(response.body().getListDataSubdistrics().get(a).getSubDistrictName());
                }
                spn_district.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        String selectedItemText = (String) parent.getItemAtPosition(position);
                        Toast.makeText(ChangeAddress.this, selectedItemText + " dipilih", Toast.LENGTH_SHORT).show();
                        getVillages(position);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }
            @Override
            public void onFailure(Throwable t) {
                Log.d("AKSES KE SERVER GAGAL",t.getMessage());
                Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
            }
        });
    };

    private void getVillages(Integer position){
        Call<Borrowers> getDataLoan = mApiInterface.getListVillages(position);
        getDataLoan.enqueue(new Callback<Borrowers>() {
            @Override
            public void onResponse(Response<Borrowers> response, Retrofit retrofit) {
                for (int a = 0; a < response.body().getListDataVillages().size(); a++) {
                    villages.add(response.body().getListDataVillages().get(a).getVillageName());
                }

                spn_kelurahan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        String selectedItemText = (String) parent.getItemAtPosition(position);
                        Toast.makeText(ChangeAddress.this, selectedItemText + " dipilih", Toast.LENGTH_SHORT).show();
                        idVillage = String.valueOf(position);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }
            @Override
            public void onFailure(Throwable t) {
                Log.d("AKSES KE SERVER GAGAL",t.getMessage());
                Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
            }
        });
    };

}
