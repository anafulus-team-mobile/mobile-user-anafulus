package com.anafulus.project.Profile;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.anafulus.project.R;
import com.anafulus.project.SectionsPageAdapter;

public class PageData extends AppCompatActivity {

    private Toolbar toolbar;
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.page_data);

        String tabToOpen = getIntent().getExtras().getString("tab");

        //Toolbar
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Akun");
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(PageData.this, PageAccount.class);
                startActivity(i);
                finish();
            }
        });
        //ViewPager
        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        setupViewPager(mViewPager);

        //Tab Layout
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
        mViewPager.setCurrentItem(Integer.parseInt(tabToOpen));

        tabLayout.getTabAt(0).setText("Data Diri");
        tabLayout.getTabAt(1).setText("Alamat");
        tabLayout.getTabAt(2).setText("Data Persyaratan");
        tabLayout.getTabAt(3).setText("Rekening Bank");
        tabLayout.getTabAt(4).setText("Ubah PIN");
    }

    private void setupViewPager(ViewPager viewPager) {
        SectionsPageAdapter adapter = new SectionsPageAdapter(getSupportFragmentManager());
        adapter.addFragment(new PersonalFragment());
        adapter.addFragment(new AddressFragment());
        adapter.addFragment(new RequirementFragment());
        adapter.addFragment(new BankFragment());
        adapter.addFragment(new PinFragment());
        viewPager.setAdapter(adapter);
    }

    public void onBackPressed() {
        Intent i = new Intent(PageData.this, PageAccount.class);
        startActivity(i);
        finish();
    }
}