package com.anafulus.project.Profile;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.anafulus.project.Config.BorrowerApi;
import com.anafulus.project.Config.RetrofitClient;
import com.anafulus.project.Models.Borrowers;
import com.anafulus.project.Murabahah.ChangeMurabahahRequirement;
import com.anafulus.project.R;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.RequestBody;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import okhttp3.MultipartBody;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

import static android.os.Environment.getExternalStoragePublicDirectory;

public class ChangeDataRequirement extends AppCompatActivity {

    SharedPreferences preferences;
    BorrowerApi mApiInterface;
    private EditText name, number, relation;
    private Button btn_change, btn_save;
    String selfieToFile, ktpToFile, houseToFile, sallaryToFile;
    ImageView imageSelfie, imageKtp, imageHouse, imageSallary;
    Toolbar toolbar;
//    private Uri filePath;
    private Uri selfieImagePath;
    public static final int CAMERA_REQUEST1 = 1;
    public static final int CAMERA_REQUEST2 = 2;
    public static final int CAMERA_REQUEST3 = 3;
    public static final int CAMERA_REQUEST4 = 4;
    Bitmap imageSelfieBitmap;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.change_data_requirement);

        preferences = getSharedPreferences("borrower_profil", MODE_PRIVATE);
        mApiInterface = RetrofitClient.getClient().create(BorrowerApi.class);

        //INITIAL EDITTEXT
        name = findViewById(R.id.name);
        number = findViewById(R.id.number);
        relation = findViewById(R.id.relation);

        //INITIAL BUTTON CHANGE
        btn_change = findViewById(R.id.btn_change);

        //INITIAL TOOLBAR
        toolbar = findViewById(R.id.toolbar);

        //TOOLBAR
        //Toolbar
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Data Persyaratan");
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ChangeDataRequirement.this, PageData.class);
                i.putExtra("tab", "2");
                startActivity(i);
                finish();
            }
        });
        getRequirementData();

        //Button Selfie
        final ImageButton buttonSelfie = findViewById(R.id.btn_selfie);
        buttonSelfie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(i, CAMERA_REQUEST1);
            }
        });
        imageSelfie = findViewById(R.id.selfie);

        //Button Ktp
        ImageButton buttonKtp = findViewById(R.id.btn_ktp);
        buttonKtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(i, CAMERA_REQUEST2);
            }
        });

        imageKtp = findViewById(R.id.ktp);

        //Button House
        ImageButton buttonHouse = findViewById(R.id.btn_house);
        buttonHouse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(i, CAMERA_REQUEST3);
            }
        });

        imageHouse = findViewById(R.id.house);

        //Button Sallary
        ImageButton buttonBusiness = findViewById(R.id.btn_sallary);
        buttonBusiness.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(i, CAMERA_REQUEST4);
            }
        });

        imageSallary = findViewById(R.id.sallary);
        //Button Save/Change
        btn_change = findViewById(R.id.btn_change);
        btn_change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // CALL THIS METHOD TO GET THE ACTUAL PATH
                File finalFile = new File(getRealPathFromURI(selfieImagePath));


// Create a request body with file and image media type
                RequestBody fileReqBody = RequestBody.create(MediaType.parse("Pictures/*"), finalFile);

                MultipartBody.Part part = MultipartBody.Part.createFormData("upload", finalFile.getName(), fileReqBody);

                //                String filePath = getRealPathFromURIPath(selfieImage, ChangeDataRequirement.this);
//                File file = new File(filePath);
                Log.d(ChangeDataRequirement.class.getSimpleName(), "Filename " + finalFile);

                String nameFamily = name.getText().toString();
                String phoneNumberFam = number.getText().toString();
                String familyRelationship = relation.getText().toString();
                Intent i = new Intent(ChangeDataRequirement.this, PinChangeDataRequirement.class);
//                String path = getPath(imageSallary.);
//                ImageView imageView=(ImageView)view.findViewById(R.id.imageView);
//                Bitmap image=((BitmapDrawable)imageSelfie.getDrawable()).getBitmap();
//                Log.d("datanya", image.toString());
                //                File file = new File(imageSelfie.getPa .getPath());
                i.putExtra("name", nameFamily);
                i.putExtra("number", phoneNumberFam);
                i.putExtra("relation", familyRelationship);
                startActivity(i);
                finish();
            }
        });
    }

    //SET IMAGE
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_REQUEST1) {
            if (resultCode == RESULT_OK) {
                Bitmap bitmap = (Bitmap) data.getExtras().get("data");
                selfieImagePath = getImageUri(getApplicationContext(), bitmap);
                imageSelfieBitmap = (Bitmap) data.getExtras().get("data");

                if (bitmap != null) {
                    imageSelfie.setImageBitmap(bitmap);
                } else {
                    Intent i = new Intent(this, ChangeDataRequirement.class);
                    startActivity(i);
                    finish();
                }
            }
        }
        if (requestCode == CAMERA_REQUEST2){
            if (resultCode == RESULT_OK) {
                Bitmap bitmap = (Bitmap) data.getExtras().get("data");
                if (bitmap != null) {
                    imageKtp.setImageBitmap(bitmap);
                } else {
                    Intent i = new Intent(this, ChangeDataRequirement.class);
                    startActivity(i);
                    finish();
                }
            }
        }
        if (requestCode == CAMERA_REQUEST3){
            if (resultCode == RESULT_OK){
                Bitmap bitmap = (Bitmap) data.getExtras().get("data");
                if (bitmap !=null){
                    imageHouse.setImageBitmap(bitmap);
                }else {
                    Intent i = new Intent(this, ChangeDataRequirement.class);
                    startActivity(i);
                    finish();
                }
            }
        }
        if (requestCode == CAMERA_REQUEST4){
            if (resultCode == RESULT_OK){
                Bitmap bitmap = (Bitmap) data.getExtras().get("data");
                if (bitmap != null){
                    imageSallary.setImageBitmap(bitmap);
                }else {
                    Intent i = new Intent(this, ChangeDataRequirement.class);
                    startActivity(i);
                    finish();
                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(ChangeDataRequirement.this, PageData.class);
        i.putExtra("tab", "2");
        startActivity(i);
        finish();
    }

    private void getRequirementData() {
        String idBorrower= preferences.getString("idBorrower",null);
        Call<Borrowers> postKontakCall = mApiInterface.dataBorrower(idBorrower);
        postKontakCall.enqueue(new Callback<Borrowers>() {
            @Override
            public void onResponse(Response<Borrowers> response, Retrofit retrofit) {
                int i =0 ;
                name.setText(response.body().getListDataBorrower().get(i).getFamily_name());
                number.setText(response.body().getListDataBorrower().get(i).getFamily_phone_number());
                relation.setText(response.body().getListDataBorrower().get(i).getFamily_relationship());
            }
            @Override
            public void onFailure(Throwable t) {
                Log.d("AKSES KE SERVER GAGAL",t.getMessage());
            }
        });
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }


    //Requesting permission

}
