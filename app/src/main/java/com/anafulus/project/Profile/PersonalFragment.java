package com.anafulus.project.Profile;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.anafulus.project.Config.BorrowerApi;
import com.anafulus.project.Config.RetrofitClient;
import com.anafulus.project.Models.Borrowers;
import com.anafulus.project.R;

import java.text.NumberFormat;
import java.util.Locale;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class PersonalFragment extends Fragment {
    SharedPreferences preferences;
    BorrowerApi mApiInterface;

    private Button btn_change;
    private TextView nameBorrower, emailBorrower, noHpBorrower, gender, birthDate, lastEducation, localId, familyId, npwpId, income, lastJob;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,  ViewGroup container,Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.personal_fragment, container, false);
        preferences = getActivity().getSharedPreferences("borrower_profil", Context.MODE_PRIVATE);
        mApiInterface = RetrofitClient.getClient().create(BorrowerApi.class);
        getProfilData();

        nameBorrower = view.findViewById(R.id.name);
        emailBorrower = view.findViewById(R.id.email);
        noHpBorrower = view.findViewById(R.id.phoneNumber);
        gender = view.findViewById(R.id.gender);
        birthDate = view.findViewById(R.id.birthDate);
        lastEducation = view.findViewById(R.id.lastEducation);
        localId = view.findViewById(R.id.localId);
        familyId = view.findViewById(R.id.familyId);
        npwpId = view.findViewById(R.id.npwp);
        income = view.findViewById(R.id.income);
        lastJob = view.findViewById(R.id.lastJob);

        btn_change = view.findViewById(R.id.btn_change);
        btn_change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), ChangePersonal.class);
                i.putExtra("name", nameBorrower.getText().toString());
                i.putExtra("email", emailBorrower.getText().toString());
                i.putExtra("phone_number", noHpBorrower.getText().toString());
                i.putExtra("gender", gender.getText().toString());
                i.putExtra("birthdate", birthDate.getText().toString());
                i.putExtra("last_education", lastEducation.getText().toString());
                i.putExtra("local_id", localId.getText().toString());
                i.putExtra("family_id", familyId.getText().toString());
                i.putExtra("npwp_id", npwpId.getText().toString());
                if(income.getText().toString().equals("")){
                    i.putExtra("income", "0");
                } else{
                    String xRupiah = income.getText().toString().substring(1);
                    String doubleRupiah = xRupiah.substring(1);
                    i.putExtra("income", doubleRupiah.replaceAll("[-+.^:,]", ""));
                }
                i.putExtra("lastjob", lastJob.getText().toString());
                startActivity(i);
                getActivity().finish();
            }
        });
        return view;
    }
    private void getProfilData() {
        String idBorrower= preferences.getString("idBorrower",null);
        Call<Borrowers> postKontakCall = mApiInterface.dataBorrower(idBorrower);
        postKontakCall.enqueue(new Callback<Borrowers>() {
            @Override
            public void onResponse(Response<Borrowers> response, Retrofit retrofit) {
                int i =0 ;
                Locale localeID = new Locale("in", "ID");
                NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);

                nameBorrower.setText(response.body().getListDataBorrower().get(i).getName());
                emailBorrower.setText(response.body().getListDataBorrower().get(i).getEmail());
                noHpBorrower.setText(response.body().getListDataBorrower().get(i).getPhone_number());
                gender.setText(response.body().getListDataBorrower().get(i).getGender());
                birthDate.setText(response.body().getListDataBorrower().get(i).getBirth_date());
                lastEducation.setText(response.body().getListDataBorrower().get(i).getLast_education());
                if(response.body().getListDataBorrower().get(i).getBorrower_local_id().equals(0)){
                    localId.setText("");
                } else{
                    localId.setText(response.body().getListDataBorrower().get(i).getBorrower_local_id().toString());
                }
                if(response.body().getListDataBorrower().get(i).getFamily_card_id().equals(0)){
                    familyId.setText("");
                } else{
                    familyId.setText(response.body().getListDataBorrower().get(i).getFamily_card_id().toString());
                }
                if(response.body().getListDataBorrower().get(i).getPrincipal_taxpayer_id().equals(0)){
                    npwpId.setText("");
                } else{
                    npwpId.setText(response.body().getListDataBorrower().get(i).getPrincipal_taxpayer_id().toString());
                }
                if(response.body().getListDataBorrower().get(i).getIncome().equals(0)){
                    income.setText("");
                } else{
                    double doubleIncome = Double.parseDouble(response.body().getListDataBorrower().get(i).getIncome().toString());
                    income.setText(formatRupiah.format((double)doubleIncome));
                }

                lastJob.setText(response.body().getListDataBorrower().get(i).getLast_job());
            }
            @Override
            public void onFailure(Throwable t) {
                Log.d("AKSES KE SERVER GAGAL",t.getMessage());
            }
        });
    }
}
