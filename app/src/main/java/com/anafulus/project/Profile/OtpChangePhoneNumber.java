package com.anafulus.project.Profile;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.alimuzaffar.lib.pin.PinEntryEditText;
import com.anafulus.project.Config.AuthApi;
import com.anafulus.project.Config.BorrowerApi;
import com.anafulus.project.Config.RetrofitClient;
import com.anafulus.project.Login.OtpChangeLogin;
import com.anafulus.project.Models.Auths;
import com.anafulus.project.Models.Borrowers;
import com.anafulus.project.R;
import com.anafulus.project.Register.DataRegister;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;


public class OtpChangePhoneNumber extends AppCompatActivity {
    SharedPreferences preferences;
    Toolbar toolbar;
    private PinEntryEditText edtxtPhoneCode;
    private TextView txtNewPhoneNumber;
    Button btnCodeVerification;
    BorrowerApi mApiInterface;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);
        preferences = getSharedPreferences("borrower_profil",MODE_PRIVATE);

        mApiInterface = RetrofitClient.getClient().create(BorrowerApi.class);

        //INITIAL
        txtNewPhoneNumber = findViewById(R.id.number);
        btnCodeVerification = findViewById(R.id.button_verify);
        edtxtPhoneCode = findViewById(R.id.text_otp);

        //GET text from login page
        String strNewHP = getIntent().getExtras().getString("number");
        String stringPhoneCode = getIntent().getExtras().getString("phoneCode");
        edtxtPhoneCode.setText(stringPhoneCode);
        txtNewPhoneNumber.setText(strNewHP);

        //Toolbar & Button Back
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("OTP");
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(OtpChangePhoneNumber.this, ChangePonsel.class);
                startActivity(i);
                finish();
            }
        });


        //BUTTON Verify
        btnCodeVerification.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String pinPhoneCode =edtxtPhoneCode.getText().toString();
                    String stringPhoneCode = getIntent().getExtras().getString("phoneCode");
                    //CHECK otp
                    if (stringPhoneCode.equals(pinPhoneCode)) {
                        String strNewHP = getIntent().getExtras().getString("number");
                        String strLastHP = getIntent().getExtras().getString("lastHp");
                        String strIdBorrower = preferences.getString("idBorrower",null);
                        changePhoneNumber(strIdBorrower, strLastHP, strNewHP);
                    } else {
                        Toast.makeText(getApplicationContext(),"Pin Salah", Toast.LENGTH_LONG).show();
                    }
                }
            });
    }

    private void changePhoneNumber(String id, String lastHP, String newHP) {
        Call<Borrowers> changeHP = mApiInterface.changePhoneNumber(id, lastHP, newHP);
        changeHP.enqueue(new Callback<Borrowers>() {
            @Override
            public void onResponse(Response<Borrowers> response, Retrofit retrofit) {
                if (!response.isSuccess()) {
                    Toast.makeText(OtpChangePhoneNumber.this, "Nomor handphone lama tidak sesuai",Toast.LENGTH_LONG).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            final Intent mainIntent = new Intent(OtpChangePhoneNumber.this, ChangePonsel.class);
                            startActivity(mainIntent);
                            finish();
                        }
                    }, 3000);

                } else {
                    Toast.makeText(OtpChangePhoneNumber.this, "Nomor Handphone berhasil diganti",Toast.LENGTH_LONG).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            final Intent mainIntent = new Intent(OtpChangePhoneNumber.this, ChangePersonal.class);
                            startActivity(mainIntent);
                            finish();
                        }
                    }, 3000);
                }
            }
            @Override
            public void onFailure(Throwable t) {
                Log.d("AKSES KE SERVER GAGAL",t.getMessage());
                Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
            }
        });
    }
        @Override
    public void onBackPressed() {
        Intent i = new Intent(OtpChangePhoneNumber.this, ChangePonsel.class);
        startActivity(i);
        finish();
    }
}
