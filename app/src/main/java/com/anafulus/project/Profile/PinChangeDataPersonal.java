package com.anafulus.project.Profile;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.alimuzaffar.lib.pin.PinEntryEditText;
import com.anafulus.project.Config.BorrowerApi;
import com.anafulus.project.Config.RetrofitClient;
import com.anafulus.project.Models.Borrowers;
import com.anafulus.project.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class PinChangeDataPersonal extends AppCompatActivity {
    SharedPreferences preferences;
    BorrowerApi mApiInterface;
    private PinEntryEditText edtxtPin;

    private Toolbar toolbar;
    private Button btn_change;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pin_confirm);
        preferences = getSharedPreferences("borrower_profil", MODE_PRIVATE);
        mApiInterface = RetrofitClient.getClient().create(BorrowerApi.class);
        edtxtPin = findViewById(R.id.pinTxt);

        //Toolbar & Button Back
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Konfirmasi PIN");
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(PinChangeDataPersonal.this, ChangePersonal.class);
                startActivity(i);
                finish();
            }
        });

        //Button Submit
        btn_change = findViewById(R.id.btn_change);
        btn_change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String pinConfirmation = edtxtPin.getText().toString();
                String strIdBorrower = preferences.getString("idBorrower", null);
                checkPin(strIdBorrower, pinConfirmation);
            }
        });
    }

    private void checkPin(String idBorrower, String pin) {
        Call<Borrowers> borrower = mApiInterface.checkPin(idBorrower, pin);
        borrower.enqueue(new Callback<Borrowers>() {
            @Override
            public void onResponse(Response<Borrowers> response, Retrofit retrofit) {
                if (!response.isSuccess()) {
                    Toast.makeText(PinChangeDataPersonal.this, "Pin Salah", Toast.LENGTH_LONG).show();
                } else {
                    String strIdBorrower = preferences.getString("idBorrower", null);
                    String name = getIntent().getExtras().getString("name");
                    String phoneNumber = getIntent().getExtras().getString("phoneNumber");
                    String email = getIntent().getExtras().getString("email");
                    String gender = getIntent().getExtras().getString("gender");
                    String birthDateInString = getIntent().getExtras().getString("birthDate");
                    String birtDate = changeDateFormat("dd/M/yyyy","yyyy-MM-dd",birthDateInString);
                    String lastJob = getIntent().getExtras().getString("lastJob");
                    String education = getIntent().getExtras().getString("education");
                    Integer localId = 0;
                    Integer familyId = 0;
                    Integer npwpId = 0;
                    Integer income = 0;
                    if(getIntent().getExtras().getString("localId").equals("")){
                        localId = 0;
                    } else {
                        localId = Integer.parseInt(getIntent().getExtras().getString("localId"));
                    }
                    if(getIntent().getExtras().getString("familyId").equals("")){
                        familyId = 0;
                    } else {
                        familyId = Integer.parseInt(getIntent().getExtras().getString("familyId"));
                    }
                    if(getIntent().getExtras().getString("npwpId").equals("")){
                        npwpId = 0;
                    } else {
                        npwpId = Integer.parseInt(getIntent().getExtras().getString("npwpId"));
                    }
                    if(getIntent().getExtras().getString("income").equals("")){
                        income = 0;
                    } else {
                        income = Integer.parseInt(getIntent().getExtras().getString("income"));
                    }
                    updateProfil(strIdBorrower, name, phoneNumber, email, birtDate, gender, lastJob, education, localId, familyId, npwpId, income);
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Log.d("AKSES KE SERVER GAGAL", t.getMessage());
                Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void updateProfil(String idBorrower, String name, String phoneNumber, String email, String birtDate, String gender, String lastJob, String education, Integer localId, Integer familyId, Integer npwpId, Integer income) {
        Call<Borrowers> borrower = mApiInterface.updateProfil(idBorrower, name, phoneNumber, email, birtDate, gender, lastJob, education, localId, familyId, npwpId, income);
        borrower.enqueue(new Callback<Borrowers>() {
            @Override
            public void onResponse(Response<Borrowers> response, Retrofit retrofit) {
                if (!response.isSuccess()) {
                    //errorr
                    Toast.makeText(PinChangeDataPersonal.this, "Gagal Memperbaharui Data", Toast.LENGTH_LONG).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            final Intent mainIntent = new Intent(PinChangeDataPersonal.this, ChangePersonal.class);
                            startActivity(mainIntent);
                            finish();
                        }
                    }, 1000);
                } else {
                    Toast.makeText(PinChangeDataPersonal.this, "Berhasil Memperbaharui Data", Toast.LENGTH_LONG).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            final Intent mainIntent = new Intent(PinChangeDataPersonal.this, PageData.class);
                            mainIntent.putExtra("tab", "0");
                            startActivity(mainIntent);
                            finish();
                        }
                    }, 1000);
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Log.d("AKSES KE SERVER GAGAL", t.getMessage());
                Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(PinChangeDataPersonal.this, PageData.class);
        startActivity(i);
        finish();
    }

    private String changeDateFormat(String currentFormat,String requiredFormat,String dateString){
        String result="";
        SimpleDateFormat formatterOld = new SimpleDateFormat(currentFormat, Locale.getDefault());
        SimpleDateFormat formatterNew = new SimpleDateFormat(requiredFormat, Locale.getDefault());
        Date date=null;
        try {
            date = formatterOld.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (date != null) {
            result = formatterNew.format(date);
        }
        return result;
    }

}