package com.anafulus.project.Profile;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.alimuzaffar.lib.pin.PinEntryEditText;
import com.anafulus.project.Config.BorrowerApi;
import com.anafulus.project.Config.RetrofitClient;
import com.anafulus.project.Models.Borrowers;
import com.anafulus.project.R;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.RequestBody;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class PinChangeDataRequirement extends AppCompatActivity {
    SharedPreferences preferences;
    BorrowerApi mApiInterface;
    private PinEntryEditText edtxtPin;

    private Toolbar toolbar;
    private Button btn_change;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pin_confirm);
        preferences = getSharedPreferences("borrower_profil", MODE_PRIVATE);
        mApiInterface = RetrofitClient.getClient().create(BorrowerApi.class);
        edtxtPin = findViewById(R.id.pinTxt);

        //Toolbar & Button Back
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Konfirmasi PIN");
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(PinChangeDataRequirement.this, ChangeDataRequirement.class);
                startActivity(i);
                finish();
            }
        });

        //Button Submit
        btn_change = findViewById(R.id.btn_change);
        btn_change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String pinConfirmation = edtxtPin.getText().toString();
                String strIdBorrower = preferences.getString("idBorrower", null);
                checkPin(strIdBorrower, pinConfirmation);

            }
        });
    }

    private void checkPin(String idBorrower, String pin) {
        Call<Borrowers> borrower = mApiInterface.checkPin(idBorrower, pin);
        borrower.enqueue(new Callback<Borrowers>() {
            @Override
            public void onResponse(Response<Borrowers> response, Retrofit retrofit) {
                if (!response.isSuccess()) {
                    Toast.makeText(PinChangeDataRequirement.this, "Pin Salah", Toast.LENGTH_LONG).show();
                } else {
                    String strIdBorrower = preferences.getString("idBorrower", null);
                    String nameFamily = getIntent().getExtras().getString("name");
                    String phoneNumberFam = getIntent().getExtras().getString("number");
                    String familyRelationship = getIntent().getExtras().getString("relation");
                    updateDataRequirement(strIdBorrower, nameFamily, phoneNumberFam, familyRelationship);
                }
            }
            @Override
            public void onFailure(Throwable t) {
                Log.d("AKSES KE SERVER GAGAL", t.getMessage());
                Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
            }
        });
    }

    protected void updateDataRequirement(String strIdBorrower, String nameFamily, String phoneNumberFam, String familyRelationship) {
//        RequestBody idBorrower = RequestBody.create(MediaType.parse("text/plain"), strIdBorrower);
        RequestBody method = RequestBody.create(MediaType.parse("text/plain"), "PUT");
        RequestBody familyRelation = RequestBody.create(MediaType.parse("text/plain"), familyRelationship);
        RequestBody name = RequestBody.create(MediaType.parse("text/plain"), nameFamily);
        RequestBody phoneNumber = RequestBody.create(MediaType.parse("text/plain"), phoneNumberFam);

        String idSelfieImage="";
//        Log.d("idnya", idBorrower.toString());
        Call<Borrowers> borrower = mApiInterface.updateRequirement(strIdBorrower, method, familyRelation, name, phoneNumber);
        borrower.enqueue(new Callback<Borrowers>() {
            @Override
            public void onResponse(Response<Borrowers> response, Retrofit retrofit) {
                if (response.isSuccess()) {
                    Toast.makeText(PinChangeDataRequirement.this, "Berhasil Memperbaharui Data", Toast.LENGTH_LONG).show();
                    Intent i = new Intent(PinChangeDataRequirement.this, PageData.class);
                    i.putExtra("tab", "2");
                    startActivity(i);
                    finish();
                } else {
                    Toast.makeText(PinChangeDataRequirement.this, "Gagal Memperbaharui Data", Toast.LENGTH_LONG).show();

                }

            }

            @Override
            public void onFailure(Throwable t) {
                Log.d("AKSES KE SERVER GAGAL", t.getMessage());
                Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
            }
        });
    }
}
