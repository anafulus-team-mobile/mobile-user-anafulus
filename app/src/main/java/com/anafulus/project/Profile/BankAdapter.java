package com.anafulus.project.Profile;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.anafulus.project.Config.BorrowerApi;
import com.anafulus.project.Config.RetrofitClient;
import com.anafulus.project.Models.BankAccount;
import com.anafulus.project.Models.Borrowers;
import com.anafulus.project.R;
import com.anafulus.project.Withdrawal.BankItem;

import java.util.ArrayList;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class BankAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<BankAccount> dataBank;
    private LayoutInflater mInflater;
    BorrowerApi mApiInterface;

    public BankAdapter(Context context, ArrayList<BankAccount> dataBank) {
        this.context = context;
        this.dataBank = dataBank;
        mInflater = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getViewTypeCount() {
        return dataBank.size();
    }
    @Override

    public int getItemViewType(int position) {

        return position;
    }

    @Override
    public int getCount() {
        return dataBank.size();
    }

    @Override
    public Object getItem(int position) {

        return dataBank.get(position);

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Integer finalPosition = position;
        ViewHolder holder;
        mApiInterface = RetrofitClient.getClient().create(BorrowerApi.class);

        final LayoutInflater inflater = (LayoutInflater)
                context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null){
            convertView = inflater.inflate(R.layout.listview_bank_in_profile, null);
            holder = new ViewHolder();
            holder.bankName = (TextView) convertView.findViewById(R.id.bank);
            holder.bankAccountName = (TextView) convertView.findViewById(R.id.name);
            holder.bankAccountNumber = (TextView) convertView.findViewById(R.id.number);
            holder.imageView = (ImageView) convertView.findViewById(R.id.menu);
            holder.imageBank = (ImageView) convertView.findViewById(R.id.imageBank);

            holder.imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {

                    switch (v.getId()) {
                        case R.id.menu:
                            PopupMenu popupMenu = new PopupMenu(context, v);
                            popupMenu.getMenuInflater().inflate(R.menu.popup_menu, popupMenu.getMenu());
                            popupMenu.show();
                            popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                                @Override
                                public boolean onMenuItemClick(MenuItem item) {

                                    switch (item.getItemId()) {
                                        case R.id.edit:
                                            final Intent i = new Intent(context, EditBankAccount.class);
                                            BankAccount bankItem = (BankAccount) getItem(finalPosition);
                                            i.putExtra("idBankAccount", bankItem.getId().toString());
                                            context.startActivity(i);
                                            break;
                                        case R.id.delete:
                                            AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                            View view = inflater.inflate(R.layout.popup_cancel, null);
                                            builder.setCancelable(false);
                                            builder.setView(view);
                                            Button buttonNo = view.findViewById(R.id.btn_no);
                                            TextView textQuestion = view.findViewById(R.id.question);
                                            textQuestion.setText("Apakah anda yakin ingin menghapus Bank ini?");
                                            final AlertDialog dialog = builder.create();
                                            buttonNo.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    dialog.dismiss();
                                                }
                                            });

                                            Button buttonYes = view.findViewById(R.id.btn_yes);
                                            buttonYes.setText("Ya, Hapus");

                                            buttonYes.setOnClickListener(new View.OnClickListener() {

                                                @Override
                                                public void onClick(View v) {
                                                    BankAccount bankItem = (BankAccount) getItem(finalPosition);
                                                    deleteBankAccount(bankItem.getId().toString(), dialog);
                                                    Toast.makeText(context, "Berhasil Memperbaharui Data", Toast.LENGTH_LONG).show();
                                                    Intent i = new Intent(context, PageData.class);
                                                    i.putExtra("tab", "3");
                                                    context.startActivity(i);
                                                }
                                            });

                                            builder.setView(view);
                                            dialog.show();
                                    }

                                    return false;
                                }
                            });
                    }
                }
            });

            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }

        BankAccount bankItem = (BankAccount) getItem(position);
        holder.bankAccountName.setText(bankItem.getBank_account_name());
        holder.bankAccountNumber.setText(bankItem.getBank_account_number().toString());
        holder.bankName.setText(bankItem.getBank_name());
        if(bankItem.getBank_name().equals("BCA")){
            holder.imageBank.setImageResource(R.drawable.bca);
        } else if (bankItem.getBank_name().equals("CIMB Niaga")){
            holder.imageBank.setImageResource(R.drawable.cimb);
        } else if(bankItem.getBank_name().equals("Mandiri")){
            holder.imageBank.setImageResource(R.drawable.mandiri);
        } else if(bankItem.getBank_name().equals("BRI")){
            holder.imageBank.setImageResource(R.drawable.bri);
        } else if(bankItem.getBank_name().equals("BNI")){
            holder.imageBank.setImageResource(R.drawable.bni);
        }
        return convertView;
    }

    private void deleteBankAccount(String id, Dialog dialog) {
        final Dialog popupDialog = dialog;
        Call<Borrowers> borrower = mApiInterface.deleteBankAccount(id);
        borrower.enqueue(new Callback<Borrowers>() {
            @Override
            public void onResponse(Response<Borrowers> response, Retrofit retrofit) {
                if (response.isSuccess()) {
//                    popupDialog.dismiss();

                    Toast.makeText(context, "di hapus", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Throwable t) {
                Log.d("AKSES KE SERVER GAGAL", t.getMessage());
//                Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
            }
        });
    }
    private class ViewHolder {
        protected TextView bankName, bankAccountNumber, bankAccountName;
        protected ImageView imageView, imageBank;
        protected Button btn_look;
    }
}
