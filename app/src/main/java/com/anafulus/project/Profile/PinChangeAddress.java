package com.anafulus.project.Profile;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.alimuzaffar.lib.pin.PinEntryEditText;
import com.anafulus.project.Config.BorrowerApi;
import com.anafulus.project.Config.RetrofitClient;
import com.anafulus.project.Models.Borrowers;
import com.anafulus.project.R;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.RequestBody;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class PinChangeAddress extends AppCompatActivity {
    SharedPreferences preferences;
    BorrowerApi mApiInterface;
    private PinEntryEditText edtxtPin;

    private Toolbar toolbar;
    private Button btn_change;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pin_confirm);
        preferences = getSharedPreferences("borrower_profil", MODE_PRIVATE);
        mApiInterface = RetrofitClient.getClient().create(BorrowerApi.class);
        edtxtPin = findViewById(R.id.pinTxt);

        //Toolbar & Button Back
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Konfirmasi PIN");
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(PinChangeAddress.this, ChangeAddress.class);
                startActivity(i);
                finish();
            }
        });

        //Button Submit
        btn_change = findViewById(R.id.btn_change);
        btn_change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String pinConfirmation = edtxtPin.getText().toString();
                String strIdBorrower = preferences.getString("idBorrower", null);
                checkPin(strIdBorrower, pinConfirmation);

            }
        });
    }

    private void checkPin(String idBorrower, String pin) {
        Call<Borrowers> borrower = mApiInterface.checkPin(idBorrower, pin);
        borrower.enqueue(new Callback<Borrowers>() {
            @Override
            public void onResponse(Response<Borrowers> response, Retrofit retrofit) {
                if (!response.isSuccess()) {
                    Toast.makeText(PinChangeAddress.this, "Pin Salah", Toast.LENGTH_LONG).show();
                } else {
//                    i.putExtra("id_village", idVillage);
//                    i.putExtra("address", address.getText().toString());
//                    i.putExtra("domicileAddress", domicileAddress.getText().toString());

                    String strIdBorrower = preferences.getString("idBorrower", null);
                    Integer idVillage = Integer.parseInt(getIntent().getExtras().getString("id_village"));
                    String address = getIntent().getExtras().getString("address");
                    String domicileAddress = getIntent().getExtras().getString("domicileAddress");
                    updateDataAddress(strIdBorrower, idVillage, address, domicileAddress);
                }
            }
            @Override
            public void onFailure(Throwable t) {
                Log.d("AKSES KE SERVER GAGAL", t.getMessage());
                Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
            }
        });
    }

    protected void updateDataAddress(String strIdBorrower, Integer idVillage, String address, String domicileAddress) {
        Call<Borrowers> borrower = mApiInterface.updateAddress(strIdBorrower, idVillage, address, domicileAddress);
        borrower.enqueue(new Callback<Borrowers>() {
            @Override
            public void onResponse(Response<Borrowers> response, Retrofit retrofit) {
                if (response.isSuccess()) {
                    Toast.makeText(PinChangeAddress.this, "Berhasil Memperbaharui Data", Toast.LENGTH_LONG).show();
                    Intent i = new Intent(PinChangeAddress.this, PageData.class);
                    i.putExtra("tab", "1");
                    startActivity(i);
                    finish();
                } else {
                    Toast.makeText(PinChangeAddress.this, "Gagal Memperbaharui Data", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Log.d("AKSES KE SERVER GAGAL", t.getMessage());
                Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
            }
        });
    }
}

