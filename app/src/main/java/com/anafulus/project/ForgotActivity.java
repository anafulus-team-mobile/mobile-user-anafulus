package com.anafulus.project;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.anafulus.project.Login.Login;


public class ForgotActivity extends AppCompatActivity {

    private EditText ponsel;
    private Button btn_reset;
    private TextView text1, text2;
    private Toolbar toolbar;

    Typeface tf;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgot_pin);

        //DECLARE
        //Font
        tf = Typeface.createFromAsset(getAssets(), "Verdana.ttf");

        text1 = findViewById(R.id.text1);
        text1.setTypeface(tf);

        text2 = findViewById(R.id.text2);
        text2.setTypeface(tf);

        btn_reset = findViewById(R.id.btn_reset);
        btn_reset.setTypeface(tf);

        ponsel = findViewById(R.id.number);
        ponsel.setTypeface(tf);

        //Toolbar & Button Back
        toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Lupa Kata Sandi");
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ForgotActivity.this, Login.class);
                i.putExtra("finish", true);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                finish();
            }
        });

    }
}
