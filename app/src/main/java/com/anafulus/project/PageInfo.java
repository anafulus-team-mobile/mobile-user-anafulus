package com.anafulus.project;

import android.content.Intent;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.anafulus.project.Help.Help;
import com.anafulus.project.History.PageHistory;
import com.anafulus.project.Information.CallUs;
import com.anafulus.project.Information.OjkRegulation;
import com.anafulus.project.Information.PolicyBorrow;
import com.anafulus.project.Information.PolicyCompany;
import com.anafulus.project.Information.PolicyPrivacy;
import com.anafulus.project.Information.ProfileCompany;
import com.anafulus.project.Information.TermsCondition;
import com.anafulus.project.Notify.PageNotify;
import com.anafulus.project.Profile.PageAccount;

public class PageInfo extends AppCompatActivity {

    private Toolbar toolbar;
    private Button company, call, policy_company, policy_borrow, policy_privacy, terms_conditions, ojk_regulation;
    private boolean doubleTap = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.page_info);

        //Toolbar
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Informasi");

        //Button Profile Company
        company = findViewById(R.id.company);
        company.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(PageInfo.this, ProfileCompany.class);
                startActivity(i);
                finish();
            }
        });

        //Button Call Us
        call = findViewById(R.id.call);
        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(PageInfo.this, CallUs.class);
                startActivity(i);
                finish();
            }
        });

        //Button Policy Company
        policy_company = findViewById(R.id.policy_company);
        policy_company.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i  = new Intent(PageInfo.this, PolicyCompany.class);
                startActivity(i);
                finish();
            }
        });

        //Button Policy Borrow
        policy_borrow = findViewById(R.id.policy_borrow);
        policy_borrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i  = new Intent(PageInfo.this, PolicyBorrow.class);
                startActivity(i);
                finish();
            }
        });

        //Button Policy Privacy
        policy_privacy = findViewById(R.id.policy_privacy);
        policy_privacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i  = new Intent(PageInfo.this, PolicyPrivacy.class);
                startActivity(i);
                finish();
            }
        });

        //Button Terms Conditions
        terms_conditions = findViewById(R.id.terms_condition);
        terms_conditions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i  = new Intent(PageInfo.this, TermsCondition.class);
                startActivity(i);
                finish();
            }
        });

        //Button Ojk Regulation
        ojk_regulation = findViewById(R.id.ojk_regulation);
        ojk_regulation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i  = new Intent(PageInfo.this, OjkRegulation.class);
                startActivity(i);
                finish();
            }
        });

        //Bottom Navigation
        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottomNavView_Bar);
        Menu menu = bottomNavigationView.getMenu();
        MenuItem menuItem = menu.getItem(2);
        menuItem.setChecked(true);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.nav_dashboard:
                        Intent a = new Intent(PageInfo.this, PageDashboard.class);
                        startActivity(a);
                        finish();
                        break;

                    case R.id.nav_notify:
                        Intent b = new Intent(PageInfo.this, PageNotify.class);
                        startActivity(b);
                        finish();
                        break;

                    case R.id.nav_info:
                        break;

                    case R.id.nav_history:
                        Intent c = new Intent(PageInfo.this, PageHistory.class);
                        startActivity(c);
                        finish();
                        break;

                    case R.id.nav_account:
                        Intent d = new Intent(PageInfo.this, PageAccount.class);
                        startActivity(d);
                        finish();
                        break;
                }
                return false;
            }
        });
    }

    //BUTTON HELP
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.help_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_help:
                Intent i = new Intent(PageInfo.this, Help.class);
                startActivity(i);
        }
        return super.onOptionsItemSelected(item);
    }

    //Double Tap For Exit App
    @Override
    public void onBackPressed() {
        if (doubleTap){
            super.onBackPressed();
        }else {
            Toast.makeText(this, "Ketuk Tombol Back 2 Kali Untuk Keluar Aplikasi", Toast.LENGTH_SHORT).show();
            doubleTap = true;
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    doubleTap = false;
                }
            },500);
        }
    }
    }
