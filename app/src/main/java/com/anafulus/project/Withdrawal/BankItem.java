package com.anafulus.project.Withdrawal;

public class BankItem {

    private String name, number, bank;

    public BankItem(String name, String number, String bank){
        this.name = name;
        this.number = number;
        this.bank = bank;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    @Override
    public String toString(){
        return name + number + bank;
    }
}
