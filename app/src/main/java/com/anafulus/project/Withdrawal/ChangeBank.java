package com.anafulus.project.Withdrawal;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.anafulus.project.Profile.ChangeBankAccount;
import com.anafulus.project.Profile.PageData;
import com.anafulus.project.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ChangeBank extends AppCompatActivity {

    private Spinner spn_bankname;
    private Toolbar toolbar;
    private Button btn_change;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.change_bank_account);

        //TOOLBAR
        //Toolbar
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Nomor Rekening");
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ChangeBank.this, WithDraw.class);
                startActivity(i);
                finish();
            }
        });

        //Button Save
        btn_change = findViewById(R.id.btn_change);
        btn_change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(ChangeBank.this, "Data Bank Berhasil Diubah", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(ChangeBank.this, WithDraw.class);
                startActivity(i);
                finish();
            }
        });

        //Spinner Bank Name
        spn_bankname = findViewById(R.id.bankname);

        // Initializing a String Array
        String[] bank = new String[]{
                "Nama Bank...",
                "BCA",
                "Mandiri",
                "CIMB Niaga",
                "BRI",
                "BNI"
        };
        final List<String> bankList = new ArrayList<>(Arrays.asList(bank));

        // Initializing an ArrayAdapter
        final ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                this, R.layout.spinner_item, bankList) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                } else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if (position == 0) {
                    // Set the hint text color gray
                    tv.setTextColor(Color.GRAY);
                } else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };

        //Spinner Bank
        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
        spn_bankname.setAdapter(spinnerArrayAdapter);
        spn_bankname.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItemText = (String) parent.getItemAtPosition(position);
                // If user change the default selection
                // First item is disable and it is used for hint
                if (position == 1) {
                    Toast.makeText(ChangeBank.this, "BCA", Toast.LENGTH_SHORT).show();
                } else if (position == 2) {
                    Toast.makeText(ChangeBank.this, "Mandiri", Toast.LENGTH_SHORT).show();
                } else if (position == 3) {
                    Toast.makeText(ChangeBank.this, "CIMB Niaga", Toast.LENGTH_SHORT).show();
                } else if (position == 4){
                    Toast.makeText(ChangeBank.this, "BRI", Toast.LENGTH_SHORT).show();
                } else if (position == 5){
                    Toast.makeText(ChangeBank.this, "BNI", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(ChangeBank.this, WithDraw.class);
        startActivity(i);
        finish();
    }
}
