package com.anafulus.project.Withdrawal;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.anafulus.project.Profile.PageAccount;
import com.anafulus.project.R;

public class ConfirmPinBank extends AppCompatActivity {

    private Button btn_change;
    private TextView text1;
    private Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pin_confirm);

        //Toolbar & Button Back
        toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Konfirmasi Pin Anda");
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ConfirmPinBank.this, ConfirmWithdraw.class);
                startActivity(i);
                finish();
            }
        });

        //Button Submit
        btn_change = findViewById(R.id.btn_change);
        btn_change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(ConfirmPinBank.this, "Berhasil Menarik Dana Mohon Tunggu Hingga Muncul Notifikasi", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(ConfirmPinBank.this, PageAccount.class);
                startActivity(i);
                finish();
            }
        });

        //TextView
        text1 = findViewById(R.id.text1);
        text1.setText("Untuk melakukan penarikan dana silahkan masukkan nomor pin anda");


    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(ConfirmPinBank.this,ConfirmWithdraw.class);
        startActivity(i);
        finish();
    }
}
