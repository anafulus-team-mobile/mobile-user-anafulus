package com.anafulus.project.Withdrawal;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.anafulus.project.Config.BorrowerApi;
import com.anafulus.project.Config.RetrofitClient;
import com.anafulus.project.Help.Help;
import com.anafulus.project.Models.Agent;
import com.anafulus.project.Models.BankAccount;
import com.anafulus.project.Models.Borrowers;
import com.anafulus.project.Murabahah.AgentListAdapterMurabahah;
import com.anafulus.project.Profile.ChangeBankAccount;
import com.anafulus.project.Profile.PageAccount;
import com.anafulus.project.Profile.PinChangeBankAccount;
import com.anafulus.project.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class WithDraw extends AppCompatActivity {
    SharedPreferences preferences;
    BorrowerApi mApiInterface;

    private Toolbar toolbar;
    private Button choose, withdraw;
    private RelativeLayout layout_bank2;
    private Spinner spn_bankname;

    public static final String[] name = new String[] {
            "Yayan Ruhiyan", "Jujuk Margono", "Riko Ceper" };

    public static final String[] number = new String[] { "08388619584",
            "08388619584", "08388619584"};

    public static final String[] bank = new String[] { "BCA",
            "Mandiri", "BCA"};

    ListView listView;
    List<BankItem> bankItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.withdraw);

        preferences = getSharedPreferences("borrower_profil", MODE_PRIVATE);
        mApiInterface = RetrofitClient.getClient().create(BorrowerApi.class);

        //Toolbar & Button Back
        toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Tarik Dana");
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(WithDraw.this, PageAccount.class);
                startActivity(i);
                finish();
            }
        });

        bankItems = new ArrayList<BankItem>();
        for (int i = 0; i < name.length; i++) {
            BankItem item = new BankItem( name[i], number[i],
                    bank[i]);
            bankItems.add(item);
        }

        String strIdBorrower = preferences.getString("idBorrower", null);
        getBankAccount(strIdBorrower);


        //Button Withdraw
        withdraw = findViewById(R.id.withdraw);
        withdraw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(WithDraw.this, ConfirmWithdraw.class);
                startActivity(i);
                finish();
            }
        });

        //Spinner Bank Name
        spn_bankname = findViewById(R.id.bankname);

        // Initializing a String Array
        String[] bank = new String[]{
                "Nama Bank...",
                "BCA",
                "Mandiri",
                "CIMB Niaga",
                "BRI",
                "BNI"
        };

        final List<String> bankList = new ArrayList<>(Arrays.asList(bank));

        // Initializing an ArrayAdapter
        final ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                this, R.layout.spinner_item, bankList) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                } else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if (position == 0) {
                    // Set the hint text color gray
                    tv.setTextColor(Color.GRAY);
                } else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };

        //Spinner Bank
        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
        spn_bankname.setAdapter(spinnerArrayAdapter);
        spn_bankname.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItemText = (String) parent.getItemAtPosition(position);
                // If user change the default selection
                // First item is disable and it is used for hint
//                if (position == 1) {
//                    Toast.makeText(WithDraw.this, "BCA", Toast.LENGTH_SHORT).show();
//                } else if (position == 2) {
//                    Toast.makeText(WithDraw.this, "Mandiri", Toast.LENGTH_SHORT).show();
//                } else if (position == 3) {
//                    Toast.makeText(WithDraw.this, "CIMB Niaga", Toast.LENGTH_SHORT).show();
//                } else if (position == 4){
//                    Toast.makeText(WithDraw.this, "BRI", Toast.LENGTH_SHORT).show();
//                } else if (position == 5){
//                    Toast.makeText(WithDraw.this, "BNI", Toast.LENGTH_SHORT).show();
//                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    //BUTTON HELP
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.help_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_help:
                Intent i = new Intent(WithDraw.this, Help.class);
                startActivity(i);
        }
        return super.onOptionsItemSelected(item);
    }

    private void getBankAccount(String idBorrower) {
        Call<Borrowers> borrower = mApiInterface.getBankAccount(idBorrower);
        borrower.enqueue(new Callback<Borrowers>() {
            @Override
            public void onResponse(Response<Borrowers> response, Retrofit retrofit) {
                if (response.isSuccess()) {
                    if(response.body().getListBankAccount()==null){
                    } else {
                        ArrayList<BankAccount> modelAgentArrayList = new ArrayList<>();
                        for (int a = 0; a < response.body().getListBankAccount().size(); a++) {
                            BankAccount bankAccountView = new BankAccount();
                            bankAccountView.setBank_account_name(response.body().getListBankAccount().get(a).getBank_account_name());
                            bankAccountView.setBank_account_number(response.body().getListBankAccount().get(a).getBank_account_number());
                            bankAccountView.setBank_name(response.body().getListBankAccount().get(a).getBank_name());
                            modelAgentArrayList.add(bankAccountView);
                        }
                        makeToList(modelAgentArrayList);
                    }
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Log.d("AKSES KE SERVER GAGAL", t.getMessage());
                Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
            }
        });
    }
    private void makeToList(ArrayList<BankAccount> dataBank) {
        ListView listViewNotify;
        BankAdapter bankAdapter;
        listViewNotify = findViewById(R.id.listviewBank);
        bankAdapter = new BankAdapter(this, dataBank);
        listViewNotify.setAdapter(bankAdapter);
    }

//    //LISTVIEW BORROWER AGENT
//    listView = (ListView) findViewById(R.id.listviewBank);
//    BankAdapter adapter = new BankAdapter(this, bankItems);
//        listView.setAdapter(adapter);

    @Override
    public void onBackPressed() {
        Intent i = new Intent(WithDraw.this, PageAccount.class);
        startActivity(i);
        finish();
    }
}
