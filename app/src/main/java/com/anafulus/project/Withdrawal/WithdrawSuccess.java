package com.anafulus.project.Withdrawal;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.anafulus.project.Profile.PageAccount;
import com.anafulus.project.R;

public class WithdrawSuccess extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.success);

        ImageView imageSuccess = findViewById(R.id.image_success);
        imageSuccess.setImageResource(R.drawable.icon_penarikan_dana);

        TextView textSuccess = findViewById(R.id.title_success);
        textSuccess.setText("penarikan dana berhasil");

        TextView textDescription = findViewById(R.id.description);
        textDescription.setText("Dana akan sampai ke rekening anda paling lama 7 hari kerja");

        Button buttonBackProfile = findViewById(R.id.btn_back);
        buttonBackProfile.setText("Kembali ke Profil");
        buttonBackProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(WithdrawSuccess.this, PageAccount.class);
                startActivity(i);
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        //DO NOTHING
    }
}
