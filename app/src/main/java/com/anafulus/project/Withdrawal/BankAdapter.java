package com.anafulus.project.Withdrawal;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.anafulus.project.Models.BankAccount;
import com.anafulus.project.Profile.BankFragment;
import com.anafulus.project.R;

import java.util.ArrayList;

public class BankAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<BankAccount> dataBank;
    private LayoutInflater mInflater;

    public BankAdapter(Context context, ArrayList<BankAccount> dataBank) {
        this.context = context;
        this.dataBank = dataBank;
        mInflater = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getViewTypeCount() {
        return dataBank.size();
    }
    @Override

    public int getItemViewType(int position) {

        return position;
    }

    @Override
    public int getCount() {
        return dataBank.size();
    }

    @Override
    public Object getItem(int position) {

        return dataBank.get(position);

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;

        LayoutInflater inflater = (LayoutInflater)
                context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null){
            convertView = inflater.inflate(R.layout.listview_bank_in_withdraw, null);
            holder = new ViewHolder();
            holder.bankName = (TextView) convertView.findViewById(R.id.bank);
            holder.bankAccountName = (TextView) convertView.findViewById(R.id.name);
            holder.bankAccountNumber = (TextView) convertView.findViewById(R.id.number);

            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }

        BankAccount bankItem = (BankAccount) getItem(position);

        holder.bankAccountName.setText(bankItem.getBank_account_name());
        holder.bankAccountNumber.setText(bankItem.getBank_account_number().toString());
        holder.bankName.setText(bankItem.getBank_name());

        return convertView;
    }

    private class ViewHolder {
        protected TextView bankName, bankAccountNumber, bankAccountName;
        protected Button btn_look;
    }
}
