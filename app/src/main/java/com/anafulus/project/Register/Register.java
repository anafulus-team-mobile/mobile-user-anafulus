package com.anafulus.project.Register;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.anafulus.project.Login.Login;
import com.anafulus.project.R;

import java.util.regex.Pattern;

public class Register extends AppCompatActivity {

    private Toolbar toolbar;
    private TextView text1, text2;
    private EditText number;
    private Button btn_regist;
    String string;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register);

//        //DECLARE
//        //Font
//        tf = Typeface.createFromAsset(getAssets(), "Verdana.ttf");

        text1 = findViewById(R.id.text1);
//        text1.setTypeface(tf);
        text2 = findViewById(R.id.text2);
//        text2.setTypeface(tf);
        number = findViewById(R.id.number);
//        number.setTypeface(tf);
        btn_regist = findViewById(R.id.btn_regist);
//        btn_regist.setTypeface(tf);

        //Toolbar & Button Back
        toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Daftar");
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Register.this, Login.class);
                i.putExtra("finish", true);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                finish();
            }
        });

        btn_regist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validNumber(number.getText().toString())) {
                    Intent i = new Intent(Register.this, OtpRegister.class);
                    string = number.getText().toString();
                    i.putExtra("number", string);
                    startActivity(i);
                    finish();
                } else {
                    number.setError("Nomor Telepon Salah");
                }
            }

        });
    }
    //Regex Phone Number
    private boolean validNumber(String phone) {
        boolean check = false;
        if (!Pattern.matches("[a-zA-Z]+", phone)){
            if (phone.length()<10||phone.length()>13){
                check = false;
                number.setError("Nomor Telepon Salah");
            }else {
                check = true;
            }
        }else {
            check = false;
        }
        return check;
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(Register.this, Login.class);
        startActivity(i);
        finish();
    }
}
