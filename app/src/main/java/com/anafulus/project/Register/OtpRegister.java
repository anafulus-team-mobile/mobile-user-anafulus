package com.anafulus.project.Register;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.anafulus.project.R;

public class OtpRegister extends AppCompatActivity {

    private Toolbar toolbar;
    private Button button_verify;
    private TextView text_failed, number;
    String string;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);

        //Toolbar & Button Back
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("OTP");
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(OtpRegister.this, Register.class);
                startActivity(i);
                finish();
            }
        });

        //Button
        button_verify = findViewById(R.id.button_verify);
        button_verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(OtpRegister.this, DataRegister.class);
                string = number.getText().toString();
                i.putExtra("number", string);
                startActivity(i);
                finish();
            }
        });

        //Text Failed
        text_failed = findViewById(R.id.text_failed);

        //GET text from login page
        number = findViewById(R.id.number);
        string = getIntent().getExtras().getString("number");
        number.setText(string);

    }
}
