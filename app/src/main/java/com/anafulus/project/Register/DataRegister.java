package com.anafulus.project.Register;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.alimuzaffar.lib.pin.PinEntryEditText;
import com.anafulus.project.Config.AuthApi;
import com.anafulus.project.Config.BorrowerApi;
import com.anafulus.project.Config.RetrofitClient;
import com.anafulus.project.Models.Auths;
import com.anafulus.project.Models.Borrowers;
import com.anafulus.project.OtpLoginPhone;
import com.anafulus.project.PageDashboard;
import com.anafulus.project.Profile.PageData;
import com.anafulus.project.R;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class DataRegister extends AppCompatActivity {
    BorrowerApi mApiInterface;
    AuthApi mLoginApiInterface;
    SharedPreferences preferences;

    private EditText txtName, txtEmail;
    private PinEntryEditText txtPin, txtConfirmationPin;

    private TextView number;
    private CheckBox check;
    private Button btn_regist;
    String string;
    Toolbar toolbar;

    @Override
    public void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.data_register);
        mApiInterface = RetrofitClient.getClient().create(BorrowerApi.class);
        mLoginApiInterface = RetrofitClient.getClient().create(AuthApi.class);
        preferences = getSharedPreferences("borrower_profil",MODE_PRIVATE);

        txtName = findViewById(R.id.name);
        txtEmail = findViewById(R.id.email);
        number = findViewById(R.id.number);
        txtPin = findViewById(R.id.pin);
        txtConfirmationPin = findViewById(R.id.confirmation_pin);
        check = findViewById(R.id.check);
        string = getIntent().getExtras().getString("number");
        number.setText(string);

        //Toolbar & Button Back
        toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Daftar");
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(DataRegister.this, Register.class);
                i.putExtra("finish", true);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                finish();
            }
        });

        //Button Register
        btn_regist = findViewById(R.id.btn_regist);
        btn_regist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = txtName.getText().toString();
                String email = txtEmail.getText().toString();
                String phoneNumber = getIntent().getExtras().getString("number");
                String pin = txtPin.getText().toString();
                String confirmationPin = txtConfirmationPin.getText().toString();
                if (pin.equals(confirmationPin) ) {
                    if(check.isChecked()){
                        register(name, email, phoneNumber, pin);
                    } else {
                        check.setError("Mohon checklist jika anda sudah membaca persyaratan dan ketentuan menjadi bagian dari kami");
                    }

                } else {
                    txtConfirmationPin.setError("Konfirmasi pin tidak sesuai dengan pin");
                }

                //                Intent i = new Intent(DataRegister.this, PageDashboard.class);
//                startActivity(i);
//                finish();
//                Toast.makeText(DataRegister.this, "Anda Berhasil Daftar", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void register(String name, String email, final String phoneNumber, String pin){
        Log.d("name", name);
        Log.d("email", email);
        Log.d("phoneNumber", phoneNumber);
        Log.d("pin", pin);
        final String phoneNumberBorrower = phoneNumber;
        Call<Borrowers> register = mApiInterface.registerBorrower(name, email, phoneNumber, pin);
        register.enqueue(new Callback<Borrowers>() {
            @Override
            public void onResponse(Response<Borrowers> response, Retrofit retrofit) {
                if (response.isSuccess()){
                    userLogin(phoneNumberBorrower);
                    Toast.makeText(DataRegister.this, "Anda Berhasil Daftar", Toast.LENGTH_SHORT).show();
                } else {
                }
            }
            @Override
            public void onFailure(Throwable t) {
                Log.d("AKSES KE SERVER GAGAL",t.getMessage());
            }
        });
    }

    private void userLogin(String phoneNumberBorrower){
        String phoneNumber= phoneNumberBorrower;
        Call<Borrowers> loginBorrower = mLoginApiInterface.userLogin(phoneNumber);
        loginBorrower.enqueue(new Callback<Borrowers>() {
            @Override
            public void onResponse(Response<Borrowers> response, Retrofit retrofit) {
                if (response.isSuccess()) {
                    int i = 0;
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString("idBorrower",response.body().getListDataBorrower().get(i).getId_borrower().toString());
                    editor.putBoolean("isLogin",true);
                    editor.commit();
                    Intent a = new Intent(DataRegister.this, PageDashboard.class);
                    startActivity(a);
                    finish();
                } else {
                    Toast.makeText(DataRegister.this, response.body().getMsg(),Toast.LENGTH_LONG).show();
                }
            }
            @Override
            public void onFailure(Throwable t) {
                Log.d("AKSES KE SERVER GAGAL",t.getMessage());
                Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
            }
        });
    }
}

