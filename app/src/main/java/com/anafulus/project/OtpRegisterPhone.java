package com.anafulus.project;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.alimuzaffar.lib.pin.PinEntryEditText;
import com.anafulus.project.Login.Login;
import com.anafulus.project.Register.DataRegister;

public class OtpRegisterPhone extends AppCompatActivity {

    Typeface tf;
    Toolbar toolbar;
    private PinEntryEditText pinEntry;
    private TextView txt_fail, number;
    Button button_verify;
    String string;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);

        //INITIAL
        txt_fail = findViewById(R.id.text_failed);
        button_verify = findViewById(R.id.button_verify);
        number = findViewById(R.id.number);

        //GET text from register page
        string = getIntent().getExtras().getString("number");
        number.setText(string);



        //Toolbar & Button Back
        toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Daftar");
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(OtpRegisterPhone.this, Login.class);
                i.putExtra("finish", true);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                finish();
            }
        });

        pinEntry = (PinEntryEditText) findViewById(R.id.text_otp);
        if (pinEntry != null) {
            pinEntry.setOnPinEnteredListener(new PinEntryEditText.OnPinEnteredListener() {
                @Override
                public void onPinEntered(CharSequence str) {
                    if (str.toString().equals("1234")) {
                        txt_fail.setText("");
                        button_verify.setEnabled(true);
                    } else {
                        txt_fail.setText("Kode yang Anda masukkan salah");
                    }
                }
            });

        button_verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(OtpRegisterPhone.this, DataRegister.class);
                string = number.getText().toString();
                i.putExtra("number", string);
                startActivity(i);
                finish();
            }
        });

        }
    }
}
