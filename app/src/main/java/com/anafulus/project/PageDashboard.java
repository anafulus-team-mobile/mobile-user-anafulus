package com.anafulus.project;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.anafulus.project.Config.AuthApi;
import com.anafulus.project.Config.BorrowerApi;
import com.anafulus.project.Config.RetrofitClient;
import com.anafulus.project.DetailInstallment.NotYet;
import com.anafulus.project.Help.Help;
import com.anafulus.project.History.PageHistory;
import com.anafulus.project.Models.Auths;
import com.anafulus.project.Models.Borrower;
import com.anafulus.project.Models.Borrowers;
import com.anafulus.project.Notify.PageNotify;
import com.anafulus.project.Payment.Payment;
import com.anafulus.project.Profile.PageAccount;
import com.anafulus.project.Profile.PageData;
import com.anafulus.project.Withdrawal.WithDraw;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class PageDashboard extends AppCompatActivity {
    SharedPreferences preferences, preferencesLoanActive;
    BorrowerApi mApiInterface;

    ViewFlipper viewFlipper;
    private static ViewPager mPager;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;

    private static final String TAG = "PageDashboard";
    private SectionsPageAdapter mSectionsPageAdapter;

    private ViewPager mViewPager;
    private Button tarikDana, complete, detail, payment;
    private Menu action;
    private Toolbar toolbar;
    TabLayout indicator;

    private ViewPager viewPager;

    private boolean doubleTap = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.page_dashboard);
        preferences = getSharedPreferences("borrower_profil",MODE_PRIVATE);
        preferencesLoanActive = getSharedPreferences("loan_active",MODE_PRIVATE);

        tarikDana = findViewById(R.id.withdraw);
        tarikDana.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(PageDashboard.this, WithDraw.class);
                startActivity(i);
                finish();
            }
        });
        mApiInterface = RetrofitClient.getClient().create(BorrowerApi.class);
        getDataBorrower();
        getLastLoan();

        //Toolbar
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        //TAB LAYOUT
        mSectionsPageAdapter = new SectionsPageAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager =  findViewById(R.id.container);
        setupViewPager(mViewPager);

        //BUTTON COMPLETE
        complete = findViewById(R.id.complete);
        complete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(PageDashboard.this, PageAccount.class);
                startActivity(i);
                finish();
            }
        });

        //BUTTON DETAIL
        detail = findViewById(R.id.detail);
        detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(PageDashboard.this, NotYet.class);
                startActivity(i);
                finish();
            }
        });

        //BUTTON PAYMENT
        payment = findViewById(R.id.payment);
        payment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(PageDashboard.this, Payment.class);
                startActivity(i);
                finish();
            }
        });

        //IMAGE SLIDER
        viewPager = findViewById(R.id.view_pager);
        ImageSliderAdapter imageSliderAdapter = new ImageSliderAdapter(this);
        viewPager.setAdapter(imageSliderAdapter);
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new SliderTimer(), 2000, 4000);

        indicator = findViewById(R.id.indicator);
        indicator.setupWithViewPager(viewPager, true);


        //BOTTOM NAVIGATION
        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottomNavView_Bar);
        Menu menu = bottomNavigationView.getMenu();
        MenuItem menuItem = menu.getItem(0);
        menuItem.setChecked(true);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.nav_dashboard:
                        break;

                    case R.id.nav_notify:
                        Intent a = new Intent(PageDashboard.this, PageNotify.class);
                        startActivity(a);
                        finish();
                        break;

                    case R.id.nav_info:
                        Intent b = new Intent(PageDashboard.this, PageInfo.class);
                        startActivity(b);
                        finish();
                        break;

                    case R.id.nav_history:
                        Intent c = new Intent(PageDashboard.this, PageHistory.class);
                        startActivity(c);
                        finish();
                        break;

                    case R.id.nav_account:
                        Intent d = new Intent(PageDashboard.this, PageAccount.class);
                        startActivity(d);
                        finish();
                        break;
                }
                return false;
            }
        });
    }

    private void getDataBorrower() {
        String idBorrower= preferences.getString("idBorrower",null);
        Call<Borrowers> postKontakCall = mApiInterface.dataBorrower(idBorrower);
        postKontakCall.enqueue(new Callback<Borrowers>() {
            @Override
            public void onResponse(Response<Borrowers> response, Retrofit retrofit) {
                int i =0 ;
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString("email",response.body().getListDataBorrower().get(i).getEmail());
                editor.putString("phone_number",response.body().getListDataBorrower().get(i).getPhone_number());
                editor.putString("rating",response.body().getListDataBorrower().get(i).getRating());
                editor.putString("rating_desc",response.body().getListDataBorrower().get(i).getRating_desc());
                editor.putString("finance_balance",response.body().getListDataBorrower().get(i).getFinance_balance().toString());
                editor.commit();

                String name  = response.body().getListDataBorrower().get(i).getName();
                String noHp = response.body().getListDataBorrower().get(i).getPhone_number();
                String email = response.body().getListDataBorrower().get(i).getEmail();
                String gender = response.body().getListDataBorrower().get(i).getGender();
                String birthdate = response.body().getListDataBorrower().get(i).getBirth_date();
                String education = response.body().getListDataBorrower().get(i).getLast_education();
                String localId = response.body().getListDataBorrower().get(i).getBorrower_local_id().toString();
                String familyId = response.body().getListDataBorrower().get(i).getFamily_card_id().toString();
                String npwp = response.body().getListDataBorrower().get(i).getPrincipal_taxpayer_id().toString();
                String income = response.body().getListDataBorrower().get(i).getIncome().toString();
                String job = response.body().getListDataBorrower().get(i).getLast_education();
                String address = response.body().getListDataBorrower().get(i).getId_card_address();
                String domicileAddress = response.body().getListDataBorrower().get(i).getDomicile_address();
                String selfieImage = response.body().getListDataBorrower().get(i).getId_selfie_image();
                String localIdImage = response.body().getListDataBorrower().get(i).getLocal_id_image();
                String familyIdImage = response.body().getListDataBorrower().get(i).getFamily_card_image();
                String incomeImage = response.body().getListDataBorrower().get(i).getSalary_slip_image();
                String homeImage = response.body().getListDataBorrower().get(i).getHome_image();
                String familyName = response.body().getListDataBorrower().get(i).getFamily_name();
                String familyHP = response.body().getListDataBorrower().get(i).getFamily_phone_number();
                String familyRelationship = response.body().getListDataBorrower().get(i).getFamily_relationship();
                RelativeLayout warningProfil = findViewById(R.id.layout_warning);

                if ((noHp == null) || (email== null) || (gender== null) || (name == null) || (birthdate== null) || (education== null) || (localId== null) || (familyId== null) || (npwp== null) || (income== null) || (job== null) || (address== null) || (domicileAddress== null) || (selfieImage== null) || (localIdImage== null) || (familyIdImage== null) || (incomeImage== null) || (homeImage== null) || (familyName== null) || (familyHP== null) || (familyRelationship== null)){
                    warningProfil.setVisibility(View.VISIBLE);
                    complete.setVisibility(View.VISIBLE);
                } else {
                    warningProfil.setVisibility(View.GONE);
                    complete.setVisibility(View.GONE);
                }

                if(response.body().getListDataBorrower().get(i).getFinance_balance().toString().equals("0")){
                    tarikDana.setVisibility(View.GONE);
                } else{
                    tarikDana.setVisibility(View.VISIBLE);
                }

            }
            @Override
            public void onFailure(Throwable t) {
                Log.d("AKSES KE SERVER GAGAL",t.getMessage());
                Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void getLastLoan() {
        String idBorrower= preferences.getString("idBorrower",null);
        Call<Borrowers> getDataLoan = mApiInterface.getLastLoan(idBorrower);
        getDataLoan.enqueue(new Callback<Borrowers>() {
            @Override
            public void onResponse(Response<Borrowers> response, Retrofit retrofit) {
                int i =0 ;
                RelativeLayout installmentLayout = findViewById(R.id.layout_status2);
                TextView textWarning = findViewById(R.id.text_warning);
                ImageView iconVal1 = findViewById(R.id.img1);
                ImageView iconVal2 = findViewById(R.id.img2);
                ImageView iconVal3 = findViewById(R.id.img3);
                ImageView iconVal4 = findViewById(R.id.img4);

                SharedPreferences.Editor editorLoanActive = preferencesLoanActive.edit();

                if ((response.body()==null)) {
                    editorLoanActive.putString("loanActive","false");
                    installmentLayout.setVisibility(View.GONE);

                } else {
                    editorLoanActive.putString("loanActive","true");
                    if (response.body().getListLoan().get(i).getLoan_status().equals("Belum disetujui")){
                        tarikDana.setVisibility(View.GONE);
                        installmentLayout.setVisibility(View.GONE);
                        iconVal1.setImageResource(R.drawable.checked);
                        textWarning.setText("Menunggu persetujuan dari admin");
                        textWarning.setTextColor(getResources().getColor(R.color.green));
                    } else if (response.body().getListLoan().get(i).getLoan_status().equals("Pinjaman disetujui")){
                        tarikDana.setVisibility(View.VISIBLE);
                        installmentLayout.setVisibility(View.GONE);
                        iconVal1.setImageResource(R.drawable.checked);
                        iconVal2.setImageResource(R.drawable.checked);
                        iconVal3.setImageResource(R.drawable.checked);
                        iconVal4.setImageResource(R.drawable.checked);
                        textWarning.setText("Pinjaman disetujui, tarik dana pinjaman anda !");
                        textWarning.setTextColor(getResources().getColor(R.color.green));
                    }
                    if (response.body().getListLoan().get(i).getIs_confirm_agent() == null ){
                        tarikDana.setVisibility(View.GONE);
                        installmentLayout.setVisibility(View.GONE);
                        iconVal1.setImageResource(R.drawable.checked);
                        textWarning.setText("Menunggu persetujuan dari agent");
                        textWarning.setTextColor(getResources().getColor(R.color.green));
                    } else if(response.body().getListLoan().get(i).getIs_confirm_agent() == 0 ){
                        tarikDana.setVisibility(View.GONE);
                        installmentLayout.setVisibility(View.GONE);
                        iconVal1.setImageResource(R.drawable.checked);
                        iconVal2.setImageResource(R.drawable.minus);
                        textWarning.setTextColor(getResources().getColor(R.color.red));
                        textWarning.setText(response.body().getListLoan().get(i).getRejected_reason());
                    } else if (response.body().getListLoan().get(i).getIs_confirm_agent() == 1 ){
                        tarikDana.setVisibility(View.GONE);
                        installmentLayout.setVisibility(View.GONE);
                        iconVal1.setImageResource(R.drawable.checked);
                        iconVal2.setImageResource(R.drawable.checked);
                        textWarning.setText("Menunggu persetujuan dari admin");
                        textWarning.setTextColor(getResources().getColor(R.color.green));
                    }

                }
                editorLoanActive.commit();
            }
            @Override
            public void onFailure(Throwable t) {
                Log.d("AKSES KE SERVER GAGAL",t.getMessage());
                Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void setupViewPager(ViewPager mViewPager) {
        SectionsPageAdapter adapter = new SectionsPageAdapter(getSupportFragmentManager());
        adapter.addFragment(new FragmentMurabahah());
        adapter.addFragment(new FragmentReguler());
        adapter.addFragment(new FragmentBusiness());
        mViewPager.setAdapter(adapter);
        mViewPager.setClipToPadding(false);
    }

    //BUTTON HELP
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.help_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_help:
                Intent i = new Intent(PageDashboard.this, Help.class);
                startActivity(i);
        }
        return super.onOptionsItemSelected(item);
    }

    public class SliderTimer extends TimerTask {

        @Override
        public void run() {
            PageDashboard.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (viewPager.getCurrentItem() == 0) {
                        viewPager.setCurrentItem(1);
                    } else if (viewPager.getCurrentItem() == 1) {
                        viewPager.setCurrentItem(2);
                    } else {
                        viewPager.setCurrentItem(0);
                    }
                }
            });
        }
    }

    //Double Tap For Exit App
    @Override
    public void onBackPressed() {
        if (doubleTap){
            super.onBackPressed();
        }else {
            Toast.makeText(this, "Ketuk Tombol Back 2 Kali Untuk Keluar Aplikasi", Toast.LENGTH_SHORT).show();
            doubleTap = true;
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    doubleTap = false;
                }
            },500);
        }
    }
}